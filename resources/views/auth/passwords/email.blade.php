@extends('layouts.auth2')
@section('title', __('lang_v1.reset_password'))
<style>
    input[type=email]{
        background: transparent;
        border: none;
        border-bottom: 1px solid #000000;
    }
</style>

@section('content')

    <div class="col-md-3">&nbsp;</div>
    <div class="col-md-6 right-col-content ui-widget-shadow radius" style="margin-top: 100px; border-radius: 15px;">
        @include('layouts.partials.logo')
        <form  method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="@lang('lang_v1.email_address')">
                <span class="fa fa-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
            <br>
            <div class="form-group" style="align-content: center">
                <button type="submit" class="btn btn-round" style="background-color: #28D25C; color: white">
                    @lang('lang_v1.send_password_reset_link')
                </button>
            </div>
        </form>
    </div>

@endsection
