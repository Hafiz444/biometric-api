{{--{{dd($receipt_details)}}--}}
<div class="row">
	<!-- Header text and img -->
	@if(session('business.id') == 306)
		<div class="col-xs-12 text-center">
			<img style="width: 100%" src="{{ url( '/img/delivery_challan_header.png' ) }}" alt="Business Logo">
		</div>
	@endif
	<div class="col-xs-12 text-center">
		<h2>DELIVERY CHALLAN</h2>
		<br>
	</div>

	<!-- Customer & Business Details -->
	<div class="col-xs-12 text-center">

		<!-- Customer Details span -->
		<span class="pull-left text-left word-wrap">
			<table class="table">
				<tr>
					<td><b>Customer Name:</b></td>
					<td>{{$receipt_details->customer_name}}</td>
				</tr>
				<tr>
					<td><b>Address:</b></td>
					<td><small>{!! $receipt_details->customer_info !!}</small></td>
				</tr>
				<tr>
					<td><b>Attention:</b></td>
					<td></td>
				</tr>
				<tr>
					<td><b>Designation:</b></td>
					<td></td>
				</tr>
			</table>
		</span>

		<!-- Business Details -->
		<span class="pull-right text-left">
			<table class="table">
				<tr>
					<td><b>{{$receipt_details->invoice_no_prefix}}:</b></td>
					<td>{{$receipt_details->invoice_no}}</td>
				</tr>
				<tr>
					<td><b>{{$receipt_details->date_label}}:</b></td>
					<td>{{$receipt_details->invoice_date}}</td>
				</tr>
				<tr>
					<td><b>VEHICAL #:</b></td>
					<td></td>
				</tr>
				<tr>
					<td><b>Challan #:</b></td>
					<td>{{$receipt_details->transaction_id}}</td>
				</tr>
				<tr>
					<td><b>Challan Date:</b></td>
					<td>{{$receipt_details->invoice_date}}</td>
				</tr>
			</table>
		</span>
	</div>
</div>

<div class="row">
	@includeIf('sale_pos.receipts.partial.common_repair_invoice')
</div>

<div class="row">
	<div class="col-xs-12">
		<br/>
		<table class="table table-responsive table-slim">
			<thead>
			<tr>
				<th style="border: 1px solid; width: 20px">Sr.</th>
				<th style="border: 1px solid">{{$receipt_details->table_product_label}}</th>
				<th style="border: 1px solid">Packing</th>
				<th class="text-right" style="border: 1px solid">{{$receipt_details->table_qty_label}}</th>
				<th class="text-right" style="border: 1px solid">Unit</th>
				<th class="text-right" style="border: 1px solid">Remarks</th>
			</tr>
			</thead>
			<tbody>
			@php
				$total_quantity = 0;
			@endphp
			@forelse($receipt_details->lines as $line)
				@php
					$if_sub_unit = \App\Unit::where('short_name', $line['units'])->value('base_unit_id');
					$basic_unit = \App\Unit::where('id', $if_sub_unit)->value('short_name');
					$total_quantity += $line['quantity'];
				@endphp
				<tr>
					<td style="border: 1px solid; width: 20px">{{$loop->iteration}}</td>
					<td style="word-break: break-all; border: 1px solid">
						@if(!empty($line['image']))
							<img src="{{$line['image']}}" alt="Image" width="50" style="float: left; margin-right: 8px;">
						@endif
						{{$line['name']}} {{$line['product_variation']}} {{$line['variation']}}
						@if(!empty($line['sub_sku'])), {{$line['sub_sku']}} @endif @if(!empty($line['brand'])), {{$line['brand']}} @endif @if(!empty($line['cat_code'])), {{$line['cat_code']}}@endif
						@if(!empty($line['product_custom_fields'])), {{$line['product_custom_fields']}} @endif
						@if(!empty($line['sell_line_note']))({{$line['sell_line_note']}}) @endif
						@if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}:  {{$line['lot_number']}} @endif
						@if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}:  {{$line['product_expiry']}} @endif

						@if(!empty($line['warranty_name'])) <br><small>{{$line['warranty_name']}} </small>@endif @if(!empty($line['warranty_exp_date'])) <small>- {{@format_date($line['warranty_exp_date'])}} </small>@endif
						@if(!empty($line['warranty_description'])) <small> {{$line['warranty_description'] ?? ''}}</small>@endif
					</td>
					<td style="border: 1px solid">{{$line['units']}}</td>
					<td class="text-right" style="border: 1px solid">{{$line['quantity']}}</td>
					<td class="text-right" style="border: 1px solid">{{$basic_unit ? $basic_unit : $line['units']}}</td>
					<td class="text-right" style="border: 1px solid; width: 30%"></td>
				</tr>
			@empty
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
			@endforelse
			<tr>
				<th colspan="2" style="text-align: center; border: 1px solid">{!! $receipt_details->total_label !!}</th>
				<td style="border: 1px solid"></td>
				<td class="text-right" style="border: 1px solid">{{$total_quantity}}</td>
				<td style="border: 1px solid"></td>
				<td style="border: 1px solid"></td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<br>
<br>
<div class="row">
	<div class="col-xs-12">
		<table class="table">
			<thead>
			<tr>
				<th>Dispatched By:</th>
				<td></td>
				<th>Received By:</th>
				<td></td>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>Name:</td>
				<td></td>
				<td>Name:</td>
				<td></td>
			</tr>
			<tr>
				<td>Sign & Stamp.</td>
				<td></td>
				<td>Sign & Stamp.</td>
				<td></td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<br>
<br>
<br>
<!--Footer address text-->
<p style="text-align: center">
	Works/R&D: 5340, 42, 51, 52, Sec#15/16, Gulistan-e-Mazdoor, Hub River Road, Karachi - Pakistan <br>
	www.gwischemical.com, <u>info@gwischemical.com</u>, 021-35301220/021-35301221
</p>
