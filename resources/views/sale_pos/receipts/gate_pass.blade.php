{{--{{dd($receipt_details)}}--}}
<div class="row">
	<!-- Header text and img -->
	@if(session('business.id') == 306)
		<div class="col-xs-12 text-center">
			<img style="width: 100%" src="{{ url( '/img/delivery_challan_header.png' ) }}" alt="Business Logo">
		</div>
	@endif
	<div class="col-xs-12 text-center">
		<h2>GATE PASS</h2>
		<br>
	</div>

	<!-- Customer & Business Details -->
	<div class="col-xs-12 text-left">
		<div class="row">
			<div class="col-xs-4"><b>Sr #</b> <u>{{$receipt_details->transaction_id}}</u><br>
				<b>Mr.</b> {{$receipt_details->customer_name}}</div>
			<div class="col-xs-4"><b>E. Code</b> _______________</div>
			<div class="col-xs-4"><b>Date</b> {{$receipt_details->invoice_date}}<br><b>Dept.</b> _______________</div>
		</div>
	</div>
</div>

<div class="row">
	@includeIf('sale_pos.receipts.partial.common_repair_invoice')
</div>

<div class="row">
	<div class="col-xs-12">
		<br/>
		<table class="table table-responsive table-slim text-center">
			<thead>
			<tr>
				<th style="border: 1px solid; width: 20px">Sr.</th>
				<th style="border: 1px solid">Item Code</th>
				<th style="border: 1px solid">{{$receipt_details->table_product_label}}</th>
				<th class="text-right" style="border: 1px solid">Unit</th>
				<th class="text-right" style="border: 1px solid">{{$receipt_details->table_qty_label}}</th>
				<th class="text-right" style="border: 1px solid">Remarks</th>
			</tr>
			</thead>
			<tbody>
			@php
				$total_quantity = 0;
			@endphp
			@forelse($receipt_details->lines as $line)
				@php
					$if_sub_unit = \App\Unit::where('short_name', $line['units'])->value('base_unit_id');
					$basic_unit = \App\Unit::where('id', $if_sub_unit)->value('short_name');
					$total_quantity += $line['quantity'];
				@endphp
				<tr>
					<td style="border: 1px solid; width: 20px">{{$loop->iteration}}</td>
					<td style="border: 1px solid">{{$line['sub_sku']}}</td>
					<td style="word-break: break-all; border: 1px solid">

						{{$line['name']}} {{$line['product_variation']}} {{$line['variation']}}
						@if(!empty($line['product_custom_fields'])), {{$line['product_custom_fields']}} @endif
						@if(!empty($line['sell_line_note']))({{$line['sell_line_note']}}) @endif
						@if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}:  {{$line['lot_number']}} @endif
						@if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}:  {{$line['product_expiry']}} @endif

						@if(!empty($line['warranty_name'])) <br><small>{{$line['warranty_name']}} </small>@endif @if(!empty($line['warranty_exp_date'])) <small>- {{@format_date($line['warranty_exp_date'])}} </small>@endif
						@if(!empty($line['warranty_description'])) <small> {{$line['warranty_description'] ?? ''}}</small>@endif
					</td>
					<td style="border: 1px solid">{{$line['units']}}</td>
					<td style="border: 1px solid">{{$line['quantity']}}</td>
					<td style="border: 1px solid; width: 30%"></td>
				</tr>
			@empty
				<tr>
					<td colspan="6">&nbsp;</td>
				</tr>
			@endforelse
			<tr>
				<th colspan="3" style="text-align: center; border: 1px solid">{!! $receipt_details->total_label !!}</th>
				<td style="border: 1px solid"></td>
				<td style="border: 1px solid">{{$total_quantity}}</td>
				<td style="border: 1px solid"></td>
			</tr>
			</tbody>
		</table>
	</div>
</div>
<br>
<br>
<div class="row">
	<div class="col-xs-12">
		<table class="table text-center">
			<thead>
			<tr>
				<th style="border: 1px solid">Prepared By: <br><br>&nbsp; {{--{{$receipt_details->created_by}}--}}</th>
				<th style="border: 1px solid">Received By: <br><br>&nbsp;</th>
				<th style="border: 1px solid">Head of Department <br><br>&nbsp;</th>
				<th style="border: 1px solid">Pass In/Out Incharge <br><br>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
