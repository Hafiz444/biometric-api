{{--{{dd($receipt_details)}}--}}
<div class="row">

	<br>
	<br>
	<br>
	<!-- Header text -->
	@if(!empty($receipt_details->header_text))
		<div class="col-xs-12 text-center">
			{!! $receipt_details->header_text !!}
		</div>
	@endif

	<!-- Customer & Business Details -->
	<div class="col-xs-12 text-center">

		<!-- Customer Details span -->
		<span class="pull-left text-left word-wrap">
			<table class="table">
				<tr>
					<td><b>M/S:</b></td>
					<td>{{$receipt_details->customer_name}}</td>
				</tr>
				<tr>
					<td><b>Address:</b></td>
					<td><small>{!! $receipt_details->customer_info !!}</small></td>
				</tr>
				<tr>
					<td><b>Attention:</b></td>
					<td></td>
				</tr>
				<tr>
					<td><b>{{$receipt_details->customer_tax_label}} #:</b></td>
					<td>{{$receipt_details->customer_tax_number}}</td>
				</tr>
				<tr>
					<td><b>STRN #:</b></td>
					<td></td>
				</tr>
			</table>
		</span>

		<!-- Business Details -->
		<span class="pull-right text-left">
			<table class="table">
				<tr>
					<td><b>{{$receipt_details->invoice_no_prefix}}:</b></td>
					<td>{{$receipt_details->invoice_no}}</td>
				</tr>
				<tr>
					<td><b>{{$receipt_details->date_label}}:</b></td>
					<td>{{$receipt_details->invoice_date}}</td>
				</tr>
				<tr>
					<td><b>{{$receipt_details->sub_heading_line1}}:</b></td>
					<td>{{$receipt_details->transaction_id}}</td>
				</tr>
				<tr>
					<td><b>PO Date:</b></td>
					<td>{{$receipt_details->invoice_date}}</td>
				</tr>
				<tr>
					<td><b>{{$receipt_details->tax_label1}}:</b></td>
					<td>{{$receipt_details->tax_info1}}</td>
				</tr>
				<tr>
					<td><b>{{$receipt_details->tax_label2}}:</b></td>
					<td>{{$receipt_details->tax_info2}}</td>
				</tr>
			</table>
		</span>
	</div>
</div>

<div class="row">
	@includeIf('sale_pos.receipts.partial.common_repair_invoice')
</div>

<div class="row">
	<div class="col-xs-12">
		<br/>
		<table class="table table-responsive table-slim text-center">
			<thead>
				<tr>
					<th style="border: 1px solid; width: 20px">Sr.</th>
					<th style="border: 1px solid; width: 30%">{{$receipt_details->table_product_label}}</th>
					@if($receipt_details->invoice_format == 'sst')
						<th style="border: 1px solid">Tariff Heading</th>
					@elseif($receipt_details->invoice_format == 'gst')
						<th style="border: 1px solid">{{$receipt_details->table_qty_label}}</th>
						<th style="border: 1px solid">HS Code</th>
						<th style="border: 1px solid">Unit</th>
						<th style="border: 1px solid">{{$receipt_details->table_unit_price_label}}</th>
					@else
						<th style="border: 1px solid">{{$receipt_details->table_qty_label}}</th>
						<th style="border: 1px solid">Unit</th>
						<th style="border: 1px solid">{{$receipt_details->table_unit_price_label}}</th>
					@endif

					@if($receipt_details->invoice_format == 'sst')
						<th style="border: 1px solid">Value of Exclusive of Sindh Sale Tax</th>
					@elseif($receipt_details->invoice_format == 'gst')
						<th style="border: 1px solid">Total Price <br><small>(Excluding Sales Tax)</small></th>
					@endif
					@if($receipt_details->invoice_format == 'sst')
						<th style="border: 1px solid">Rate Of Sale Tax</th>
					@endif
					@if($receipt_details->invoice_format == 'sst')
						<th style="border: 1px solid">Amount Of Sindh Sale Tax</th>
					@elseif($receipt_details->invoice_format == 'gst')
						<th style="border: 1px solid">GST <br><small>(Payable) 17%</small></th>
					@endif
					@if($receipt_details->invoice_format == 'sst')
						<th style="border: 1px solid">Value Inclusive Of Sindh Sale Tax</th>
					@elseif($receipt_details->invoice_format == 'gst')
						<th style="border: 1px solid">Total Price <br><small>(Including Sale Tax)</small></th>
					@else
						<th style="border: 1px solid">Sub Total</th>
					@endif
				</tr>
			</thead>
			<tbody>
			@php
				$total_price_exc_tax = 0;
				$total_tax = 0;
			@endphp
				@forelse($receipt_details->lines as $line)
					@php
                        $total_price_exc_tax += $line['price_exc_tax'];
                        $total_tax += $line['tax']*$line['quantity'];
					@endphp
					<tr>
						<td style="border: 1px solid; width: 20px">{{$loop->iteration}}</td>
						<td style="word-break: break-all; border: 1px solid">
							@if(!empty($line['image']))
								<img src="{{$line['image']}}" alt="Image" width="50" style="float: left; margin-right: 8px;">
							@endif
                            {{$line['name']}} {{$line['product_variation']}} {{$line['variation']}} 
                            @if(!empty($line['sub_sku'])), {{$line['sub_sku']}} @endif @if(!empty($line['brand'])), {{$line['brand']}} @endif @if(!empty($line['cat_code'])), {{$line['cat_code']}}@endif
                            @if(!empty($line['product_custom_fields'])), {{$line['product_custom_fields']}} @endif
                            @if(!empty($line['sell_line_note']))({{$line['sell_line_note']}}) @endif 
                            @if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}:  {{$line['lot_number']}} @endif 
                            @if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}:  {{$line['product_expiry']}} @endif

                            @if(!empty($line['warranty_name'])) <br><small>{{$line['warranty_name']}} </small>@endif @if(!empty($line['warranty_exp_date'])) <small>- {{@format_date($line['warranty_exp_date'])}} </small>@endif
                            @if(!empty($line['warranty_description'])) <small> {{$line['warranty_description'] ?? ''}}</small>@endif
                        </td>
						@if($receipt_details->invoice_format == 'sst')
							<td style="border: 1px solid">{{$line['tariff_heading']}}</td>
						@elseif($receipt_details->invoice_format == 'gst')
							<td style="border: 1px solid">{{$line['quantity']}}</td>
							<td style="border: 1px solid">{{$line['hs_code']}}</td>
							<td style="border: 1px solid">{{$line['units']}}</td>
							<td style="border: 1px solid">{{$line['unit_price_exc_tax']}}</td>
						@else
							<td style="border: 1px solid">{{$line['quantity']}}</td>
							<td style="border: 1px solid">{{$line['units']}}</td>
							<td style="border: 1px solid">{{$line['unit_price_exc_tax']}}</td>
						@endif
						@if($receipt_details->invoice_format == 'sst')
							<td style="border: 1px solid">{{$line['unit_price_exc_tax']*$line['quantity']}}</td>
						@elseif($receipt_details->invoice_format == 'gst')
							<td style="border: 1px solid">{{$line['price_exc_tax']}}</td>
						@endif
						@if($receipt_details->invoice_format == 'sst')
							<td style="border: 1px solid">{{$line['tax_percent']}}%</td>
						@endif
						@if($receipt_details->invoice_format == 'sst' || $receipt_details->invoice_format == 'gst')
							<td style="border: 1px solid">{{$line['tax']*$line['quantity']}}</td>
						@endif
						<td style="border: 1px solid">{{$line['line_total']}}</td>
					</tr>
				@empty
					@if($receipt_details->invoice_format == 'sst')
						<tr>
							<td colspan="7">&nbsp;</td>
						</tr>
					@elseif($receipt_details->invoice_format == 'sst')
						<tr>
							<td colspan="9">&nbsp;</td>
						</tr>
					@else
						<tr>
							<td colspan="6">&nbsp;</td>
						</tr>
					@endif

				@endforelse

				@if($receipt_details->invoice_format == 'sst')
					<tr>
						<th colspan="3" style="text-align: center; border: 1px solid">{!! $receipt_details->total_label !!}</th>
						<td style="border: 1px solid">{{$total_price_exc_tax}}</td>
						<td></td>
						<td style="border: 1px solid">{{$total_tax}}</td>
						<td style="border: 1px solid">{{$receipt_details->total}}</td>
					</tr>
					<tr>
						<th colspan="2" style="border: 1px solid">Amount in Words</th>
						<td colspan="5" style="border: 1px solid">
							@php
								$amount_in_words = \App\Helper::ConvertNumberToWords($receipt_details->total_unformatted)
							@endphp
							{{$amount_in_words}} Only
						</td>
					</tr>
				@elseif($receipt_details->invoice_format == 'gst')
					<tr>
						<th colspan="6" style="text-align: center; border: 1px solid">{!! $receipt_details->total_label !!}</th>
						<td style="border: 1px solid">{{$total_price_exc_tax}}</td>
						<td style="border: 1px solid">{{$total_tax}}</td>
						<td style="border: 1px solid">{{$receipt_details->total}}</td>
					</tr>
					<tr>
						<th colspan="2" style="border: 1px solid">Amount in Words</th>
						<td colspan="7" style="border: 1px solid">
							@php
								$amount_in_words = \App\Helper::ConvertNumberToWords($receipt_details->total_unformatted)
							@endphp
							{{$amount_in_words}} Only
						</td>
					</tr>
				@else
					<tr>
						<th colspan="4" style="text-align: center; border: 1px solid">{!! $receipt_details->total_label !!}</th>
						<td style="border: 1px solid">{{$total_price_exc_tax}}</td>
						<td style="border: 1px solid">{{$receipt_details->total}}</td>
					</tr>
					<tr>
						<th colspan="2" style="border: 1px solid">Amount in Words</th>
						<td colspan="4" style="border: 1px solid">
							@php
								$amount_in_words = \App\Helper::ConvertNumberToWords($receipt_details->total_unformatted)
							@endphp
							{{$amount_in_words}} Only
						</td>
					</tr>
				@endif

			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-md-12"><hr/></div>
	<span>
	@if(!empty($receipt_details->footer_text))
		<div class="row">
			<div class="col-xs-12">
				{!! $receipt_details->footer_text !!}
			</div>
		</div>
	@endif
	</span>
</div>