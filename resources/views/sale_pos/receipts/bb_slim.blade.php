<html>
<head>
	<style>
		@media print {
			@page {
				margin: 0mm 3mm 0mm 10mm;
			}

			table {
				border-collapse: collapse;
				font-size: 8px;
			}

			th, td {
				padding: 2px;
				font-weight: 600;
				font-family: Arial, Helvetica, sans-serif;
				border-bottom: none;
				border-collapse: collapse;
			}

			h2 {
				background-color: black;
				color: white;
			}

			h5 {
				margin-top: 5px;
				font-size: 10px;
				line-height: 2px;
				padding-top: 3px;
			}

			.line_height {
				line-height: 1px;
			}

			p {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 10px;
			}

			.tb_heading {
				border-bottom: 1px solid black;
				border-top: 1px solid black;
				border-left: 1px solid black;
				border-right: 1px solid black;
			}

			.tb_des {
				border-left: 1px solid black;
				border-right: 1px solid black;
				border-bottom: 1px solid black;
			}

			.logo {
				margin-left: -3px;
			}
		}
	</style>

</head>
<body>
<table border="0">
	<tr>
		<td class="logo" style="border-bottom: 0px solid white; align-content: center;">
			@if(!empty($receipt_details->logo))
				<img src="{{$receipt_details->logo}}" class=""  >
			@endif
		</td>
	</tr>
	<tr>
		<td style="border-bottom: 0px solid white;">
			<h1 style="text-align: center">
				@if(!empty($receipt_details->display_name))
					{{$receipt_details->display_name}}
				@endif
			</h1>
			<p style="align-content: center"><b>
				@if(!empty($receipt_details->address))
					{!! $receipt_details->address !!}
				@endif
			</p>
			<p align="Center"></p>
		</td>
	</tr>
	<tr>
		<td>
		</td>
	</tr>
</table>
<table border="0">
	<tr>
		<td>
			<b>@if(!empty($receipt_details->invoice_no_prefix))<b>{!! $receipt_details->invoice_no_prefix !!}</b>@endif
				{{$receipt_details->invoice_no}} </b>
		</td>
		<td>
			<b>{{$receipt_details->date_label}}</b> {{$receipt_details->invoice_date}}
			@if(!empty($receipt_details->due_date_label))
				<br><b>{{$receipt_details->due_date_label}}</b> {{$receipt_details->due_date ?? ''}}
			@endif

			@if(!empty($receipt_details->serial_no_label) || !empty($receipt_details->repair_serial_no))<br>
			@if(!empty($receipt_details->serial_no_label))
				<b>{!! $receipt_details->serial_no_label !!}</b>
			@endif
			{{$receipt_details->repair_serial_no}}<br>
			@endif

			@if(!empty($receipt_details->repair_status_label) || !empty($receipt_details->repair_status))
				@if(!empty($receipt_details->repair_status_label))
					<b>{!! $receipt_details->repair_status_label !!}</b>
				@endif
				{{$receipt_details->repair_status}}<br>
			@endif

			@if(!empty($receipt_details->repair_warranty_label) || !empty($receipt_details->repair_warranty))
				@if(!empty($receipt_details->repair_warranty_label))
					<b>{!! $receipt_details->repair_warranty_label !!}</b>
				@endif
				{{$receipt_details->repair_warranty}}<br>
			@endif

		<!-- Waiter info -->
			@if(!empty($receipt_details->service_staff_label) || !empty($receipt_details->service_staff))
				@if(!empty($receipt_details->service_staff_label))<b>{!! $receipt_details->service_staff_label !!}</b>
				@endif
				{{$receipt_details->service_staff}}
			@endif
		</td>
	</tr>
	<tr>
		<td>
			@if(!empty($receipt_details->types_of_service))
				<strong>{!! $receipt_details->types_of_service_label !!}:</strong>
				{{$receipt_details->types_of_service}}

			<!-- Waiter info -->
				@if(!empty($receipt_details->types_of_service_custom_fields))
					@foreach($receipt_details->types_of_service_custom_fields as $key => $value)
						<br><strong>{{$key}}: </strong> {{$value}}
					@endforeach
				@endif
			@endif

			@if(!empty($receipt_details->table_label) || !empty($receipt_details->table))
				@if(!empty($receipt_details->table_label))
					<b>{!! $receipt_details->table_label !!}</b>
				@endif
				{{$receipt_details->table}}
			@endif

		<!-- customer info -->
			@if(!empty($receipt_details->customer_name))
				<b>{{ $receipt_details->customer_label }}</b> {{ $receipt_details->customer_name }}
			@endif

			@if(!empty($receipt_details->customer_custom_fields))
				{!! $receipt_details->customer_custom_fields !!}
			@endif
			@if(!empty($receipt_details->customer_info))
				{!! $receipt_details->customer_info !!}
			@endif
			@if(!empty($receipt_details->client_id_label))
				<b>{{ $receipt_details->client_id_label }}</b> {{ $receipt_details->client_id }}
			@endif
			@if(!empty($receipt_details->customer_tax_label))
				<b>{{ $receipt_details->customer_tax_label }}</b> {{ $receipt_details->customer_tax_number }}
			@endif
			@if(!empty($receipt_details->customer_rp_label))
				<strong>{{ $receipt_details->customer_rp_label }}</strong> {{ $receipt_details->customer_total_rp }}
			@endif
		</td>
		<td>
			@if(!empty($receipt_details->sales_person_label))
				<b>{{ $receipt_details->sales_person_label }}</b> {{ $receipt_details->sales_person }}
			@endif
		</td>
	</tr>
</table>
<div>
	<p>
		@if(!empty($receipt_details->defects_label) || !empty($receipt_details->repair_defects))
			<br>
			@if(!empty($receipt_details->defects_label))
				<b>{!! $receipt_details->defects_label !!}</b>
			@endif
			{{$receipt_details->repair_defects}}
		@endif
	</p>
</div>
<table border="0">
	<tr>
		<th class="tb_heading" >#</th>
		<th class="tb_heading" >{{$receipt_details->table_product_label}}</th>
		<th class="tb_heading" align="right">{{$receipt_details->table_qty_label}}</th>
		<th class="tb_heading" align="center">{{$receipt_details->table_unit_price_label}}</th>
		<th class="tb_heading" align="center">{{$receipt_details->table_subtotal_label}}</th>
	</tr>
	@php($itemcount=0)
	@forelse($receipt_details->lines as $line)
		<tr style="border-top: 1px solid black; border-left: 1px solid black; border-right: 1px solid black;">
			<td class="serial_number tb_des" style="">
				{{$loop->iteration}}
			</td>
			<td class="tb_des" style="word-break: break-all;">
				@if(!empty($line['image']))
					<img src="{{$line['image']}}" alt="Image" width="50" style="float: left; margin-right: 8px;">
				@endif
				{{$line['name']}} {{$line['product_variation']}} {{$line['variation']}}
				@if(!empty($line['sub_sku'])), {{$line['sub_sku']}} @endif @if(!empty($line['brand'])), {{$line['brand']}} @endif @if(!empty($line['cat_code'])), {{$line['cat_code']}}@endif
				@if(!empty($line['product_custom_fields'])), {{$line['product_custom_fields']}} @endif
				@if(!empty($line['sell_line_note']))({{$line['sell_line_note']}}) @endif
				@if(!empty($line['lot_number']))<br> {{$line['lot_number_label']}}:  {{$line['lot_number']}} @endif
				@if(!empty($line['product_expiry'])), {{$line['product_expiry_label']}}:  {{$line['product_expiry']}} @endif
				@if(!empty($line['warranty_name'])) <br><small>{{$line['warranty_name']}} </small>@endif @if(!empty($line['warranty_exp_date'])) <small>- {{@format_date($line['warranty_exp_date'])}} </small>@endif
				@if(!empty($line['warranty_description'])) <small> {{$line['warranty_description'] ?? ''}}</small>@endif
			</td>
			<td class="tb_des">{{$line['quantity']}} <!--{{$line['units']}}--> </td>
			<td class="tb_des">{{$line['unit_price_inc_tax']}}</td>
			<td class="tb_des">{{$line['line_total']}}</td>
			@php($itemcount++)
		</tr>
		@if(!empty($line['modifiers']))
			@foreach($line['modifiers'] as $modifier)
				<tr class="border-bottom border-top">
					<td class="tb_des">
						{{$modifier['name']}} {{$modifier['variation']}}
						@if(!empty($modifier['sub_sku'])), {{$modifier['sub_sku']}} @endif @if(!empty($modifier['cat_code'])), {{$modifier['cat_code']}}@endif
						@if(!empty($modifier['sell_line_note']))({{$modifier['sell_line_note']}}) @endif
					</td>
					<td class="tb_des">{{$modifier['quantity']}} {{$modifier['units']}} </td>
					<td class="tb_des">{{$modifier['unit_price_inc_tax']}}</td>
					<td class="tb_des">{{$modifier['line_total']}}</td>
				</tr>
				@php($itemcount++)
			@endforeach
		@endif
	@empty
		<tr>
			<td colspan="4" class="tb_des">&nbsp;</td>
		</tr>
	@endforelse

</table>
<div><p></p></div>
<table>
	<tr>
		<td ><h5>Items: &nbsp {{$itemcount}}</h5></td>
		<td ><h5>&nbsp</h5></td>
		<td ><h5>&nbsp</h5></td>
	</tr>
	<tr>
		<td ><h5>{!! $receipt_details->subtotal_label !!}</h5></td>
		<td ><h5>{{$receipt_details->subtotal}}</h5></td>
		<td><h5></h5></td>
	</tr>

	@if(!empty($receipt_details->payments))
		@foreach($receipt_details->payments as $payment)
			<tr>
				<td ><h5>{{$payment['method']}}:</h5></td>
				<td ><h5>{{$payment['amount']}}</h5></td>
				<td><h5></h5></td>
			</tr>
		@endforeach
	@endif

	@if(!empty($receipt_details->all_due))
		<tr>
			<td ><h5>{!! $receipt_details->all_bal_label !!}</h5></td>
			<td ><h5>{{$receipt_details->all_due}}</h5></td>
			<td><h5></h5></td>
		</tr>
	@endif

	@if(!empty($receipt_details->shipping_charges))
		<tr>
			<td ><h5>{!! $receipt_details->shipping_charges_label !!}</h5></td>
			<td ><h5>{{$receipt_details->shipping_charges}}</h5></td>
			<td><h5></h5></td>
		</tr>
	@endif

	@if( !empty($receipt_details->discount) )
		<tr>
			<td ><h5>{!! $receipt_details->discount_label !!}</h5></td>
			<td ><h5> {{$receipt_details->discount}}</h5></td>
			<td><h5></h5></td>
		</tr>
	@endif

	@if( !empty($receipt_details->reward_point_label) )
		<tr>
			<td ><h5>{!! $receipt_details->reward_point_label !!}</h5></td>
			<td ><h5>(-) {{$receipt_details->reward_point_amount}}</h5></td>
			<td><h5></h5></td>
		</tr>
	@endif

	@if( !empty($receipt_details->tax) )
		<tr>
			<td ><h5>{!! $receipt_details->tax_label !!}</h5></td>
			<td ><h5>(+) {{$receipt_details->tax}}</h5></td>
			<td><h5></h5></td>
		</tr>
	@endif

	@if(!empty($receipt_details->total_due))
		<tr>
			<td ><h5>{!! $receipt_details->total_due_label !!}</h5></td>
			<td ><h5>{{$receipt_details->total_due}}</h5></td>
			<td><h5></h5></td>
		</tr>
	@endif

	<tr>
		<td ><h5><b>Net Total:</b></h5></td>
		<td ><h5>{{$receipt_details->total}}</h5></td>
		<td><h5></h5></td>
	</tr>

	@if(!empty($receipt_details->total_paid))
		<tr>
			<td><h5><b>{!! $receipt_details->total_paid_label !!}:</b></h5></td>
			<td><h5>{{$receipt_details->total_paid}}</h5></td>
			<td><h5></h5></td>
		</tr>
	@endif

	<tr>
		<td></td>
		<td></td>
		<td><h5></h5></td>
	</tr>
</table>

@if(!empty($receipt_details->additional_notes))
	<div  style="margin-top:20px; ">
		<div>
			{{$receipt_details->additional_notes}}
		</div>
	</div>
@endif

@if($receipt_details->show_barcode)
	<div style="padding:10px !important;">
		<div>
			{{-- Barcode --}}
			<img class="center-block" src="data:image/png;base64,{{DNS1D::getBarcodePNG($receipt_details->invoice_no, 'C128', 2,30,array(39, 48, 54), true)}}">
		</div>
	</div>
@endif

@if(!empty($receipt_details->footer_text))
	<div class="" style="padding:10px !important;">
		<div class="">
			{!! $receipt_details->footer_text !!}
		</div>
	</div>
@endif

<div class=" border-top" style="margin-top:20px !important; border-top: 1px dotted black !important;">
	<div class=" text-center">
		<small>	Powered By Kodeinn | 0304-4612345 </small>
	</div>
</div>
</body>
</html>