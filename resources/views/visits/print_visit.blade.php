<style>
	@media print {
		a[href]:after {
			content: none !important;
		}
		table * {
			font-size: 12px !important;
		}
	}
</style>
<div>
	<div class="row">
		<!-- Header text and img -->
		@if(session('business.id') == 306)
			<div class="col-xs-12 text-center">
				<img style="width: 100%" src="{{ url( '/img/delivery_challan_header.png' ) }}" alt="Business Logo">
			</div>
		@endif
<!--		<div class="col-xs-12 text-center">-->
			<h2 class="text-center">@lang('visit.visit_header')</h2>
<!--		</div>-->
		<div class="col-md-12">
			<table class="table table-condensed table-bordered">
				<tbody>
				<tr>
					<th>@lang('visit.customer_name')</th>
					<td>{{$customer_name}}</td>
					<th>@lang('visit.customer_business')</th>
					<td>{{$visit->customer_business}}</td>
					<th>@lang('visit.phone_no')</th>
					<td>{{$visit->customer_phone}}</td>
				</tr>
				<tr>
					<th>@lang('visit.email')</th>
					<td>{{$visit->email}}</td>
					<th>@lang('visit.designation')</th>
					<td>{{$visit->designation}}</td>
					<th>@lang('visit.plant_capacity')</th>
					<td>{{$visit->plant_capacity}}</td>
				</tr>
				<tr>
					<th>@lang('visit.date_time')</th>
					<td>{{$visit->date_time}}</td>
					<th>@lang('visit.business_locations')</th>
					<td>{{$location_name}}</td>
					<th>@lang('visit.address')</th>
					<td>{{$visit->address}}</td>
				</tr>
				</tbody>
			</table>

			<!--Pre-Treatment table-->
			<table class="table table-condensed table-bordered">
				<thead>
				<tr>
					<th>Pre-Treatment</th>
					<th>Units</th>
					<th colspan="2" class="text-center">Reading</th>
				</tr>
				<tr>
					<th></th>
					<th></th>
					<th class="text-center">RO 1 (Before CIP)</th>
					<th class="text-center">RO 2 (After CIP)</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Hours Meter</th>
					<td>hrs</td>
					<td>{{$visit->hours_meter_RO1}}</td>
					<td>{{$visit->hours_meter_RO2}}</td>
				</tr>
				<tr>
					<th>Water Temperature</th>
					<td>C</td>
					<td>{{$visit->water_temperature_RO1}}</td>
					<td>{{$visit->water_temperature_RO2}}</td>
				</tr>
				<tr>
					<th>Multimedia Filter inlet pressure (A)</th>
					<td>psig/bar</td>
					<td>{{$visit->multimedia_filter_inlet_RO1}}</td>
					<td>{{$visit->multimedia_filter_inlet_RO2}}</td>
				</tr>
				<tr>
					<th>Multimedia Filter outlet pressure (A)</th>
					<td>psig/bar</td>
					<td>{{$visit->multimedia_filter_outlet_RO1}}</td>
					<td>{{$visit->multimedia_filter_outlet_RO2}}</td>
				</tr>
				<tr>
					<th>Differential Pressure</th>
					<td>psig/bar</td>
					<td>{{$visit->differential_pressure_RO1}}</td>
					<td>{{$visit->differential_pressure_RO2}}</td>
				</tr>
				</tbody>

				<thead>
				<tr>
					<th><u>FEED</u></th>
					<th></th>
					<th colspan="2"></th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>PH</th>
					<td></td>
					<td>{{$visit->ph_feed_RO1}}</td>
					<td>{{$visit->ph_feed_RO2}}</td>
				</tr>
				<tr>
					<th>TDS/Conductivity</th>
					<td>mg/lit/us/cm</td>
					<td>{{$visit->tds_conductivity_RO1}}</td>
					<td>{{$visit->tds_conductivity_RO2}}</td>
				</tr>
				<tr>
					<th>Free Chlorine</th>
					<td>mg/lit/us/cm</td>
					<td>{{$visit->free_chlorine_RO1}}</td>
					<td>{{$visit->free_chlorine_RO2}}</td>
				</tr>
				<tr>
					<th>Iron</th>
					<td>mg/lit/us/cm</td>
					<td>{{$visit->iron_RO1}}</td>
					<td>{{$visit->iron_RO2}}</td>
				</tr>
				<tr>
					<th>SDI Before Cartridge Filter</th>
					<td></td>
					<td>{{$visit->sdi_before_cartridge_RO1}}</td>
					<td>{{$visit->sdi_before_cartridge_RO2}}</td>
				</tr>
				<tr>
					<th>SDI After Cartridge Filter</th>
					<td></td>
					<td>{{$visit->sdi_after_cartridge_RO1}}</td>
					<td>{{$visit->sdi_after_cartridge_RO2}}</td>
				</tr>
				</tbody>

				<thead>
				<tr>
					<th><u>PRODUCT WATER</u></th>
					<th></th>
					<th colspan="2"></th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>PH</th>
					<td></td>
					<td>{{$visit->ph_product_water_RO1}}</td>
					<td>{{$visit->ph_product_water_RO2}}</td>
				</tr>
				<tr>
					<th>TDS/Conductivity</th>
					<td>mg/lit/us/cm</td>
					<td>{{$visit->tds_conductivity_product_water_RO1}}</td>
					<td>{{$visit->tds_conductivity_product_water_RO2}}</td>
				</tr>
				</tbody>

				<thead>
				<tr>
					<th><u>REJECT WATER</u></th>
					<th></th>
					<th colspan="2"></th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>PH</th>
					<td></td>
					<td>{{$visit->ph_reject_water_RO1}}</td>
					<td>{{$visit->ph_reject_water_RO2}}</td>
				</tr>
				<tr>
					<th>TDS/Conductivity</th>
					<td>mg/lit</td>
					<td>{{$visit->tds_conductivity_RO1}}</td>
					<td>{{$visit->tds_conductivity_RO2}}</td>
				</tr>
				</tbody>
			</table>

			<table class="table table-condensed table-bordered">
				<thead>
				<tr>
					<th>Chemical Dosing</th>
					<th>Stock</th>
					<th colspan="2" class="text-center">Dosing</th>
					<th colspan="2" class="text-center">Tank Level</th>
				</tr>
				<tr>
					<th>Pre-Treatment</th>
					<th></th>
					<th class="text-center">RO 1 (Before CIP)</th>
					<th class="text-center">RO 2 (After CIP)</th>
					<th class="text-center">RO 1 (Before CIP)</th>
					<th class="text-center">RO 2 (After CIP)</th>
				</tr>
				</thead>
				<tbody>
				<tr>

					<th>Sodium Hypochlorite</th>
					<td></td>
					<td>{{$visit->sodium_hypochlorite_dosing_RO1}}</td>
					<td>{{$visit->sodium_hypochlorite_dosing_RO2}}</td>
					<td>{{$visit->sodium_hypochlorite_tank_level_RO1}}</td>
					<td>{{$visit->sodium_hypochlorite_tank_level_RO2}}</td>
				</tr>
				<tr>
					<th>Polygard 600</th>
					<td></td>
					<td>{{$visit->polygard_600_dosing_RO1}}</td>
					<td>{{$visit->polygard_600_dosing_RO2}}</td>
					<td>{{$visit->polygard_600_tank_level_RO1}}</td>
					<td>{{$visit->polygard_600_tank_level_RO2}}</td>
				</tr>
				<tr>
					<th>Sodium Metabisulphite</th>
					<td></td>
					<td>{{$visit->sodium_metabisulphite_dosing_RO1}}</td>
					<td>{{$visit->sodium_metabisulphite_dosing_RO2}}</td>
					<td>{{$visit->sodium_metabisulphite_tank_level_RO1}}</td>
					<td>{{$visit->sodium_metabisulphite_tank_level_RO2}}</td>
				</tr>
				<tr>
					<th>Acid H2SO4 / HCL</th>
					<td></td>
					<td>{{$visit->acid_dosing_RO1}}</td>
					<td>{{$visit->acid_dosing_RO2}}</td>
					<td>{{$visit->acid_tank_level_RO1}}</td>
					<td>{{$visit->acid_tank_level_RO2}}</td>
				</tr>
				<tr>
					<th>Biocide R-22/R-22</th>
					<td></td>
					<td>{{$visit->biocide_dosing_RO1}}</td>
					<td>{{$visit->biocide_dosing_RO2}}</td>
					<td>{{$visit->biocide_tank_level_RO1}}</td>
					<td>{{$visit->biocide_tank_level_RO2}}</td>
				</tr>
				</tbody>
				<thead>
				<tr>
					<th><u>Post Treatment</u></th>
					<th colspan="5"></th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Caustic</th>
					<td>mg/lit</td>
					<td>{{$visit->Caustic_dosing_RO1}}</td>
					<td>{{$visit->Caustic_dosing_RO2}}</td>
					<td>{{$visit->Caustic_tank_level_RO1}}</td>
					<td>{{$visit->Caustic_tank_level_RO2}}</td>
				</tr>
				<tr>
					<th>Sodium Hypochlorite</th>
					<td>mg/lit</td>
					<td>{{$visit->sodium_hypochlorite_dosing_RO1}}</td>
					<td>{{$visit->sodium_hypochlorite_dosing_RO2}}</td>
					<td>{{$visit->sodium_hypochlorite_tank_level_RO1}}</td>
					<td>{{$visit->sodium_hypochlorite_tank_level_RO2}}</td>
				</tr>
				</tbody>
			</table>
			<table class="table table-condensed table-bordered">
				<thead>
				<tr>
					<th>PRESSURE</th>
					<th>Units</th>
					<th>RO 1 (Before CIP)</th>
					<th>RO 2 (After CIP)</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Filter Cartridge (In)</th>
					<td>psig/bar</td>
					<td>{{$visit->filter_cartridge_in_RO1}}</td>
					<td>{{$visit->filter_cartridge_in_RO2}}</td>
				</tr>
				<tr>
					<th>Filter Cartridge (Out)</th>
					<td>psig/bar</td>
					<td>{{$visit->filter_cartridge_out_RO1}}</td>
					<td>{{$visit->filter_cartridge_out_RO2}}</td>
				</tr>
				<tr>
					<th>Differential Pressure</th>
					<td>psig/bar</td>
					<td>{{$visit->differential_pressure_pressure_RO1}}</td>
					<td>{{$visit->differential_pressure_pressure_RO2}}</td>
				</tr>
				<tr>
					<th>RO Pump Discharge Pressure</th>
					<td>psig/bar</td>
					<td>{{$visit->pump_discharge_pressure_RO1}}</td>
					<td>{{$visit->pump_discharge_pressure_RO2}}</td>
				</tr>
				<tr>
					<th>RO Membrane Feed Pressure (A)</th>
					<td>psig/bar</td>
					<td>{{$visit->membrane_feed_pressure_a_RO1}}</td>
					<td>{{$visit->membrane_feed_pressure_a_RO2}}</td>
				</tr>
				<tr>
					<th>Inter Stage Pressure(B)</th>
					<td>psig/bar</td>
					<td>{{$visit->inter_stage_pressure_b_RO1}}</td>
					<td>{{$visit->inter_stage_pressure_b_RO2}}</td>
				</tr>
				<tr>
					<th>RO Reject Pressure ©</th>
					<td>psig/bar</td>
					<td>{{$visit->reject_pressure_RO1}}</td>
					<td>{{$visit->reject_pressure_RO2}}</td>
				</tr>
				<tr>
					<th>Differential Pressure(A-B)</th>
					<td>psig/bar</td>
					<td>{{$visit->differential_pressure_a_b_RO1}}</td>
					<td>{{$visit->differential_pressure_a_b_RO2}}</td>
				</tr>
				<tr>
					<th>Differential Pressure (B-C)</th>
					<td>psig/bar</td>
					<td>{{$visit->differential_pressure_b_c_RO1}}</td>
					<td>{{$visit->differential_pressure_b_c_RO2}}</td>
				</tr>
				<tr>
					<th>Differential Pressure(A-C)</th>
					<td>psig/bar</td>
					<td>{{$visit->differential_pressure_a_c_RO1}}</td>
					<td>{{$visit->differential_pressure_a_c_RO2}}</td>
				</tr>
				<tr>
					<th>Total</th>
					<td colspan="3"></td>
				</tr>
				</tbody>

				<thead>
				<tr>
					<th><u>FLOWS</u></th>
					<th colspan="3"></th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Product Flow (1)</th>
					<td>psig/bar</td>
					<td>{{$visit->product_flow_RO1}}</td>
					<td>{{$visit->product_flow_RO2}}</td>
				</tr>
				<tr>
					<th>Reject Flow (2)</th>
					<td>psig/bar</td>
					<td>{{$visit->reject_flow_RO1}}</td>
					<td>{{$visit->reject_flow_RO2}}</td>
				</tr>
				<tr>
					<th>Feed Flow (1+2)</th>
					<td>psig/bar</td>
					<td>{{$visit->feed_flow_RO1}}</td>
					<td>{{$visit->feed_flow_RO2}}</td>
				</tr>
				<tr>
					<th>1st Stage Product Flow</th>
					<td>psig/bar</td>
					<td>{{$visit->stage_product_flow_RO1}}</td>
					<td>{{$visit->stage_product_flow_RO2}}</td>
				</tr>
				</tbody>

				<thead>
				<tr>
					<th><u>MISCELLANEOUS</u></th>
					<th colspan="3"></th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th>Recovery</th>
					<td>%</td>
					<td>{{$visit->recovery_RO1}}</td>
					<td>{{$visit->recovery_RO2}}</td>
				</tr>
				<tr>
					<th>Salt Passage</th>
					<td>%</td>
					<td>{{$visit->salt_passage_RO1}}</td>
					<td>{{$visit->salt_passage_RO2}}</td>
				</tr>
				<tr>
					<th>Rejection</th>
					<td>%</td>
					<td>{{$visit->rejection_RO1}}</td>
					<td>{{$visit->rejection_RO2}}</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row" style="margin-top: -15px">
		<div class="col-xs-12">
			<div class="col-xs-6 pull-left">
				{!! Form::label('comments', __('visit.comments') . ':') !!}
				<p>{{$visit->comments}}</p>
			</div>
			<div class="col-xs-6 pull-right ">
				{!! Form::label('recommendations', __('visit.recommendations') . ':') !!}
				<p>{{$visit->recommendations}}</p>
			</div>
		</div>
	</div>
</div>

<script></script>
