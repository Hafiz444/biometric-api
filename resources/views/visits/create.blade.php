@extends('layouts.app')
@section('title', __('visit.add_new_visit'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('visit.add_new_visit')</h1>
</section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(['url' => action('VisitController@store'), 'method' => 'post', 'id' => 'visit_add_form','class' => 'product_form', 'files' => true ]) !!}
            @component('components.widget', ['class' => 'box-primary'])
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('customer_id', __('visit.customer_name') . ':*') !!}
                            {!! Form::select('customer_id', $customers, null, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2', 'required']); !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('customer_business', __('visit.customer_business') . ':') !!}
                            {!! Form::text('customer_business', null, ['placeholder' => 'Business Name', 'class' => 'form-control']); !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('customer_phone', __('visit.phone_no') . ':') !!}
                            {!! Form::text('customer_phone', null, ['placeholder' => __('visit.phone_no'), 'class' => 'form-control']); !!}
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('email', __('visit.email') . ':') !!}
                            {!! Form::text('email', null, ['placeholder' => __('visit.email'), 'class' => 'form-control']); !!}
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('designation', __('visit.designation') . ':') !!}
                            {!! Form::text('designation', null, ['placeholder' => __('visit.designation'), 'class' => 'form-control']); !!}
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('plant_capacity', __('visit.plant_capacity') . ':') !!}
                            {!! Form::text('plant_capacity', null, ['placeholder' => __('visit.plant_capacity'), 'class' => 'form-control']); !!}
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('date_time', __('visit.date_time') . ':*') !!}
                            {!! Form::text('date_time', $default_datetime, [ 'class' => 'form-control', 'readonly', 'required']); !!}
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('location_id', __('visit.business_locations') . ':*') !!}
                            {!! Form::select('location_id', $business_locations, null, ['placeholder' => __('messages.please_select'), 'class' => 'form-control select2', 'required']); !!}
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            {!! Form::label('address', __('visit.address') . ':') !!}
                            {!! Form::text('address', null, ['placeholder' => __('visit.address'), 'class' => 'form-control']); !!}
                        </div>
                    </div>
                </div>
            @endcomponent

            <section class="content-header">
                <h1>@lang('visit.visit_header')</h1>
            </section>
            <br>
            @component('components.widget', ['class' => 'box-primary'])
                <div class="row">
                    <div class="col-md-8">

                        <!--Pre-Treatment table-->
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Pre-Treatment</th>
                                <th>Units</th>
                                <th colspan="2" class="text-center">Reading</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th class="text-center">RO 1 (Before CIP)</th>
                                <th class="text-center">RO 2 (After CIP)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Hours Meter</th>
                                <td>hrs</td>
                                <td><input type="text" name="hours_meter_RO1" class="form-control"></td>
                                <td><input type="text" name="hours_meter_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Water Temperature</th>
                                <td>C</td>
                                <td><input type="text" name="water_temperature_RO1" class="form-control"></td>
                                <td><input type="text" name="water_temperature_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Multimedia Filter inlet pressure (A)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="multimedia_filter_inlet_RO1" class="form-control"></td>
                                <td><input type="text" name="multimedia_filter_inlet_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Multimedia Filter outlet pressure (A)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="multimedia_filter_outlet_RO1" class="form-control"></td>
                                <td><input type="text" name="multimedia_filter_outlet_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Differential Pressure</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="differential_pressure_RO1" class="form-control"></td>
                                <td><input type="text" name="differential_pressure_RO2" class="form-control"></td>
                            </tr>
                            </tbody>

                            <thead>
                            <tr>
                                <th><u>FEED WATER</u></th>
                                <th></th>
                                <th colspan="2"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>PH</th>
                                <td></td>
                                <td><input type="text" name="ph_feed_RO1" class="form-control"></td>
                                <td><input type="text" name="ph_feed_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>TDS/Conductivity</th>
                                <td>mg/lit/us/cm</td>
                                <td><input type="text" name="tds_conductivity_RO1" class="form-control"></td>
                                <td><input type="text" name="tds_conductivity_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Free Chlorine</th>
                                <td>mg/lit/us/cm</td>
                                <td><input type="text" name="free_chlorine_RO1" class="form-control"></td>
                                <td><input type="text" name="free_chlorine_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Iron</th>
                                <td>mg/lit/us/cm</td>
                                <td><input type="text" name="iron_RO1" class="form-control"></td>
                                <td><input type="text" name="iron_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>SDI Before Cartridge Filter</th>
                                <td></td>
                                <td><input type="text" name="sdi_before_cartridge_RO1" class="form-control"></td>
                                <td><input type="text" name="sdi_before_cartridge_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>SDI After Cartridge Filter</th>
                                <td></td>
                                <td><input type="text" name="sdi_after_cartridge_RO1" class="form-control"></td>
                                <td><input type="text" name="sdi_after_cartridge_RO2" class="form-control"></td>
                            </tr>
                            </tbody>

                            <thead>
                            <tr>
                                <th><u>PRODUCT WATER</u></th>
                                <th></th>
                                <th colspan="2"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>PH</th>
                                <td></td>
                                <td><input type="text" name="ph_product_water_RO1" class="form-control"></td>
                                <td><input type="text" name="ph_product_water_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>TDS/Conductivity</th>
                                <td>mg/lit/us/cm</td>
                                <td><input type="text" name="tds_conductivity_product_water_RO1" class="form-control"></td>
                                <td><input type="text" name="tds_conductivity_product_water_RO2" class="form-control"></td>
                            </tr>
                            </tbody>

                            <thead>
                            <tr>
                                <th><u>REJECT WATER</u></th>
                                <th></th>
                                <th colspan="2"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>PH</th>
                                <td></td>
                                <td><input type="text" name="ph_reject_water_RO1" class="form-control"></td>
                                <td><input type="text" name="ph_reject_water_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>TDS/Conductivity</th>
                                <td>mg/lit</td>
                                <td><input type="text" name="tds_conductivity_RO1" class="form-control"></td>
                                <td><input type="text" name="tds_conductivity_RO2" class="form-control"></td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Chemical Dosing</th>
                                <th>Stock</th>
                                <th colspan="2" class="text-center">Dosing</th>
                                <th colspan="2" class="text-center">Tank Level</th>
                            </tr>
                            <tr>
                                <th>Pre-Treatment</th>
                                <th></th>
                                <th class="text-center">RO 1 (Before CIP)</th>
                                <th class="text-center">RO 2 (After CIP)</th>
                                <th class="text-center">RO 1 (Before CIP)</th>
                                <th class="text-center">RO 2 (After CIP)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Sodium Hypochlorite</th>
                                <td></td>
                                <td><input type="text" name="sodium_hypochlorite_dosing_RO1" class="form-control"></td>
                                <td><input type="text" name="sodium_hypochlorite_dosing_RO2" class="form-control"></td>
                                <td><input type="text" name="sodium_hypochlorite_tank_level_RO1" class="form-control"></td>
                                <td><input type="text" name="sodium_hypochlorite_tank_level_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Polygard 600</th>
                                <td></td>
                                <td><input type="text" name="polygard_600_dosing_RO1" class="form-control"></td>
                                <td><input type="text" name="polygard_600_dosing_RO2" class="form-control"></td>
                                <td><input type="text" name="polygard_600_tank_level_RO1" class="form-control"></td>
                                <td><input type="text" name="polygard_600_tank_level_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Sodium Metabisulphite</th>
                                <td></td>
                                <td><input type="text" name="sodium_metabisulphite_dosing_RO1" class="form-control"></td>
                                <td><input type="text" name="sodium_metabisulphite_dosing_RO2" class="form-control"></td>
                                <td><input type="text" name="sodium_metabisulphite_tank_level_RO1" class="form-control"></td>
                                <td><input type="text" name="sodium_metabisulphite_tank_level_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Acid H2SO4 / HCL</th>
                                <td></td>
                                <td><input type="text" name="acid_dosing_RO1" class="form-control"></td>
                                <td><input type="text" name="acid_dosing_RO2" class="form-control"></td>
                                <td><input type="text" name="acid_tank_level_RO1" class="form-control"></td>
                                <td><input type="text" name="acid_tank_level_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Biocide R-22/R-22</th>
                                <td></td>
                                <td><input type="text" name="biocide_dosing_RO1" class="form-control"></td>
                                <td><input type="text" name="biocide_dosing_RO2" class="form-control"></td>
                                <td><input type="text" name="biocide_tank_level_RO1" class="form-control"></td>
                                <td><input type="text" name="biocide_tank_level_RO2" class="form-control"></td>
                            </tr>
                            </tbody>
                            <thead>
                            <tr>
                                <th><u>Post Treatment</u></th>
                                <th colspan="5"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Caustic</th>
                                <td>mg/lit</td>
                                <td><input type="text" name="Caustic_dosing_RO1" class="form-control"></td>
                                <td><input type="text" name="Caustic_dosing_RO2" class="form-control"></td>
                                <td><input type="text" name="Caustic_tank_level_RO1" class="form-control"></td>
                                <td><input type="text" name="Caustic_tank_level_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Sodium Hypochlorite</th>
                                <td>mg/lit</td>
                                <td><input type="text" name="sodium_hypochlorite_dosing_RO1" class="form-control"></td>
                                <td><input type="text" name="sodium_hypochlorite_dosing_RO2" class="form-control"></td>
                                <td><input type="text" name="sodium_hypochlorite_tank_level_RO1" class="form-control"></td>
                                <td><input type="text" name="sodium_hypochlorite_tank_level_RO2" class="form-control"></td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>PRESSURE</th>
                                <th>Units</th>
                                <th>RO 1 (Before CIP)</th>
                                <th>RO 2 (After CIP)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Filter Cartridge (In)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="filter_cartridge_in_RO1" class="form-control"></td>
                                <td><input type="text" name="filter_cartridge_in_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Filter Cartridge (Out)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="filter_cartridge_out_RO1" class="form-control"></td>
                                <td><input type="text" name="filter_cartridge_out_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Differential Pressure</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="differential_pressure_pressure_RO1" class="form-control"></td>
                                <td><input type="text" name="differential_pressure_pressure_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>RO Pump Discharge Pressure</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="pump_discharge_pressure_RO1" class="form-control"></td>
                                <td><input type="text" name="pump_discharge_pressure_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>RO Membrane Feed Pressure (A)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="membrane_feed_pressure_a_RO1" class="form-control"></td>
                                <td><input type="text" name="membrane_feed_pressure_a_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Inter Stage Pressure(B)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="inter_stage_pressure_b_RO1" class="form-control"></td>
                                <td><input type="text" name="inter_stage_pressure_b_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>RO Reject Pressure (C)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="reject_pressure_RO1" class="form-control"></td>
                                <td><input type="text" name="reject_pressure_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Differential Pressure(A-B)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="differential_pressure_a_b_RO1" class="form-control"></td>
                                <td><input type="text" name="differential_pressure_a_b_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Differential Pressure (B-C)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="differential_pressure_b_c_RO1" class="form-control"></td>
                                <td><input type="text" name="differential_pressure_b_c_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Differential Pressure(A-C)</th>
                                <td>psig/bar</td>
                                <td><input type="text" name="differential_pressure_a_c_RO1" class="form-control"></td>
                                <td><input type="text" name="differential_pressure_a_c_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td colspan="3"></td>
                            </tr>
                            </tbody>

                            <thead>
                            <tr>
                                <th><u>FLOWS</u></th>
                                <th colspan="3"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Product Flow (1)</th>
                                <td>gpm/m3/hr</td>
                                <td><input type="text" name="product_flow_RO1" class="form-control"></td>
                                <td><input type="text" name="product_flow_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Reject Flow (2)</th>
                                <td>gpm/m3/hr</td>
                                <td><input type="text" name="reject_flow_RO1" class="form-control"></td>
                                <td><input type="text" name="reject_flow_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Feed Flow (1+2)</th>
                                <td>gpm/m3/hr</td>
                                <td><input type="text" name="feed_flow_RO1" class="form-control"></td>
                                <td><input type="text" name="feed_flow_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>1st Stage Product Flow</th>
                                <td>gpm/m3/hr</td>
                                <td><input type="text" name="stage_product_flow_RO1" class="form-control"></td>
                                <td><input type="text" name="stage_product_flow_RO2" class="form-control"></td>
                            </tr>
                            </tbody>

                            <thead>
                            <tr>
                                <th><u>MISCELLANEOUS</u></th>
                                <th colspan="3"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>Recovery</th>
                                <td>%</td>
                                <td><input type="text" name="recovery_RO1" class="form-control"></td>
                                <td><input type="text" name="recovery_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Salt Passage</th>
                                <td>%</td>
                                <td><input type="text" name="salt_passage_RO1" class="form-control"></td>
                                <td><input type="text" name="salt_passage_RO2" class="form-control"></td>
                            </tr>
                            <tr>
                                <th>Rejection</th>
                                <td>%</td>
                                <td><input type="text" name="rejection_RO1" class="form-control"></td>
                                <td><input type="text" name="rejection_RO2" class="form-control"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            @endcomponent

            <section class="content-header">
                <h1>@lang('visit.comments_recommendations')</h1>
            </section>
            <br>
            @component('components.widget', ['class' => 'box-primary'])
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('comments', __('visit.comments') . ':') !!}
                            {!! Form::textarea('comments', null, ['placeholder' => __('visit.comments'), 'class' => 'form-control']); !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('recommendations', __('visit.recommendations') . ':') !!}
                            {!! Form::textarea('recommendations', null, ['placeholder' => __('visit.recommendations'), 'class' => 'form-control']); !!}
                        </div>
                    </div>
                </div>
            @endcomponent

            <div class="row">
                <div class="col-sm-12 text-right">
                    <button type="submit" id="visit_submit" class="btn btn-primary btn-flat">@lang('messages.save')</button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
    <!-- /.content -->
@endsection

@section('javascript')
  <script src="{{ asset('js/visit.js?v=' . $asset_v) }}"></script>
@endsection