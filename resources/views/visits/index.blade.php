@extends('layouts.app')
@section('title', __( 'visit.visit_list'))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header no-print">
        <h1>@lang( 'visit.visit_list')
        </h1>
    </section>

    <!-- Main content -->
    <section class="content no-print">
        @component('components.filters', ['title' => __('report.filters')])
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('visit_list_filter_location_id',  __('purchase.business_location') . ':') !!}

                    {!! Form::select('visit_list_filter_location_id', $business_locations, null, ['class' => 'form-control select2', 'style' => 'width:100%', 'placeholder' => __('lang_v1.all') ]); !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('visit_list_filter_customer_id',  __('contact.customer') . ':') !!}
                    {!! Form::select('visit_list_filter_customer_id', $customers, null, ['class' => 'form-control select2', 'style' => 'width:100%', 'placeholder' => __('lang_v1.all')]); !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    {!! Form::label('visit_list_filter_date_range', __('report.date_range') . ':') !!}
                    {!! Form::text('visit_list_filter_date_range', null, ['placeholder' => __('lang_v1.select_a_date_range'), 'class' => 'form-control', 'readonly']); !!}
                </div>
            </div>
        @endcomponent
        @component('components.widget', ['class' => 'box-primary', 'title' => __( 'visit.visit_list')])
            @can('visits.create')
                @slot('tool')
                    <div class="box-tools">
                        <a class="btn btn-block btn-primary" href="{{action('VisitController@create')}}"><i class="fa fa-plus"></i> @lang('messages.add')</a>
                    </div>
                @endslot
            @endcan

                <table class="table table-bordered table-striped ajax_view" id="visit_table">
                    <thead>
                    <tr>
                        <th>Sr. #</th>
                        <th>@lang('visit.customer_name')</th>
                        <th>@lang('visit.customer_business')</th>
                        <th>@lang('visit.phone_no')</th>
                        <th>@lang('visit.date_time')</th>
                        <th>@lang('sale.location')</th>
                        <th>@lang('messages.action')</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>

                    </tfoot>
                </table>

        @endcomponent
    </section>
    <!-- /.content -->
@stop

@section('javascript')
    <script src="{{ asset('js/visit.js?v=' . $asset_v) }}"></script>
@endsection