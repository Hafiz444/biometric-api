@extends('layouts.app')
@section('title', __('home.home'))

@section('content')

    <!-- Content Header (Page header) -->
    {{--<section class="content-header content-header-custom">
        <h1>{{ __('home.welcome_message', ['name' => Session::get('user.first_name')]) }}
        </h1>
    </section>--}}
    @if(auth()->user()->can('dashboard.data'))
        <!-- Main content -->
        <section class="content content-custom no-print">
            <br>
            <div class="row">

            </div>
            <br>
            <div class="row row-custom">
                <div class="col-md-4 col-xs-12">
                    @if(count($all_locations) > 1)
                        {!! Form::select('dashboard_location', $all_locations, null, ['class' => 'form-control select2', 'placeholder' => __('lang_v1.select_location'), 'id' => 'dashboard_location']); !!}
                    @endif
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="btn-group pull-right" data-toggle="buttons">
                        <label class="btn btn-info active">
                            <input type="radio" name="date-filter"
                                   data-start="{{ date('Y-m-d') }}"
                                   data-end="{{ date('Y-m-d') }}"
                                   checked> {{ __('home.today') }}
                        </label>
                        <label class="btn btn-info">
                            <input type="radio" name="date-filter"
                                   data-start="{{ $date_filters['this_week']['start']}}"
                                   data-end="{{ $date_filters['this_week']['end']}}"
                            > {{ __('home.this_week') }}
                        </label>
                        <label class="btn btn-info">
                            <input type="radio" name="date-filter"
                                   data-start="{{ $date_filters['this_month']['start']}}"
                                   data-end="{{ $date_filters['this_month']['end']}}"
                            > {{ __('home.this_month') }}
                        </label>
                        <label class="btn btn-info">
                            <input type="radio" name="date-filter"
                                   data-start="{{ $date_filters['this_fy']['start']}}"
                                   data-end="{{ $date_filters['this_fy']['end']}}"
                            > {{ __('home.this_fy') }}
                        </label>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 col-custom">
                    <div class="info-box info-box-new-style">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-cash"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text text-black">{{ __('home.total_purchase') }}</span>
                            <span class="info-box-number total_purchase"><i class="fas fa-sync fa-spin fa-fw margin-bottom"></i></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12 col-custom">
                    <div class="info-box info-box-new-style">
                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cart-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text text-black">{{ __('home.total_sell') }}</span>
                            <span class="info-box-number total_sell"><i class="fas fa-sync fa-spin fa-fw margin-bottom"></i></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12 col-custom">
                    <div class="info-box info-box-new-style">
	        <span class="info-box-icon bg-yellow">
	        	<i class="fa fa-dollar"></i>
				<i class="fa fa-exclamation"></i>
	        </span>

                        <div class="info-box-content">
                            <span class="info-box-text text-black">{{ __('home.purchase_due') }}</span>
                            <span class="info-box-number purchase_due"><i class="fas fa-sync fa-spin fa-fw margin-bottom"></i></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <!-- <div class="clearfix visible-sm-block"></div> -->
                <div class="col-md-3 col-sm-6 col-xs-12 col-custom">
                    <div class="info-box info-box-new-style">
	        <span class="info-box-icon bg-yellow">
	        	<i class="ion ion-ios-paper-outline"></i>
	        	<i class="fa fa-exclamation"></i>
	        </span>

                        <div class="info-box-content">
                            <span class="info-box-text text-black">{{ __('home.invoice_due') }}</span>
                            <span class="info-box-number invoice_due"><i class="fas fa-sync fa-spin fa-fw margin-bottom"></i></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <div class="row row-custom">

                <!-- Add Products -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-aqua-gradient" style="color: #fff;" href="/products/create">
                        <span class="info-box-number text-center" style="margin-top: 30px;"><i class="fa fa-plus"></i> {{ __('product.add_product') }}</span>
                    </a>
                </div>

                <!-- Add Purchase -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-red-gradient" style="color: #fff;" href="/purchases/create">
                        <span class="info-box-number text-center" style="margin-top: 30px;"><i class="fa fa-plus"></i> {{ __('purchase.add_purchase') }}</span>
                    </a>
                </div>

                <!-- Add Sales -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-green-gradient" style="color: #fff;" href="/sells/create">
                        <span class="info-box-number text-center" style="margin-top: 30px;"><i class="fa fa-plus"></i> {{ __('sale.add_sale') }}</span>
                    </a>
                </div>

                <!-- Add Supplier -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a href="#" class="info-box bg-light-blue-gradient btn-modal"
                       data-href="{{action('ContactController@create', ['type' => 'supplier'])}}"
                       data-container=".contact_modal" style="">
                        <span class="info-box-number text-center" style="margin-top: 30px;"><i class="fa fa-plus"></i> {{ __('lang_v1.add_supplier') }}</span> </a>
                </div>

                <!-- Add Customer -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a href="#" class="info-box bg-yellow-gradient btn-modal"
                       data-href="{{action('ContactController@create', ['type' => 'customer'])}}"
                       data-container=".contact_modal" style="">
                        <span class="info-box-number text-center" style="margin-top: 30px;"><i class="fa fa-plus"></i> {{ __('lang_v1.add_customer') }}</span> </a>
                </div>

            @php
                $business_id = auth()->user()->business_id;
                $subscription = \Modules\Superadmin\Entities\Subscription::where('business_id', $business_id)->first();
            @endphp

            <!-- Add Journal Voucher -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-teal-gradient" style="color: #fff;"
                       @if(auth()->user()->can('account.access') && in_array('account', $enabled_modules))
                       @if(!empty($journal_voucher_link))
                       href="{{action($journal_voucher_link)}}"
                       @else
                       href="#"
                            @endif
                            @endif
                    >
                        <span class="info-box-number text-center" style="margin-top: 30px;"><i class="fa fa-plus"></i> {{ __('lang_v1.add_jv') }}</span>
                    </a>
                </div>

            </div>
            <div class="row row-custom">
                <!-- products -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-aqua-gradient" style="color: #fff;" href="/products">
                        <span class="info-box-number text-center" style="margin-top: 30px;">{{ __('product.products') }}</span>
                    </a>
                </div>

                <!-- purchase -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-red-gradient" style="color: #fff;" href="/purchases">
                        <span class="info-box-number text-center" style="margin-top: 30px;">{{ __('purchase.purchases') }}</span>
                    </a>
                </div>

                <!-- Sales -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-green-gradient" style="color: #fff;" href="/sells">
                        <span class="info-box-number text-center" style="margin-top: 30px;">{{ __('sale.sells') }}</span>
                    </a>
                </div>

                <!-- Supplier -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-light-blue-gradient" style="color: #fff;" href="{{action('ContactController@index', ['type' => 'supplier'])}}">
                        <span class="info-box-number text-center" style="margin-top: 30px;">{{ __('report.supplier') }}</span>
                    </a>
                </div>

                <!-- Customer -->
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-yellow-gradient" style="color: #fff;" href="{{action('ContactController@index', ['type' => 'customer'])}}">
                        <span class="info-box-number text-center" style="margin-top: 30px;">{{ __('report.customer') }}</span>
                    </a>
                </div>

                <!-- Accounts -->

                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a class="info-box bg-teal-gradient" style="color: #fff;"
                       @if(auth()->user()->can('account.access') && in_array('account', $enabled_modules))
                       href="/account/account"
                       @else
                       href="#"
                            @endif
                    >
                        <span class="info-box-number text-center" style="margin-top: 30px;">{{ __('account.accounts') }}</span>
                    </a>
                </div>


            </div>
        @if(!empty($widgets['after_sale_purchase_totals']))
            @foreach($widgets['after_sale_purchase_totals'] as $widget)
                {!! $widget !!}
            @endforeach
        @endif

        @if(!empty($all_locations))
            <!-- purchase chart start -->
                <div class="row">
                    <div class="col-sm-6" style="text-align: center">
                        @component('components.widget', ['class' => 'box-danger', 'title' => __('home.purchases_last_30_days')])
                            {!! $purchases_chart_1->container() !!}
                        @endcomponent
                    </div>

                @endif
                @if(!empty($widgets['after_purchases_last_30_days']))
                    @foreach($widgets['after_purchases_last_30_days'] as $widget)
                        {!! $widget !!}
                    @endforeach
                @endif


                @if(!empty($all_locations))
                    <!-- sales chart start -->
                        <div class="row">
                            <div class="col-sm-6" style="text-align: center">
                                @component('components.widget', ['class' => 'box-success', 'title' => __('home.sells_last_30_days')])
                                    {!! $sells_chart_1->container() !!}
                                @endcomponent
                            </div>

                        @endif
                        @if(!empty($widgets['after_sales_last_30_days']))
                            @foreach($widgets['after_sales_last_30_days'] as $widget)
                                {!! $widget !!}
                            @endforeach
                        @endif



                        {{--@if(!empty($all_locations))

                              <div class="col-sm-6">
                                @component('components.widget', ['class' => 'box-primary', 'title' => __('home.sells_current_fy')])
                                  {!! $sells_chart_2->container() !!}
                                @endcomponent
                              </div>
                          </div>
                        @endif--}}
                        <!-- sales chart end -->
                            {{--@if(!empty($widgets['after_sales_current_fy']))
                              @foreach($widgets['after_sales_current_fy'] as $widget)
                                {!! $widget !!}
                              @endforeach
                            @endif--}}

                            {{--<!-- products less than alert quntity -->
                            <div class="row">

                            <div class="col-sm-6">
                              @component('components.widget', ['class' => 'box-warning'])
                                @slot('icon')
                                  <i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true"></i>
                                @endslot
                                @slot('title')
                                  {{ __('lang_v1.sales_payment_dues') }} @show_tooltip(__('lang_v1.tooltip_sales_payment_dues'))
                                @endslot
                                <table class="table table-bordered table-striped" id="sales_payment_dues_table">
                                  <thead>
                                    <tr>
                                      <th>@lang( 'contact.customer' )</th>
                                      <th>@lang( 'sale.invoice_no' )</th>
                                      <th>@lang( 'home.due_amount' )</th>
                                    </tr>
                                  </thead>
                                </table>
                              @endcomponent
                            </div>

                                <div class="col-sm-6">

                              @component('components.widget', ['class' => 'box-warning'])
                                @slot('icon')
                                  <i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true"></i>
                                @endslot
                                @slot('title')
                                  {{ __('lang_v1.purchase_payment_dues') }} @show_tooltip(__('tooltip.payment_dues'))
                                @endslot
                                <table class="table table-bordered table-striped" id="purchase_payment_dues_table">
                                  <thead>
                                    <tr>
                                      <th>@lang( 'purchase.supplier' )</th>
                                      <th>@lang( 'purchase.ref_no' )</th>
                                              <th>@lang( 'home.due_amount' )</th>
                                    </tr>
                                  </thead>
                                </table>
                              @endcomponent

                                </div>
                          </div>--}}

                            <div class="row">

                                <div class="col-sm-6">
                                    @component('components.widget', ['class' => 'box-warning'])
                                        @slot('icon')
                                            <i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true"></i>
                                        @endslot
                                        @slot('title')
                                            {{ __('home.product_stock_alert') }} @show_tooltip(__('tooltip.product_stock_alert'))
                                        @endslot
                                        <table class="table table-bordered table-striped" id="stock_alert_table">
                                            <thead>
                                            <tr>
                                                <th>@lang( 'sale.product' )</th>
                                                <th>@lang( 'business.location' )</th>
                                                <th>@lang( 'report.current_stock' )</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    @endcomponent
                                </div>
                                @can('stock_report.view')
                                    @if(session('business.enable_product_expiry') == 1)
                                        <div class="col-sm-6">
                                            @component('components.widget', ['class' => 'box-warning'])
                                                @slot('icon')
                                                    <i class="fa fa-exclamation-triangle text-yellow" aria-hidden="true"></i>
                                                @endslot
                                                @slot('title')
                                                    {{ __('home.stock_expiry_alert') }} @show_tooltip( __('tooltip.stock_expiry_alert', [ 'days' =>session('business.stock_expiry_alert_days', 30) ]) )
                                                @endslot
                                                <input type="hidden" id="stock_expiry_alert_days" value="{{ \Carbon::now()->addDays(session('business.stock_expiry_alert_days', 30))->format('Y-m-d') }}">
                                                <table class="table table-bordered table-striped" id="stock_expiry_alert_table">
                                                    <thead>
                                                    <tr>
                                                        <th>@lang('business.product')</th>
                                                        <th>@lang('business.location')</th>
                                                        <th>@lang('report.stock_left')</th>
                                                        <th>@lang('product.expires_in')</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            @endcomponent
                                        </div>
                                    @endif
                                @endcan
                            </div>
                            <div class="modal fade contact_modal" tabindex="-1" role="dialog"
                                 aria-labelledby="gridSystemModalLabel">
                            </div>

                @if(!empty($widgets['after_dashboard_reports']))
                    @foreach($widgets['after_dashboard_reports'] as $widget)
                        {!! $widget !!}
                    @endforeach
                @endif
        </section>
        <!-- /.content -->
@stop
@section('javascript')
    <script src="{{ asset('js/home.js?v=' . $asset_v) }}"></script>
    @if(!empty($all_locations))
        {!! $sells_chart_1->script() !!}
        {!! $purchases_chart_1->script() !!}
    @endif
    @endif
    <script type="text/javascript">
        $(document).on('shown.bs.modal', '.contact_modal', function(e) {
            initAutocomplete();
        });
    </script>
@endsection

