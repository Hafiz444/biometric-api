{!! NoCaptcha::renderJs() !!}
{!! Form::hidden('language', request()->lang); !!}

<fieldset>
    <legend></legend>
    <div class="col-md-6">
        {!! Form::label('name', __('business.business_name') . ':*' ) !!}
        <div class="form-group has-feedback">
            <span class="fa fa-suitcase form-control-feedback"></span>
            {!! Form::text('name', null, ['class' => 'form-control','placeholder' => __('business.business_name'), 'required']); !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('mobile', __('lang_v1.business_telephone') . ':') !!}
        <div class="form-group has-feedback">
            <span class="fa fa-phone form-control-feedback"></span>
            {!! Form::text('mobile', null, ['class' => 'form-control','placeholder' => __('lang_v1.business_telephone')]); !!}
        </div>
    </div>

    <div class="clearfix"></div>
    {!! Form::hidden('start_date',Carbon\Carbon::now()->format("d/m/Y")); !!}
    <div class="col-md-6">
        {!! Form::label('email', __('business.email') . ':*') !!}
        <div class="form-group has-feedback">
            <span class="fa fa-envelope form-control-feedback"></span>
            {!! Form::text('email', null, ['class' => 'form-control','placeholder' => __('business.email'), 'required']); !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('currency_id', __('business.currency') . ':*') !!}
        <div class="form-group has-feedback">
            <select class="form-control select2" name="currency_id" id="" required>
                @foreach($currencies as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
            <span class="fas fa-money-bill-alt form-control-feedback"></span>
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('first_name', __('business.name') . ':*') !!}
        <div class="form-group has-feedback">
            <span class="fa fa-info form-control-feedback"></span>
            {!! Form::text('first_name', null, ['class' => 'form-control','placeholder' => __('lang_v1.owner_name'), 'required']); !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('username', __('business.username') . ':*') !!}
        <div class="form-group has-feedback">
            <span class="fa fa-user form-control-feedback"></span>
            {!! Form::text('username', null, ['class' => 'form-control','placeholder' => __('business.username'), 'required']); !!}
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="col-md-6">
        {!! Form::label('password', __('business.password') . ':*') !!}
        <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            {!! Form::password('password', ['class' => 'form-control','placeholder' => __('business.password'), 'required']); !!}
        </div>
    </div>

    <div class="col-md-6">
        {!! Form::label('confirm_password', __('business.confirm_password') . ':*') !!}
        <div class="form-group has-feedback">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            {!! Form::password('confirm_password', ['class' => 'form-control','placeholder' => __('business.confirm_password'), 'required']); !!}
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        @if(!empty($system_settings['superadmin_enable_register_tc']))
            <div class="form-group">
                <label>
                    {!! Form::checkbox('accept_tc', 0, false, ['required', 'class' => 'input-icheck']); !!}
                    <u>
                        <a class="terms_condition cursor-pointer" data-toggle="modal" data-target="#tc_modal">
                            @lang('lang_v1.accept_terms_and_conditions')
                        </a>
                    </u>
                </label>
            </div>
            @include('business.partials.terms_conditions')
        @endif
        {!! NoCaptcha::display() !!}
    </div>
</fieldset>