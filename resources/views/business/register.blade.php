@extends('layouts.auth2')
@section('title', __('lang_v1.register'))

<style>
    /*input[type=text], input[type=password], input[type=select] {
        background: transparent;
        border: none;
        border-bottom: 1px solid #000000;
    }*/
    @media (min-width: 280px) and (max-width: 950px) {
        .mbl-d-none{
            display: none !important;
        }

    }
</style>
@section('content')
    <div class="col-md-12 right-col-content ui-widget-shadow radius" style="margin-top: 100px; border-radius: 15px;">

        {!! Form::open(['url' => route('business.postRegister'), 'method' => 'post', 'id' => 'business_register_form','files' => true ]) !!}

        <div class="col-md-8">
            @include('layouts.partials.logo')
            <p class="form-header">@lang('business.business_registration_form')</p>
            @include('business.partials.register_form')

            <br>
            <br>
            <button type="submit" class="btn btn-login btn-round" style="background-color: #28D25C; color: white">@lang('business.register')</button>
        </div>
        {!! Form::close() !!}
        <div class="col-md-4 col-xs-12 col-sm-12" id="left-sidebar" style="text-align: center">
            <img src="/uploads/signin.jpg" class="mbl-d-none center-block" alt="Logo" {{--style="display: none;"--}}>
            <br><br>
            <a href="{{ action('Auth\LoginController@login') }}@if(!empty(request()->lang)){{'?lang=' . request()->lang}} @endif" style="color: #28D25C">
                <b>{{ __('business.already_registered')}}</b> <u>{{ __('business.sign_in') }}</u></a>
        </div>
    </div>
@stop
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#change_lang').change( function(){
                window.location = "{{ route('business.getRegister') }}?lang=" + $(this).val();
            });
        })
        $("form").submit(function(event) {
            var recaptcha = $("#g-recaptcha-response").val();
            if (recaptcha === "") {
                event.preventDefault();
                toastr.error("Please check the recaptcha!");
            }
        });
    </script>
@endsection