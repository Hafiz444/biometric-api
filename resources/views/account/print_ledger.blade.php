<style>
	@media print {
		a[href]:after {
			content: none !important;
		}
		table * {
			font-size: 12px !important;
		}
	}
</style>
<div>
	<h2 style="text-align: center"><b>{{$business_name}}</b></h2>
	<h4 style="margin-bottom: -20px">@lang('account.account_name'): <b>{{$account_name}}</b></h4>
	<span class="pull-right">@lang('messages.from'): <b>{{$start_date}}</b> - @lang('messages.to'): <b>{{$end_date}}</b></span>
	<table class="table table-bordered table-striped" id="haha">
		<thead>
		<tr>
			<th>@lang( 'messages.date' )</th>
			<th>@lang( 'purchase.ref_no' )</th>
			<th>@lang( 'lang_v1.added_by' )</th>
			<th>@lang('account.debit')</th>
			<th>@lang('account.credit')</th>
			<th>@lang( 'lang_v1.balance' )</th>
		</tr>
		</thead>
		<tbody id="ledger"></tbody>
	</table>
</div>

<script>
	$(document).ready(function() {
		var ledger_data = $('#account_book tbody').html();
		$('#ledger').html(ledger_data);
	});
</script>
