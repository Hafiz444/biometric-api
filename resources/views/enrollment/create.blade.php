@extends('layouts.app')
@section('title', __( 'user.add_enrollment' ))
@section('content')

	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>@lang( 'user.add_enrollment' )</h1>
	</section>

	<!-- Main content -->
	<section class="content">
		{!! Form::open(['url' => action('EnrollmentController@store'), 'method' => 'post', 'id' => 'enrollment_add_form' ]) !!}
		@component('components.widget', ['class' => 'box-primary', 'title' => __('user.add_enrollment')])
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('device_id', __('device.device') . ':*') !!}
					<div class="form-group">
						{!! Form::select('device_id', $devices, null, ['class' => 'form-control select2', 'id' => 'device_id', 'style' => 'width: 100%;', 'required' ]); !!}
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('select_user', __('user.select_user') . ':*') !!}
					<div class="form-group">
						{!! Form::select('user_id', $users, null, ['class' => 'form-control select2', 'id' => 'user_id', 'style' => 'width: 100%;', 'required' ]); !!}
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('finger_id', __('device.finger_id') . ':*') !!}
					<div class="form-group">
						<select class="form-control select2" name="finger_id" id="finger_id" required>
							<option value="">@lang('messages.please_select')</option>
						</select>
					</div>
				</div>
			</div>
		@endcomponent

		<div class="row">
			<div class="col-md-12">
				<button type="submit" class="btn btn-primary pull-right" id="submit_enrollment_button">@lang( 'messages.save' )</button>
			</div>
		</div>
		{!! Form::close() !!}
	</section>
@stop

@section('javascript')
	<script type="text/javascript">

		// check required fields
		$(document).on('click', 'button#submit_enrollment_button', function(e) {
			e.preventDefault();

			if ($('form#enrollment_add_form').valid()) {
				$('form#enrollment_add_form').submit();
			}
		});

		$('#device_id').change( function(){
			var device_id = $( "#device_id" ).val();

			$.ajax({
				url: "/get_finger_ids",
				type: "POST",
				data: {
					device_id: device_id,
				},
				cache: false,
				success: function(result){
					var select = $('form select[name = finger_id]');
					select.empty();

					$.each(result,function(key, value) {
						select.append('<option value=' + value + '>' +'Finger # '+ value + '</option>');
					});
				}
			});

		});
	</script>
@endsection
