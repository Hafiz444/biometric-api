@extends('layouts.app')
@section('title', __( 'user.manage_enrollment' ))

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>@lang( 'user.users' )
            <small>@lang( 'user.manage_enrollment' )</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        @component('components.widget', ['class' => 'box-primary', 'title' => __( 'user.manage_enrollment' )])
            @can('user.create')
                @slot('tool')
                    <div class="box-tools">
                        <a class="btn btn-block btn-primary"
                           href="{{action('EnrollmentController@create')}}" >
                            <i class="fa fa-plus"></i> @lang( 'messages.add' )</a>
                    </div>
                @endslot
            @endcan
            @can('user.view')
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="users_table">
                        <thead>
                        <tr>
                            <th>@lang( 'user.user_id' )</th>
                            <th>@lang( 'user.name' )</th>
                            <th>@lang( 'business.email' )</th>
                            <th>@lang( 'device.device_name' )</th>
                            <th>@lang( 'device.finger_id' )</th>
                            <th>@lang( 'device.status' )</th>
                            <th>@lang( 'messages.action' )</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            @endcan
        @endcomponent
    </section>
    <!-- /.content -->
@stop
@section('javascript')
    <script type="text/javascript">
        $(document).ready( function(){
            var users_table = $('#users_table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/enrollments',
                columnDefs: [ {
                    "targets": [4],
                    "orderable": false,
                    "searchable": false
                } ],
                "columns":[
                    {"data":"user_id"},
                    {"data":"full_name"},
                    {"data":"email"},
                    {"data":"device_name"},
                    {"data":"finger_id"},
                    {"data":"enrollment_status"},
                    {"data":"action"}
                ]
            });
            $(document).on('click', 'button.delete_user_button', function(){
                swal({
                    title: LANG.sure,
                    text: LANG.confirm_delete_enrollment,
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        var href = $(this).data('href');
                        var data = $(this).serialize();
                        $.ajax({
                            method: "DELETE",
                            url: href,
                            dataType: "json",
                            data: data,
                            success: function(result){
                                if(result.success == true){
                                    toastr.success(result.msg);
                                    users_table.ajax.reload();
                                } else {
                                    toastr.error(result.msg);
                                }
                            }
                        });
                    }
                });
            });

        });


    </script>
@endsection
