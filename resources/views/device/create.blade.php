@extends('layouts.app')
@section('title', __('device.add_device'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('device.add_device')</h1>
</section>

<!-- Main content -->
<section class="content">
	{!! Form::open(['url' => action('DeviceController@store'), 'method' => 'post', 'id' => 'add_device_form', 'files' => true ]) !!}
	@component('components.widget', ['class' => 'box-primary'])
		<div class="row">

			@if(count($business_locations) == 1)
				@php
					$default_location = current(array_keys($business_locations->toArray()))
				@endphp
			@else
				@php $default_location = null; @endphp
			@endif
			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('location_id', __('purchase.business_location').':*') !!}
					{!! Form::select('location_id', $business_locations, $default_location, ['class' => 'form-control select2', 'placeholder' => __('messages.please_select'), 'required']); !!}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('device_name', __('device.device_name').':') !!}
					{!! Form::text('device_name', null, ['class' => 'form-control']); !!}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('device_location', __('device.device_location') . ':*') !!}
					{!! Form::select('device_location', ['inside' => __('device.inside'), 'outside' => __('device.outside')], null , ['class' => 'form-control select2','placeholder' => __('messages.please_select'), 'required']); !!}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('device_limit', __('device.device_limit') . ':*') !!}
					<select class="form-control select2" name="device_limit" id="device_limit" required>
						<option value="">@lang('messages.please_select')</option>
						@for($i=0; $i<=200; $i= $i+5)
							@if($i!=0)
								<option value="{{$i}}">{{$i}}</option>
							@endif
						@endfor
					</select>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('mac_address', __('device.mac_address') . ':*') !!}
					{!! Form::text('mac_address', null, ['class' => 'form-control', 'placeholder' => __('sale.total_amount'), 'required']); !!}
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					{!! Form::label('description', __('device.description') . ':') !!}
					{!! Form::textarea('description', null, ['class' => 'form-control', 'rows' => 3]); !!}
				</div>
			</div>
			<input type="hidden" name="status" value="0">
		</div>
	@endcomponent

	<div class="col-sm-12">
		<button type="submit" id="device_submit" class="btn btn-primary pull-right">@lang('messages.save')</button>
	</div>
{!! Form::close() !!}
</section>
@endsection
@section('javascript')
	<script src="{{ asset('js/device.js?v=' . $asset_v) }}"></script>
@endsection
