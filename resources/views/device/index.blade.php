@extends('layouts.app')
@section('title', __('device.devices'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('device.devices')</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @component('components.filters', ['title' => __('report.filters')])
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('device_list_filter_location_id',  __('purchase.business_location') . ':') !!}
                        {!! Form::select('device_list_filter_location_id', $business_locations, null, ['class' => 'form-control select2', 'style' => 'width:100%', 'placeholder' => __('lang_v1.all')]); !!}
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('device_list_filter_device_location', __('device.device_location') . ':') !!}
                        {!! Form::select('device_list_filter_device_location', ['inside' => __('device.inside'), 'outside' => __('device.outside')], null , ['class' => 'form-control select2','placeholder' => __('lang_v1.all')]); !!}
                    </div>
                </div>
            @endcomponent
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @component('components.widget', ['class' => 'box-primary', 'title' => __('device.all_devices')])
                @can('device.access')
                    @slot('tool')
                        <div class="box-tools">
                            <a class="btn btn-block btn-primary" href="{{action('DeviceController@create')}}">
                            <i class="fa fa-plus"></i> @lang('messages.add')</a>
                        </div>
                    @endslot
                @endcan
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="device_table">
                        <thead>
                            <tr>
                                <th>Sr. #</th>
                                <th>@lang('device.device_name')</th>
                                <th>@lang('device.mac_address')</th>
                                <th>@lang('device.device_location')</th>
                                <th>@lang('device.device_limit')</th>
                                <th>@lang('device.status')</th>
                                <th>@lang('device.action')</th>
                            </tr>
                        </thead>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            @endcomponent
        </div>
    </div>

</section>
<!-- /.content -->

@stop
@section('javascript')
 <script src="{{ asset('js/device.js?v=' . $asset_v) }}"></script>
@endsection