<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Product related operations.
    |
    */

    'biometric' => 'Biometric',
    'add_device' => 'Add Device',
    'edit_device' => 'Edit Device',
    'device_name' => 'Device Name',
    'biometric_device_info' => 'Biometric Device Information',
    'devices' => 'Devices',
    'device' => 'Device',
    'all_devices' => 'All Devices',
    'mac_address' => 'Mac Address',
    'list_devices' => 'List Devices',
    'list_enrollment' => 'List Enrollment',
    'enrolled' => 'Enrolled',
    'not_enrolled' => 'Not Enrolled',
    'action' => 'Action',
    'inside' => 'Inside',
    'status' => 'Status',
    'outside' => 'Outside',
    'finger_id' => 'Finger Id',
    'device_limit' => 'Device Limit',
    'description' => 'Description',
    'device_location' => 'Device Location',
    'added_success' => 'Device added successfully',
    'delete_success' => 'Device deleted successfully',
    'updated_success' => 'Device updated successfully',

 ];
