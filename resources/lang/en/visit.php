<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Product Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Product related operations.
    |
    */

    'visits' => 'Visits',
    'visit_list' => 'List Visit',
    'add_visit' => 'Add Visit',
    'edit_visit' => 'Edit Visit',
    'customer_business' => 'Customer Business',
    'add_new_visit' => 'Add New Visit',
    'address' => 'Address',
    'visit_delete_success' => 'Visit deleted successfully',
    'phone_no' => 'Phone #',
    'email' => 'Email',
    'customer_name' => 'Customer Name',
    'plant_capacity' => 'Plant Capacity',
    'date_time' => 'Date & Time',
    'designation' => 'Designation',
    'comments' => 'Comments',
    'business_locations' => 'Business Locations',
    'added_success' => 'Visit Added Successfully',
    'updated_success' => 'Visit Updated Successfully',
    'recommendations' => 'Recommendations',
    'comments_recommendations' => 'Comments & Recommendations',
    'visit_header' => 'Reverse Osmosis Plant Analysis Report',

 ];
