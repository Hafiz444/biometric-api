<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in home page.
    |
    */
    'home' => 'Home',
    'welcome_message' => 'Welcome :name,',
    'total_sell' => 'Sales',
    'total_purchase' => 'Purchases',
    'invoice_due' => 'Receivable',
    'purchase_due' => 'Payable',
    'today' => 'Today',
    'this_week' => 'This Week',
    'this_month' => 'This Month',
    'this_fy' => 'This Financial Year',
    'sells_last_30_days' => 'Sales Last 30 Days',
    'purchases_last_30_days' => 'Purchase Last 30 Days',
    'sells_current_fy' => 'Sales Current Financial Year',
    'total_sells' => 'Total Sales (:currency)',
    'total_purchases' => 'Total Purchases (:currency)',
    'product_stock_alert' => 'Product Stock Alert',
    'payment_dues' => 'Payment Dues',
    'due_amount' => 'Due Amount',
    'stock_expiry_alert' => 'Stock Expiry Alert',
    'todays_profit' => "Today's profit",
    'total_products' => "Active Items",
    ];
