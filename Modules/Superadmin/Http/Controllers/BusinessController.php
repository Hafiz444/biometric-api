<?php

namespace Modules\Superadmin\Http\Controllers;

use App\Account;
use App\AccountType;
use App\Business;
use App\Currency;
use App\Product;
use App\Transaction;
use App\User;
use App\Utils\BusinessUtil;
use App\Utils\ModuleUtil;
use App\VariationLocationDetails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Superadmin\Notifications\PasswordUpdateNotification;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\Facades\DataTables;

class BusinessController extends BaseController
{
    protected $businessUtil;
    protected $moduleUtil;

    /**
     * Constructor
     *
     * @param ProductUtils $product
     * @return void
     */
    public function __construct(BusinessUtil $businessUtil, ModuleUtil $moduleUtil)
    {
        $this->businessUtil = $businessUtil;
        $this->moduleUtil = $moduleUtil;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if (!auth()->user()->can('superadmin')) {
            abort(403, 'Unauthorized action.');
        }

        $date_today = \Carbon::today();

        $businesses = Business::orderby('name')
                    ->with(['subscriptions' => function ($query) use ($date_today) {
                        $query->whereDate('start_date', '<=', $date_today)
                            ->whereDate('end_date', '>=', $date_today);
                    }, 'locations', 'owner'])
                    ->paginate(21);
        
        $business_id = request()->session()->get('user.business_id');
        return view('superadmin::business.index')
            ->with(compact('businesses', 'business_id'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if (!auth()->user()->can('superadmin')) {
            abort(403, 'Unauthorized action.');
        }

        $currencies = $this->businessUtil->allCurrencies();
        $timezone_list = $this->businessUtil->allTimeZones();

        $accounting_methods = $this->businessUtil->allAccountingMethods();

        $months = [];
        for ($i=1; $i<=12 ; $i++) {
            $months[$i] = __('business.months.' . $i);
        }

        $is_admin = true;

        return view('superadmin::business.create')
            ->with(compact(
                'currencies',
                'timezone_list',
                'accounting_methods',
                'months',
                'is_admin'
            ));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('superadmin')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            DB::beginTransaction();

            //Create owner.
            $owner_details = $request->only(['surname', 'first_name', 'last_name', 'username', 'email', 'password']);
            $owner_details['language'] = env('APP_LOCALE');
            $user = User::create_user($owner_details);
            $business_details = $request->only(['name', 'start_date', 'currency_id', 'tax_label_1', 'tax_number_1', 'tax_label_2', 'tax_number_2', 'time_zone', 'accounting_method', 'fy_start_month']);
            $country = Currency::where('id',$business_details['currency_id'])->pluck('country')->first();
            $business_location = $request->only(['name', 'state', 'city', 'zip_code', 'landmark', 'website', 'mobile', 'alternate_number']);
            $business_location['country'] = $country;
                
            //Create the business
            $business_details['owner_id'] = $user->id;
            if (!empty($business_details['start_date'])) {
                $business_details['start_date'] = $this->businessUtil->uf_date($business_details['start_date']);
            }
                
            //upload logo
            $logo_name = $this->businessUtil->uploadFile($request, 'business_logo', 'business_logos', 'image');
            if (!empty($logo_name)) {
                $business_details['logo'] = $logo_name;
            }
            
            //default enabled modules
            $business_details['enabled_modules'] = ['purchases','add_sale','pos_sale','stock_transfers','stock_adjustment','expenses'];
            
            $business = $this->businessUtil->createNewBusiness($business_details);

            //Update user with business id
            $user->business_id = $business->id;
            $user->save();

            $get_customer = $this->businessUtil->newBusinessDefaultResources($business->id, $user->id);
            $new_location = $this->businessUtil->addLocation($business->id, $business_location);

            //create new permission with the new location
            $check_location = Permission::where('name', 'location.'.$new_location->id)->pluck('id')->first();
            if (empty($check_location)){
                Permission::create(['name' => 'location.' . $new_location->id ]);
            }

            $business_id=$user->business_id;
            $capital_account_type=new AccountType();
            $capital_account_type->name='01-CAPITAL';
            $capital_account_type->parent_account_type_id=null;
            $capital_account_type->is_deletable=false;
            $capital_account_type->business_id=$business_id;
            $capital_account_type->save();
            $assets_account_type=new AccountType();
            $assets_account_type->name='02-ASSETS';
            $assets_account_type->parent_account_type_id=null;
            $assets_account_type->is_deletable=false;
            $assets_account_type->business_id=$business_id;
            $assets_account_type->save();
            $liabilities_account_type=new AccountType();
            $liabilities_account_type->name='03-LIABILITIES';
            $liabilities_account_type->parent_account_type_id=null;
            $liabilities_account_type->is_deletable=false;
            $liabilities_account_type->business_id=$business_id;
            $liabilities_account_type->save();
            $income_account_type=new AccountType();
            $income_account_type->name='04-INCOME';
            $income_account_type->parent_account_type_id=null;
            $income_account_type->is_deletable=false;
            $income_account_type->business_id=$business_id;
            $income_account_type->save();
            $expenses_account_type=new AccountType();
            $expenses_account_type->name='05-EXPENSES';
            $expenses_account_type->parent_account_type_id=null;
            $expenses_account_type->is_deletable=false;
            $expenses_account_type->business_id=$business_id;
            $expenses_account_type->save();
            $capital_account_types=[
                '01-0001' => 'Capital Account',
                '01-0002' => 'Retained Earnings',
                '01-0003' => 'Owners Equity',
            ];
            $account_types=[];
            foreach ($capital_account_types as $key => $value) {
                $single_account_type=[];
                $single_account_type['name']=$key.' '.$value;
                $single_account_type['parent_account_type_id']=$capital_account_type->id;
                $single_account_type['is_deletable']=false;
                $single_account_type['business_id']=$business_id;
                $single_account_type['created_at']=now();
                $single_account_type['updated_at']=now();
                $account_types[]=$single_account_type;

            }
            $assets_account_types=[
                '02-0001' => 'Sundry Debtors',
                '02-0002' => 'Customers',
                '02-0003' => 'Loans & Advances',
                '02-0004' => 'Fixed Assets',
                '02-0005' => 'Current Assets ',
                '02-0006' => 'Cash Account',
                '02-0007' => 'Bank Account',
                '02-0008' => 'Deposits',
            ];
            foreach ($assets_account_types as $key => $value) {
                $single_account_type=[];
                $single_account_type['name']=$key.' '.$value;
                $single_account_type['parent_account_type_id']=$assets_account_type->id;
                $single_account_type['is_deletable']=false;
                $single_account_type['business_id']=$business_id;
                $single_account_type['created_at']=now();
                $single_account_type['updated_at']=now();
                $account_types[]=$single_account_type;
            }
            $liabilities_account_types=[
                '03-0001' => 'Sundry Creditors',
                '03-0002' => 'Suppliers',
                '03-0003' => 'Bank Loans',
                '03-0004' => 'Employee Accounts',
                '03-0005' => 'Current Liabilities',
                '03-0006' => 'Rent Expenses',
                '03-0007' => 'Charity & Donations',
            ];
            foreach ($liabilities_account_types as $key => $value) {
                $single_account_type=[];
                $single_account_type['name']=$key.' '.$value;
                $single_account_type['parent_account_type_id']=$liabilities_account_type->id;
                $single_account_type['is_deletable']=false;
                $single_account_type['business_id']=$business_id;
                $single_account_type['created_at']=now();
                $single_account_type['updated_at']=now();
                $account_types[]=$single_account_type;
            }
            $income_account_types=[
                '04-0001' => 'Direct Income',
                '04-0002' => 'Indirect Income',
                '04-0003' => 'Sales Account',
                '04-0004' => 'Rental Income',
                '04-0005' => 'Service Income',
                '04-0006' => 'Subscriptions Income',
            ];
            foreach ($income_account_types as $key => $value) {
                $single_account_type=[];
                $single_account_type['name']=$key.' '.$value;
                $single_account_type['parent_account_type_id']=$income_account_type->id;
                $single_account_type['is_deletable']=false;
                $single_account_type['business_id']=$business_id;
                $single_account_type['created_at']=now();
                $single_account_type['updated_at']=now();
                $account_types[]=$single_account_type;
            }
            $expenses_account_types=[
                '05-0001' => 'Admin & Operative Expenses',
                '05-0002' => 'Sales Expenses',
                '05-0003' => 'Vehicle Repair & Maintenance Expenses',
                '05-0004' => 'Building Repair & Maintenance Expenses',
                '05-0005' => 'Utilities Expenses',
                '05-0006' => 'Rent Expenses',
                '05-0007' => 'Charity & Donations',
            ];
            foreach ($expenses_account_types as $key => $value) {
                $single_account_type=[];
                $single_account_type['name']=$key.' '.$value;
                $single_account_type['parent_account_type_id']=$expenses_account_type->id;
                $single_account_type['is_deletable']=false;
                $single_account_type['business_id']=$business_id;
                $single_account_type['created_at']=now();
                $single_account_type['updated_at']=now();
                $account_types[]=$single_account_type;
            }
            AccountType::insert($account_types);
            $account_type=AccountType::where('business_id',$business_id)->where('name','02-0006 Cash Account')->first();
            $account=new Account();
            $account->business_id=$business_id;
            $account->account_type_id=$account_type->id;
            $account->name='Cash in Hand';
            $account->created_by=$user->id;
            $account->save();

            // create Walk-In Customer's account
            $account_type=AccountType::where('business_id', $business_id)->where('name','02-0002 Customers')->first();
            $account_type_id=$account_type->id;
            $account_model=new Account();
            $account_model->business_id=$business_id;
            $account_model->name=$get_customer->name;
            $account_model->account_type_id=$account_type_id;
            $account_model->created_by=$get_customer->created_by;
            $account_model->contact_id=$get_customer->id;
            $account_model->save();

            DB::commit();

            //Module function to be called after after business is created
            if (config('app.env') != 'demo') {
                $this->moduleUtil->getModuleData('after_business_created', ['business' => $business]);
            }

            $output = ['success' => 1,
                            'msg' => __('business.business_created_succesfully')
                        ];

            return redirect()
                ->action('\Modules\Superadmin\Http\Controllers\BusinessController@index')
                ->with('status', $output);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = ['success' => 0,
                            'msg' => __('messages.something_went_wrong')
                        ];

            return back()->with('status', $output)->withInput();
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($business_id)
    {
        if (!auth()->user()->can('superadmin')) {
            abort(403, 'Unauthorized action.');
        }

        $business = Business::with(['currency', 'locations', 'subscriptions', 'owner'])->find($business_id);
        
        $created_id = $business->created_by;

        $created_by = !empty($created_id) ? User::find($created_id) : null;

        return view('superadmin::business.show')
            ->with(compact('business', 'created_by'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('superadmin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('superadmin')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $notAllowed = $this->businessUtil->notAllowedInDemo();
            if (!empty($notAllowed)) {
                return $notAllowed;
            }

            //Check if logged in busines id is same as deleted business then not allowed.
            $business_id = request()->session()->get('user.business_id');
            if ($business_id == $id) {
                $output = ['success' => 0, 'msg' => __('superadmin.lang.cannot_delete_current_business')];
                return back()->with('status', $output);
            }

            DB::beginTransaction();

            //Delete related products & transactions.
            $products_id = Product::where('business_id', $id)->pluck('id')->toArray();
            if (!empty($products_id)) {
                VariationLocationDetails::whereIn('product_id', $products_id)->delete();
            }
            Transaction::where('business_id', $id)->delete();

            Business::where('id', $id)
                ->delete();

            DB::commit();

            $output = ['success' => 1, 'msg' => __('lang_v1.success')];
            return redirect()
                ->action('\Modules\Superadmin\Http\Controllers\BusinessController@index')
                ->with('status', $output);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = ['success' => 0,
                            'msg' => __('messages.something_went_wrong')
                        ];

            return back()->with('status', $output)->withInput();
        }
    }

    /**
     * Changes the activation status of a business.
     * @return Response
     */
    public function toggleActive(Request $request, $business_id, $is_active)
    {
        if (!auth()->user()->can('superadmin')) {
            abort(403, 'Unauthorized action.');
        }

        $notAllowed = $this->businessUtil->notAllowedInDemo();
        if (!empty($notAllowed)) {
            return $notAllowed;
        }
            
        Business::where('id', $business_id)
            ->update(['is_active' => $is_active]);

        $output = ['success' => 1,
                    'msg' => __('lang_v1.success')
                ];
        return back()->with('status', $output);
    }

    /**
     * Shows user list for a particular business
     * @return Response
     */
    public function usersList($business_id)
    {
        if (!auth()->user()->can('superadmin')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            $user_id = request()->session()->get('user.id');

            $users = User::where('business_id', $business_id)
                        ->where('id', '!=', $user_id)
                        ->where('is_cmmsn_agnt', 0)
                        ->select(['id', 'username',
                            DB::raw("CONCAT(COALESCE(surname, ''), ' ', COALESCE(first_name, ''), ' ', COALESCE(last_name, '')) as full_name"), 'email']);

            return Datatables::of($users)
                ->addColumn(
                    'role',
                    function ($row) {
                        $role_name = $this->moduleUtil->getUserRoleName($row->id);
                        return $role_name;
                    }
                )
                ->addColumn(
                    'action',
                    '@can("user.update")
                        <a href="#" class="btn btn-xs btn-primary update_user_password" data-user_id="{{$id}}" data-user_name="{{$full_name}}"><i class="glyphicon glyphicon-edit"></i> @lang("superadmin::lang.update_password")</a>
                        &nbsp;
                    @endcan'
                )
                ->filterColumn('full_name', function ($query, $keyword) {
                    $query->whereRaw("CONCAT(COALESCE(surname, ''), ' ', COALESCE(first_name, ''), ' ', COALESCE(last_name, '')) like ?", ["%{$keyword}%"]);
                })
                ->removeColumn('id')
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Updates user password from superadmin
     * @return Response
     */
    public function updatePassword(Request $request)
    {
        if (!auth()->user()->can('superadmin')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $notAllowed = $this->businessUtil->notAllowedInDemo();
            if (!empty($notAllowed)) {
                return $notAllowed;
            }
        
            $user = User::findOrFail($request->input('user_id'));
            $user->password = Hash::make($request->input('password'));
            $user->save();

            //Send password update notification
            if ($this->moduleUtil->IsMailConfigured()) {
                $user->notify(new PasswordUpdateNotification($request->input('password')));
            }

            $output = ['success' => 1,
                        'msg' => __("superadmin::lang.password_updated_successfully")
                    ];
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
            $output = ['success' => 0,
                            'msg' => __("messages.something_went_wrong")
                        ];
        }

        return $output;
    }
}
