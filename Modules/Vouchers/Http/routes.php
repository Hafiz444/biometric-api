<?php

Route::group(['middleware' => ['web', 'authh', 'SetSessionData', 'auth', 'language', 'timezone', 'AdminSidebarMenu'],
    'prefix' => 'vouchers', 'namespace' => 'Modules\Vouchers\Http\Controllers'], function () {

    Route::get('/install', 'InstallController@index');
    Route::post('/install', 'InstallController@install');
    Route::get('/install/update', 'InstallController@update');
    Route::get('/install/uninstall', 'InstallController@uninstall');

    Route::get('add-cash-payment','VoucherController@addCashPaymentVoucher');
    Route::post('add-cash-payment','VoucherController@storeCashPaymentVoucher');
    Route::get('add-cash-received','VoucherController@addCashReceivedVoucher');
    Route::post('add-cash-received','VoucherController@storeCashReceivedVoucher');
    Route::get('add-bank-payment','VoucherController@addBankPaymentVoucher');
    Route::post('add-bank-payment','VoucherController@storeBankPaymentVoucher');
    Route::get('add-bank-received','VoucherController@addBankReceivedVoucher');
    Route::post('add-bank-received','VoucherController@storeBankReceivedVoucher');
    Route::get('add-journal','VoucherController@addJournalVoucher');
    Route::post('add-journal','VoucherController@storeJournalVoucher');

    Route::get('{voucher}','VoucherController@show')->name('get_voucher');

    Route::get('/print/{voucher}','VoucherController@printVoucher');
    Route::post('/download/{voucher}','VoucherController@downloadVoucher');

    Route::get('/save_and_print/{id}', 'VoucherController@save_and_print')->name('save_and_print');

    Route::get('/destroy/{id}', 'VoucherController@destroy');

});

