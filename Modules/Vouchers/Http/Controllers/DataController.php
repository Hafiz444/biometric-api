<?php

namespace Modules\Vouchers\Http\Controllers;

use App\Transaction;
use App\Utils\ModuleUtil;
use DB;
use Illuminate\Routing\Controller;
use Menu;

class DataController extends Controller
{
    /**
     * Defines module as a superadmin package.
     * @return Array
     */
    public function superadmin_package()
    {
        return [
            [
                'name' => 'voucher_module',
                'label' => __('vouchers::voucher.vouchers_module'),
                'default' => false
            ]
        ];
    }

    /**
     * Defines user permissions for the module.
     * @return array
     */
    public function user_permissions()
    {
        return [
            [
                'value' => 'vouchers.all_vouchers',
                'label' => __('vouchers::voucher.sidebar_label'),
                'default' => false
            ],
        ];
    }

    /**
     * Adds Vouchers menus
     * @return null
     */
    public function modifyAdminMenu()
    {
        if (auth()->user()->can('superadmin')) {
            $__is_vouchers_enabled = true;
        } else {
            $business_id = session()->get('user.business_id');
            $module_util = new ModuleUtil();
            $__is_vouchers_enabled = (boolean)$module_util->hasThePermissionInSubscription($business_id, 'voucher_module', 'superadmin_package');
        }
        if ($__is_vouchers_enabled && (auth()->user()->can('vouchers.all_vouchers'))) {
            Menu::modify('admin-sidebar-menu', function ($menu) {

                //check whether accounts module is active then show vouchers
                $enabled_modules = !empty(session('business.enabled_modules')) ? session('business.enabled_modules') : [];

                if (auth()->user()->can('account.access') && in_array('account', $enabled_modules)) {
                    $menu->dropdown(
                        __('vouchers::voucher.vouchers'),
                        function ($sub) {
                            if (auth()->user()->can('vouchers.all_vouchers')) {
                                $sub->url(
                                    action('\Modules\Vouchers\Http\Controllers\VoucherController@addCashPaymentVoucher'),
                                    __('vouchers::sidebar.cash_payment_voucher'),
                                    ['icon' => 'fa fa-list', 'active' => request()->segment(1) == 'vouchers' && request()->segment(2) == 'add-cash-payment']
                                );
                                $sub->url(
                                    action('\Modules\Vouchers\Http\Controllers\VoucherController@addCashReceivedVoucher'),
                                    __('vouchers::sidebar.cash_received_voucher'),
                                    ['icon' => 'fa fa-list', 'active' => request()->segment(1) == 'vouchers' && request()->segment(2) == 'add-cash-received']
                                );
                                $sub->url(
                                    action('\Modules\Vouchers\Http\Controllers\VoucherController@addBankPaymentVoucher'),
                                    __('vouchers::sidebar.bank_payment_voucher'),
                                    ['icon' => 'fa fa-list', 'active' => request()->segment(1) == 'vouchers' && request()->segment(2) == 'add-bank-payment']
                                );
                                $sub->url(
                                    action('\Modules\Vouchers\Http\Controllers\VoucherController@addBankReceivedVoucher'),
                                    __('vouchers::sidebar.bank_received_voucher'),
                                    ['icon' => 'fa fa-list', 'active' => request()->segment(1) == 'vouchers' && request()->segment(2) == 'add-bank-received']
                                );
                                $sub->url(
                                    action('\Modules\Vouchers\Http\Controllers\VoucherController@addJournalVoucher'),
                                    __('vouchers::sidebar.journal_voucher'),
                                    ['icon' => 'fa fa-list', 'active' => request()->segment(1) == 'vouchers' && request()->segment(2) == 'add-journal']
                                );
                            }
                        },
                        ['icon' => 'fa fas fa-money-bill-alt']
                    )->order(50);
                }
            });
        }
    }

}
