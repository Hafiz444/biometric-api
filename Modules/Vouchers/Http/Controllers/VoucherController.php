<?php

namespace Modules\Vouchers\Http\Controllers;

use App\AccountTransaction;

use App\BusinessLocation;
use App\Business;
use App\ExpenseCategory;
use App\Transaction;
use App\Account;
use App\AccountType;
use App\Contact;
use Modules\Vouchers\Entities\Voucher;
use App\User;
use App\Utils\ModuleUtil;
use App\Utils\VoucherUtil;   
use App\Utils\BusinessUtil;   
use App\TransactionPayment;
use App\Utils\TransactionUtil;

use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Yajra\DataTables\Facades\DataTables;

class VoucherController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $moduleUtil;
    protected $voucherUtil;
    protected $businessUtil;
    protected $leading_zeroes=6;

    /**
     * Constructor
     *
     * @param ProductUtils $product
     * @return void
     */

    public function __construct(ModuleUtil $moduleUtil,VoucherUtil $voucherUtil,BusinessUtil $businessUtil)
    {
        $this->moduleUtil = $moduleUtil;
        $this->voucherUtil =$voucherUtil;
        $this->businessUtil=$businessUtil;
    }


	public function addCashPaymentVoucher(Request $request)
	{
	    if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');
        
        if($request->ajax())
        {
            $vouchers=Voucher::where('business_id', $business_id)->where('type','CP')->with('account_to','account_from');
            return datatables()->of($vouchers)
                ->addColumn(
                    'action',
                    '<div class="btn-group">
                        <a href="{{action(\'\Modules\Vouchers\Http\Controllers\VoucherController@destroy\', [$id])}}" class="btn btn-danger btn-xs delete_voucher"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</a>
                    </div>'
                )
                ->editColumn('ref_no', function($data){
                    return '<a href="'.route('get_voucher',$data['id']).'"></i>'.$data['ref_no'].'</a>';
                })
                ->rawColumns(['ref_no', 'action'])
                ->toJson();
        }
        else{
            $business_locations = BusinessLocation::forDropdown($business_id);
            $accounts = Account::where('business_id', $business_id)->pluck('name','id');
            $cash_in_hand_account_type_ids=AccountType::where('business_id', $business_id)
                ->where('name','02-0006 Cash Account')
                ->pluck('id')->toArray();

            $cash_in_hand_accounts=Account::where('business_id', $business_id)
                ->whereIn('account_type_id',$cash_in_hand_account_type_ids)
                ->pluck('name','id');

            $users = User::forDropdown($business_id, true, true);

            $default_datetime = $this->businessUtil->format_date('now', true);
            $vouchers=Voucher::where('business_id',$business_id)->where('type','CP')->get();
        
            return view('vouchers::add_cash_payment')
                ->with(compact('accounts','vouchers', 'business_locations', 'users', 'default_datetime', 'cash_in_hand_accounts'));
        }
	}
	public function storeCashPaymentVoucher(Request $request)
	{
	    if (!auth()->user()->can('account.access')) {
	        abort(403, 'Unauthorized action.');
	    }
        
	    try {

	        $business_id = $request->session()->get('user.business_id');

            $voucher_no = Voucher::where('business_id', $business_id)->where('type', 'CP')->orderBy('voucher_no', 'DESC')->first();

            //Check if first voucher is being created and there are no vouchers currently in the system!
            if (is_null($voucher_no)) {
                $voucher_id = 0;
            } else {
                $voucher_id = $voucher_no->voucher_no;
            }
            $voucher_id = str_pad($voucher_id + 1, $this->leading_zeroes, 0, STR_PAD_LEFT);

            $voucher=new Voucher();
            $voucher->to_account=$request->get('to_account');
            $voucher->from_account=$request->get('from_account');
            $voucher->amount=$request->get('ammount');
      		$voucher->ref_no='CP-'.$voucher_id;
            $voucher->type='CP';
      		$voucher->amount=$request->get('amount');
      		$voucher->user_id=auth()->user()->id;
      		$voucher->note=$request->get('note');
            $voucher->location_id=$request->get('location_id');
            $voucher->business_id=$business_id;
            $voucher->voucher_no=$voucher_id;
            $voucher->payment_method='Cash';
            $voucher->date=$this->moduleUtil->uf_date($request->get('date'), true);
            $voucher->document=$this->moduleUtil->uploadFile($request, 'document', 'documents');

            DB::beginTransaction();

            $voucher->save();
       
            $transaction_data=[
                'account_id' => $request->get('to_account'),
                'voucher_id' => $voucher->id,
                'debit' => $request->get('amount'),
                'credit' => 0,
                'type' => 'debit',
                'sub_type' => 'fund_transfer',
                'amount' => $request->get('amount'),
                'reff_no' => $voucher->ref_no,
                'operation_date' => $voucher->date,
                'created_by' => $voucher->user_id,
            ];

            $debit_transaction = AccountTransaction::create($transaction_data);

            $transaction_data=[
                'account_id' => $request->get('from_account'),
                'voucher_id' => $voucher->id,
                'debit' => 0,
                'credit' => $request->get('amount'),
                'type' => 'credit',
                'sub_type' => 'fund_transfer',
                'amount' => $request->get('amount'),
                'reff_no' => $voucher->ref_no,
                'transfer_transaction_id' => $debit_transaction->id,
                'operation_date' => $voucher->date,
                'created_by' => $voucher->user_id,
            ];

            $credit_transaction = AccountTransaction::create($transaction_data);
            $debit_transaction->transfer_transaction_id = $credit_transaction->id;
            $debit_transaction->save();
            DB::commit();

            if ($request->submit == 'save_and_print'){
                //return redirect()->route('save_and_print', [$voucher->id]);
                return $this->printVoucher($voucher->id, 1);
            }

            $output = [
                'success' => 1,
                'msg' => __('vouchers::voucher.cash_payment_voucher_added')
            ];

        } catch (\Exception $e) {
        	
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            DB::rollback();
            $output = [
                'success' => 0,
                'msg' => __('vouchers::voucher.something_went_wrong')
            ];
        }
        return back()->with('status', $output);
	}

    public function addCashReceivedVoucher(Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');

        if ($request->ajax()) {
            $vouchers = Voucher::where('business_id', $business_id)->where('type', 'CR')->with('account_to', 'account_from');
            return datatables()->of($vouchers)
                ->addColumn(
                    'action',
                    '<div class="btn-group">
                        <a href="{{action(\'\Modules\Vouchers\Http\Controllers\VoucherController@destroy\', [$id])}}" class="btn btn-danger btn-xs delete_voucher"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</a>
                    </div>'
                )
                ->editColumn(
                    'ref_no',
                    function ($data) {

                        return '<a href="' . route('get_voucher', $data['id']) . '"></i>' . $data['ref_no'] . '</a>';
                    }
                )
                ->rawColumns(['ref_no', 'action'])
                ->toJson();
        } else {

            $business_locations = BusinessLocation::forDropdown($business_id);

            $accounts = Account::where('business_id', $business_id)->pluck('name', 'id');

            $cash_in_hand_account_type_ids = AccountType::where('business_id', $business_id)
                ->where('name', '02-0006 Cash Account')
                ->pluck('id')->toArray();

            $cash_in_hand_accounts = Account::where('business_id', $business_id)
                ->whereIn('account_type_id', $cash_in_hand_account_type_ids)
                ->pluck('name', 'id');

            $users = User::forDropdown($business_id, true, true);

            $default_datetime = $this->businessUtil->format_date('now', true);
            $vouchers = Voucher::where('business_id', $business_id)->where('type', 'CR')->get();

            return view('vouchers::add_cash_received')
                ->with(compact('accounts', 'vouchers', 'business_locations', 'users', 'default_datetime', 'cash_in_hand_accounts'));
        }
    }

    public function storeCashReceivedVoucher(Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $business_id = $request->session()->get('user.business_id');

            $voucher_no = Voucher::where('business_id', $business_id)->where('type', 'CR')->orderBy('voucher_no', 'DESC')->first();

            //Check if first voucher is being created and there are no vouchers currently in the system!
            if (is_null($voucher_no)) {
                $voucher_id = 0;
            } else {
                $voucher_id = $voucher_no->voucher_no;
            }
            $voucher_id = str_pad($voucher_id + 1, $this->leading_zeroes, 0, STR_PAD_LEFT);

            $voucher = new Voucher();
            $voucher->to_account = $request->get('to_account');
            $voucher->from_account = $request->get('from_account');
            $voucher->amount = $request->get('ammount');
            $voucher->ref_no = 'CR-' . $voucher_id;
            $voucher->type = 'CR';
            $voucher->amount = $request->get('amount');
            $voucher->user_id = auth()->user()->id;
            $voucher->note = $request->get('note');
            $voucher->location_id = $request->get('location_id');
            $voucher->business_id = $business_id;
            $voucher->voucher_no = $voucher_id;
            $voucher->payment_method = 'Cash';
            $voucher->date = $this->moduleUtil->uf_date($request->get('date'), true);
            $voucher->document = $this->moduleUtil->uploadFile($request, 'document', 'documents');

            DB::begintransaction();

            $voucher->save();

            $transaction_data = [
                'account_id' => $request->get('to_account'),
                'voucher_id' => $voucher->id,
                'debit' => $request->get('amount'),
                'credit' => 0,
                'type' => 'debit',
                'sub_type' => 'fund_transfer',
                'amount' => $request->get('amount'),
                'reff_no' => $voucher->ref_no,
                'operation_date' => $voucher->date,
                'created_by' => $voucher->user_id,
            ];

            $debit_transaction = AccountTransaction::create($transaction_data);

            $transaction_data = [
                'account_id' => $request->get('from_account'),
                'voucher_id' => $voucher->id,
                'debit' => 0,
                'credit' => $request->get('amount'),
                'type' => 'credit',
                'sub_type' => 'fund_transfer',
                'amount' => $request->get('amount'),
                'transfer_transaction_id' => $debit_transaction->id,
                'reff_no' => $voucher->ref_no,
                'operation_date' => $voucher->date,
                'created_by' => $voucher->user_id,
            ];

            $credit_transaction = AccountTransaction::create($transaction_data);

            $debit_transaction->transfer_transaction_id = $credit_transaction->id;
            $debit_transaction->save();
            DB::commit();

            if ($request->submit == 'save_and_print'){
                //return redirect()->route('save_and_print', [$voucher->id]);
                return $this->printVoucher($voucher->id, 1);
            }

            $output = [
                'success' => 1,
                'msg' => __('vouchers::voucher.cash_received_payment_voucher_added')
            ];

        } catch (\Exception $e) {

            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            DB::rollBack();
            $output = [
                'success' => 0,
                'msg' => __('vouchers::voucher.something_went_wrong')
            ];
        }
        return back()->with('status', $output);
    }

    public function addBankPaymentVoucher(Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');

        if ($request->ajax()) {
            $vouchers = Voucher::where('business_id', $business_id)->where('type', 'BP')->with('account_to', 'account_from');
            return datatables()->of($vouchers)
                ->addColumn(
                    'action',
                    '<div class="btn-group">
                        <a href="{{action(\'\Modules\Vouchers\Http\Controllers\VoucherController@destroy\', [$id])}}" class="btn btn-danger btn-xs delete_voucher"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</a>
                    </div>'
                )
                ->editColumn(
                    'ref_no',
                    function ($data) {

                        return '<a href="' . route('get_voucher', $data['id']) . '"></i>' . $data['ref_no'] . '</a>';
                    }
                )
                ->rawColumns(['ref_no', 'action'])
                ->toJson();
        } else {

            $business_locations = BusinessLocation::forDropdown($business_id);

            $accounts = Account::where('business_id', $business_id)->pluck('name', 'id');

            $cash_at_bank_account_type_ids = AccountType::where('business_id', $business_id)
                ->where('name', '02-0007 Bank Account')
                ->pluck('id')->toArray();

            $cash_at_bank_accounts = Account::where('business_id', $business_id)
                ->whereIn('account_type_id', $cash_at_bank_account_type_ids)
                ->pluck('name', 'id');

            $users = User::forDropdown($business_id, true, true);

            $default_datetime = $this->businessUtil->format_date('now', true);
            $vouchers = Voucher::where('business_id', $business_id)->where('type', 'BP')->get();

            return view('vouchers::add_bank_payment')
                ->with(compact('accounts', 'vouchers', 'business_locations', 'users', 'default_datetime', 'cash_at_bank_accounts'));
        }
    }

    public function storeBankPaymentVoucher(Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $business_id = $request->session()->get('user.business_id');

            $voucher_no = Voucher::where('business_id', $business_id)->where('type', 'BP')->orderBy('voucher_no', 'DESC')->first();

            //Check if first voucher is being created and there are no vouchers currently in the system!
            if (is_null($voucher_no)) {
                $voucher_id = 0;
            } else {
                $voucher_id = $voucher_no->voucher_no;
            }
            $voucher_id = str_pad($voucher_id + 1, $this->leading_zeroes, 0, STR_PAD_LEFT);

            $voucher = new Voucher();
            $voucher->to_account = $request->get('to_account');
            $voucher->from_account = $request->get('from_account');
            $voucher->amount = $request->get('ammount');
            $voucher->ref_no = 'BP-' . $voucher_id;
            $voucher->type = 'BP';
            $voucher->amount = $request->get('amount');
            $voucher->user_id = auth()->user()->id;
            $voucher->note = $request->get('note');
            $voucher->location_id = $request->get('location_id');
            $voucher->business_id = $business_id;
            $voucher->voucher_no = $voucher_id;
            $voucher->payment_method = 'Bank';
            $voucher->date = $this->moduleUtil->uf_date($request->get('date'), true);
            $voucher->document = $this->moduleUtil->uploadFile($request, 'document', 'documents');

            DB::beginTransaction();

            $voucher->save();

            $transaction_data = [
                'account_id' => $request->get('to_account'),
                'voucher_id' => $voucher->id,
                'type' => 'debit',
                'debit' => $request->get('amount'),
                'credit' => 0,
                'sub_type' => 'fund_transfer',
                'amount' => $request->get('amount'),
                'reff_no' => $voucher->ref_no,
                'operation_date' => $voucher->date,
                'created_by' => $voucher->user_id,
            ];

            $debit_transaction = AccountTransaction::create($transaction_data);

            $transaction_data = [
                'account_id' => $request->get('from_account'),
                'voucher_id' => $voucher->id,
                'debit' => 0,
                'credit' => $request->get('amount'),
                'type' => 'credit',
                'sub_type' => 'fund_transfer',
                'amount' => $request->get('amount'),
                'transfer_transaction_id' => $debit_transaction->id,
                'reff_no' => $voucher->ref_no,
                'operation_date' => $voucher->date,
                'created_by' => $voucher->user_id,
            ];

            $credit_transaction = AccountTransaction::create($transaction_data);
            $debit_transaction->transfer_transaction_id = $credit_transaction->id;
            $debit_transaction->save();

            DB::commit();

            if ($request->submit == 'save_and_print'){
                //return redirect()->route('save_and_print', [$voucher->id]);
                return $this->printVoucher($voucher->id, 1);
            }

            $output = [
                'success' => 1,
                'msg' => __('vouchers::voucher.add_bank_payment_voucher')
            ];

        } catch (\Exception $e) {

            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            DB::rollback();
            $output = [
                'success' => 0,
                'msg' => __('vouchers::voucher.something_went_wrong')
            ];
        }
        return back()->with('status', $output);
    }

    public function addBankReceivedVoucher(Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');

        if ($request->ajax()) {
            $vouchers = Voucher::where('business_id', $business_id)->where('type', 'BR')->with('account_to', 'account_from');
            return datatables()->of($vouchers)
                ->addColumn(
                    'action',
                    '<div class="btn-group">
                        <a href="{{action(\'\Modules\Vouchers\Http\Controllers\VoucherController@destroy\', [$id])}}" class="btn btn-danger btn-xs delete_voucher"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</a>
                    </div>'
                )
                ->editColumn(
                    'ref_no',
                    function ($data) {

                        return '<a href="' . route('get_voucher', $data['id']) . '"></i>' . $data['ref_no'] . '</a>';
                    }
                )
                ->rawColumns(['ref_no', 'action'])
                ->toJson();
        } else {

            $business_locations = BusinessLocation::forDropdown($business_id);

            $accounts = Account::where('business_id', $business_id)->pluck('name', 'id');

            $cash_at_bank_account_type_ids = AccountType::where('business_id', $business_id)
                ->where('name', '02-0007 Bank Account')
                ->pluck('id')->toArray();

            $cash_at_bank_accounts = Account::where('business_id', $business_id)
                ->whereIn('account_type_id', $cash_at_bank_account_type_ids)
                ->pluck('name', 'id');

            $users = User::forDropdown($business_id, true, true);

            $default_datetime = $this->businessUtil->format_date('now', true);
            $vouchers = Voucher::where('business_id', $business_id)->where('type', 'BR')->get();

            return view('vouchers::add_bank_received')
                ->with(compact('accounts', 'vouchers', 'business_locations', 'users', 'default_datetime', 'cash_at_bank_accounts'));

        }
    }

    public function storeBankReceivedVoucher(Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            $business_id = $request->session()->get('user.business_id');

            $voucher_no = Voucher::where('business_id', $business_id)->where('type', 'BR')->orderBy('voucher_no', 'DESC')->first();

            //Check if first voucher is being created and there are no vouchers currently in the system!
            if (is_null($voucher_no)) {
                $voucher_id = 0;
            } else {
                $voucher_id = $voucher_no->voucher_no;
            }
            $voucher_id = str_pad($voucher_id + 1, $this->leading_zeroes, 0, STR_PAD_LEFT);

            $voucher = new Voucher();
            $voucher->to_account = $request->get('to_account');
            $voucher->from_account = $request->get('from_account');
            $voucher->amount = $request->get('ammount');
            $voucher->ref_no = 'BR-' . $voucher_id;
            $voucher->type = 'BR';
            $voucher->amount = $request->get('amount');
            $voucher->user_id = auth()->user()->id;
            $voucher->note = $request->get('note');
            $voucher->location_id = $request->get('location_id');
            $voucher->business_id = $business_id;
            $voucher->voucher_no = $voucher_id;
            $voucher->payment_method = 'Bank';
            $voucher->date = $this->moduleUtil->uf_date($request->get('date'), true);
            $voucher->document = $this->moduleUtil->uploadFile($request, 'document', 'documents');

            DB::beginTransaction();

            $voucher->save();

            $transaction_data = [
                'account_id' => $request->get('to_account'),
                'voucher_id' => $voucher->id,
                'type' => 'debit',
                'debit' => $request->get('amount'),
                'credit' => 0,
                'sub_type' => 'fund_transfer',
                'amount' => $request->get('amount'),
                'reff_no' => $voucher->ref_no,
                'operation_date' => $voucher->date,
                'created_by' => $voucher->user_id,
            ];

            $debit_transaction = AccountTransaction::create($transaction_data);

            $transaction_data = [
                'account_id' => $request->get('from_account'),
                'voucher_id' => $voucher->id,
                'type' => 'credit',
                'debit' => 0,
                'credit' => $request->get('amount'),
                'sub_type' => 'fund_transfer',
                'amount' => $request->get('amount'),
                'transfer_transaction_id' => $debit_transaction->id,
                'reff_no' => $voucher->ref_no,
                'operation_date' => $voucher->date,
                'created_by' => $voucher->user_id,
            ];

            $credit_transaction = AccountTransaction::create($transaction_data);
            $debit_transaction->transfer_transaction_id = $credit_transaction->id;
            $debit_transaction->save();
            DB::commit();

            if ($request->submit == 'save_and_print'){
                //return redirect()->route('save_and_print', [$voucher->id]);
                return $this->printVoucher($voucher->id, 1);
            }

            $output = [
                'success' => 1,
                'msg' => __('vouchers::voucher.bank_received_voucher')
            ];

        } catch (\Exception $e) {

            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            DB::rollback();
            $output = [
                'success' => 0,
                'msg' => __('vouchers::voucher.something_went_wrong')
            ];
        }
        return back()->with('status', $output);
    }

    public function addJournalVoucher(Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');

        if ($request->ajax()) {
            $journal_vouchers = Voucher::where('business_id', $business_id)->where('type', 'JV');
            return datatables()->of($journal_vouchers)
                ->addColumn(
                    'action',
                    '<div class="btn-group">
                        <a href="{{action(\'\Modules\Vouchers\Http\Controllers\VoucherController@destroy\', [$id])}}" class="btn btn-danger btn-xs delete_voucher"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</a>
                    </div>'
                )
                ->editColumn(
                    'ref_no',
                    function ($data) {

                        return '<a href="' . route('get_voucher', $data['id']) . '"></i>' . $data['ref_no'] . '</a>';
                    }
                )
                ->rawColumns(['ref_no', 'action'])
                ->toJson();
        } else {

            $business_locations = BusinessLocation::forDropdown($business_id);
            $accounts = Account::where('business_id', $business_id)->pluck('name', 'id');
            $accountsAdd = Account::where('business_id', $business_id)->get(['name', 'id']);
            $users = User::forDropdown($business_id, true, true);

            $default_datetime = $this->businessUtil->format_date('now', true);
            $vouchers = Voucher::where('business_id', $business_id)->where('type', 'JV')->get();

            return view('vouchers::add_journal')
                ->with(compact('accountsAdd', 'accounts', 'vouchers', 'business_locations', 'users', 'default_datetime'));

        }
    }

    public function storeJournalVoucher(Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        try {

            $business_id = $request->session()->get('user.business_id');
            $voucher_no = Voucher::where('business_id', $business_id)->where('type', 'JV')->orderBy('voucher_no', 'DESC')->first();

            //Check if first voucher is being created and there are no vouchers currently in the system!
            if (is_null($voucher_no)) {
                $voucher_id = 0;
            } else {
                $voucher_id = $voucher_no->voucher_no;
            }
            $voucher_id = str_pad($voucher_id + 1, $this->leading_zeroes, 0, STR_PAD_LEFT);

            $voucher = new Voucher();
            $voucher->to_account = 0;
            $voucher->from_account = $request->get('from_account');
            $voucher->ref_no = 'JV-'.$voucher_id;
            $voucher->type = 'JV';
            $voucher->user_id = auth()->user()->id;
            $voucher->note = $request->get('note');
            $voucher->location_id = $request->get('location_id');
            $voucher->business_id = $business_id;
            $voucher->voucher_no = $voucher_id;
            $voucher->payment_method = 'Bank';
            $voucher->date = $this->moduleUtil->uf_date($request->get('date'), true);
            $voucher->document = $this->moduleUtil->uploadFile($request, 'document', 'documents');

            DB::begintransaction();
            $voucher->save();

            foreach ($request->account_transaction as $key => $account_transaction) {

                if ($account_transaction['type'] == 'debit') {
                    $type = 'debit';
                    $credit = 0;
                    $debit = $account_transaction['amount'];
                }

                if ($account_transaction['type'] == 'credit') {
                    $type = 'credit';
                    $credit = $account_transaction['amount'];
                    $debit = 0;
                }
                $transaction_data = [
                    'account_id' => $account_transaction['from_account'],
                    'voucher_id' => $voucher->id,
                    'type' => $type,
                    'credit' => $credit,
                    'debit' => $debit,
                    'sub_type' => 'fund_transfer',
                    'amount' => $account_transaction['amount'],
                    'reff_no' => $voucher->ref_no,
                    'operation_date' => $voucher->date,
                    'created_by' => auth()->user()->id
                ];

                AccountTransaction::create($transaction_data);
            }
            DB::Commit();

            if ($request->submit == 'save_and_print'){
                //return redirect()->route('save_and_print', [$voucher->id]);
                return $this->printVoucher($voucher->id, 1);
            }

            $output = ['success' => 1,
                'msg' => __('vouchers::voucher.added_journal_voucher')
            ];

        } catch (\Exception $e) {

            \Log::emergency("File:" . $e->getFile() . "Line:" . $e->getLine() . "Message:" . $e->getMessage());

            DB::rollback();
            $output = [
                'success' => 0,
                'msg' => __('vouchers::voucher.something_went_wrong')
            ];
        }

        return back()->with('status', $output);
    }

	/*  Show individual Voucher */

    public function show($voucher,Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');
        $business_locations = BusinessLocation::forDropdown($business_id);
        $accounts = Account::where('business_id', $business_id)->pluck('name','id');
        $users = User::forDropdown($business_id, true, true);
        $voucher_ids=Voucher::where('business_id',$business_id)->where('type','CP')->pluck('ref_no')->toArray();
        $voucher=Voucher::where('business_id', $business_id)->with(['account_transactions','account_transactions.account'])->findorfail($voucher);

        return view('vouchers::show')->with(compact('accounts', 'business_locations', 'users','voucher'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            try {
                $business_id = request()->session()->get('user.business_id');

                DB::beginTransaction();
                $voucher = Voucher::where('business_id', $business_id)->where('id', $id)->first();
                $voucher->delete();

                //Delete account transactions
                AccountTransaction::where('voucher_id', $voucher->id)->delete();
                DB::commit();

                $output = ['success' => true,
                    'msg' => __("vouchers::voucher.voucher_delete_success")
                ];
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

                DB::rollback();
                $output = ['success' => false,
                    'msg' => __("vouchers::voucher.something_went_wrong")
                ];
            }

            return $output;
        }
    }

    /*  Download individual Voucher */

    public function downloadVoucher($voucher, Request $request)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        app('debugbar')->disable();

        $printing = false;

        $business_id = request()->session()->get('user.business_id');

        $voucher = Voucher::where('business_id', $business_id)->with(['account_to', 'account_from', 'account_transactions', 'account_transactions.account', 'account_transactions'])->findorfail($voucher);

        $receipt = $this->receiptContent($business_id, $voucher->location_id, $voucher);

        if (!empty($receipt['data'])) {
            $receipt_details = $receipt['data'];
        } else {
            $receipt_details = $receipt['html_content'];
        }

        if ($voucher->type == 'JV') {
            $credit_total = 0;
            $debit_total = 0;
            foreach ($voucher->account_transactions as $key => $account_transaction) {

                if ($account_transaction->type == 'credit') {
                    $credit_total += $account_transaction->amount;
                }

                if ($account_transaction->type == 'debit') {
                    $debit_total += $account_transaction->amount;
                }
            }


            $view = view('vouchers::journal_voucher_receipt', compact('voucher', 'receipt_details', 'credit_total', 'debit_total', 'printing'))->render();

            $base_path = \public_path();

            $pdf = \App::make('dompdf.wrapper');
            $pdf->setOptions(['base_path', $base_path]);
            $pdf->loadHTML($view);

            if ($request->get('submit') == 'download') {
                return $pdf->download($voucher->ref_no . '.pdf');
            }

            return $pdf->stream();

        } else {

            $previous_total = 0;

            $account_transaction = AccountTransaction::join(
                'accounts as A',
                'account_transactions.account_id',
                '=',
                'A.id'
            )
                ->where('A.business_id', $business_id)
                ->where('A.id', $voucher->account_from->id)
                ->where('account_transactions.voucher_id', $voucher->id)
                ->with(['transaction', 'transaction.contact', 'transfer_transaction'])
                ->select(['type', 'amount', 'operation_date',
                    'sub_type', 'transfer_transaction_id',
                    DB::raw('(SELECT SUM(IF(AT.type="credit", AT.amount, -1 * AT.amount)) from account_transactions as AT WHERE AT.operation_date <= account_transactions.operation_date AND AT.account_id  =account_transactions.account_id AND AT.deleted_at IS NULL AND AT.id <= account_transactions.id) as balance'),
                    'transaction_id',
                    'account_transactions.id',
                    'account_transactions.voucher_id'
                ])
                ->groupBy('account_transactions.id')
                ->orderBy('account_transactions.id', 'desc')
                ->orderBy('account_transactions.operation_date', 'desc')->first();

            $after_total = $account_transaction->balance;

            if ($account_transaction->type == 'credit') {
                $previous_total = $after_total - $voucher->amount;
            }

            if ($account_transaction->type == 'debit') {
                $previous_total = $after_total + $voucher->amount;
            }

            $base_path = \public_path();
            //$view=view('vouchers::showDownload',compact('voucher','receipt_details','account_transaction','after_total','previous_total','printing'))->render();

            $view = view('vouchers::receipt', compact('voucher', 'receipt_details', 'account_transaction', 'after_total', 'previous_total', 'printing'));

            $pdf = \App::make('dompdf.wrapper');
            $pdf->setOptions(['base_path', $base_path]);
            $pdf->loadHTML($view);

            if ($request->get('submit') == 'download') {
                return $pdf->download($voucher->ref_no . '.pdf');
            }

            return $pdf->stream();

        }
    }

    /*  Print individual Voucher */

    public function printVoucher($voucher, $save_and_print = 0)
    {
        if (!auth()->user()->can('account.access')) {
            abort(403, 'Unauthorized action.');
        }

        $user_business = Auth::user()->business_id;
        $business = Business::whereId($user_business)->first(['name']);
        $printing = true;

        app('debugbar')->disable();

        $business_id = request()->session()->get('user.business_id');

        $voucher = Voucher::where('business_id', $business_id)->with(['account_to', 'account_from', 'account_transactions', 'account_transactions.account', 'account_transactions'])->findorfail($voucher);

        $receipt = $this->receiptContent($business_id, $voucher->location_id, $voucher);

        if (!empty($receipt['data'])) {
            $receipt_details = $receipt['data'];
        } else {
            $receipt_details = $receipt['html_content'];
        }

        if ($voucher->type == 'JV') {
            $credit_total = 0;
            $debit_total = 0;

            foreach ($voucher->account_transactions as $key => $account_transaction) {

                if ($account_transaction->type == 'credit') {
                    $credit_total += $account_transaction->amount;
                }

                if ($account_transaction->type == 'debit') {
                    $debit_total += $account_transaction->amount;
                }
            }

            if ($save_and_print == 1){
                return view('vouchers::show_print_jv_voucher', compact('voucher', 'business', 'receipt_details', 'account_transaction', 'credit_total', 'debit_total', 'printing'));
            }else {
                $output = ['success' => 1, 'receipt' => []];
                $output['receipt']['html_content'] = view('vouchers::journal_voucher_receipt', compact('voucher', 'business', 'receipt_details', 'credit_total', 'debit_total', 'printing'))->render();
                return $output;
            }

        } else {

            $previous_total = 0;
            $after_total = 0;

            $account_transaction = AccountTransaction::join(
                'accounts as A',
                'account_transactions.account_id',
                '=',
                'A.id'
            )
                ->where('A.business_id', $business_id)
                ->where('A.id', $voucher->to_account)
                ->where('account_transactions.voucher_id', $voucher->id)
                ->with(['transaction', 'transaction.contact', 'transfer_transaction'])
                ->select(['type', 'amount', 'operation_date',
                    'sub_type', 'transfer_transaction_id',
                    DB::raw('(SELECT SUM(IF(AT.type="credit", AT.amount, -1 * AT.amount)) from account_transactions as AT WHERE AT.operation_date <= account_transactions.operation_date AND AT.account_id  = account_transactions.account_id AND AT.deleted_at IS NULL AND AT.id <= account_transactions.id) as balance'),
                    'transaction_id',
                    'account_transactions.id',
                    'account_transactions.voucher_id',
                    'account_transactions.id',

                ])
                ->groupBy('account_transactions.id')
                ->orderBy('account_transactions.id', 'desc')
                ->orderBy('account_transactions.operation_date', 'desc')->first();

            if ($voucher->type == 'CP' || $voucher->type == 'BP') {
                $account = Account::whereId($voucher->to_account)->value('contact_id');
                $contact = Contact::whereId($account)->value('type');
            }
            if ($voucher->type == 'CR' || $voucher->type == 'BR') {
                $account = Account::whereId($voucher->account_from->id)->value('contact_id');
                $contact = Contact::whereId($account)->value('type');
            }

            if ($contact == 'supplier') {
                if ($voucher->type == 'CP' || $voucher->type == 'BP') {
                    $atBalance = AccountTransaction::where('type', "credit")->where('id', '<', $account_transaction->id)->where('account_id', $voucher->to_account)->whereDate('operation_date', '<=', $account_transaction->operation_date)->sum('amount');
                    $debit = AccountTransaction::where('type', "debit")->where('id', '<', $account_transaction->id)->where('account_id', $voucher->to_account)->whereDate('operation_date', '<=', $account_transaction->operation_date)->sum('amount');

                    $previous_total = $atBalance - $debit;
                    if ($account_transaction->type == 'credit') {
                        $after_total = $previous_total + $voucher->amount;
                    }
                    if ($account_transaction->type == 'debit') {
                        $after_total = $previous_total - $voucher->amount;
                    }
                }

                if ($voucher->type == 'CR' || $voucher->type == 'BR') {
                    $atBalance = AccountTransaction::where('type', "credit")->where('id', '<', $account_transaction->id)->where('account_id', $voucher->account_from->id)->whereDate('operation_date', '<=', $account_transaction->operation_date)->sum('amount');
                    $debit = AccountTransaction::where('type', "debit")->where('id', '<', $account_transaction->id)->where('account_id', $voucher->account_from->id)->whereDate('operation_date', '<=', $account_transaction->operation_date)->sum('amount');

                    $previous_total = $atBalance - $debit;
                    if ($account_transaction->type == 'credit') {
                        $after_total = $previous_total - $voucher->amount;
                    }

                    if ($account_transaction->type == 'debit') {
                        $after_total = $previous_total + $voucher->amount;
                    }
                }
            }

            ////////////////////////end supplier ////////////////////////
            if ($contact == 'customer') {
                if ($voucher->type == 'CP' || $voucher->type == 'BP') {
                    $atBalance = AccountTransaction::where('type', "debit")->where('id', '<', $account_transaction->id)->where('account_id', $voucher->to_account)->whereDate('operation_date', '<=', $account_transaction->operation_date)->sum('amount');
                    $credit = AccountTransaction::where('type', "credit")->where('id', '<', $account_transaction->id)->where('account_id', $voucher->to_account)->whereDate('operation_date', '<=', $account_transaction->operation_date)->sum('amount');
                    $previous_total = $atBalance - $credit;
                    if ($account_transaction->type == 'credit') {
                        $after_total = $previous_total - $voucher->amount;
                    }

                    if ($account_transaction->type == 'debit') {
                        $after_total = $previous_total + $voucher->amount;
                    }

                }

                if ($voucher->type == 'CR' || $voucher->type == 'BR') {

                    $atBalance = AccountTransaction::where('type', "debit")->where('id', '<', $account_transaction->id)->where('account_id', $voucher->account_from->id)->whereDate('operation_date', '<=', $account_transaction->operation_date)->sum('amount');
                    $credit = AccountTransaction::where('type', "credit")->where('id', '<', $account_transaction->id)->where('account_id', $voucher->account_from->id)->whereDate('operation_date', '<=', $account_transaction->operation_date)->sum('amount');

                    $previous_total = $atBalance - $credit;
                    if ($account_transaction->type == 'credit') {
                        $after_total = $previous_total + $voucher->amount;
                    }

                    if ($account_transaction->type == 'debit') {
                        $after_total = $previous_total - $voucher->amount;
                    }

                }
            }
            if ($save_and_print == 1){
                return view('vouchers::show_print_voucher', compact('voucher', 'business', 'receipt_details', 'account_transaction', 'after_total', 'previous_total', 'printing'));
            }else{
                $output = ['success' => 1, 'receipt' => []];
                $output['receipt']['html_content'] = view('vouchers::receipt', compact('voucher', 'business', 'receipt_details', 'account_transaction', 'after_total', 'previous_total', 'printing'))->render();
                return $output;
            }
        }
    }

    public function receiptContent(
        $business_id,
        $location_id,
        $voucher,
        $printer_type = null,
        $from_pos_screen = true
    )
    {
        $output = ['is_enabled' => false,
            'print_type' => 'browser',
            'html_content' => null,
            'printer_config' => [],
            'data' => []
        ];

        $voucher_id = $voucher->id;

        $business_details = $this->businessUtil->getDetails($business_id);
        $location_details = BusinessLocation::find($location_id);

        if ($from_pos_screen && $location_details->print_receipt_on_invoice != 1) {
            return $output;
        }
        //Check if printing of invoice is enabled or not.
        //If enabled, get print type.
        $output['is_enabled'] = true;

        $invoice_layout = $this->businessUtil->invoiceLayout($business_id, $location_id, $location_details->invoice_layout_id);

        //Check if printer setting is provided.
        $receipt_printer_type = is_null($printer_type) ? $location_details->receipt_printer_type : $printer_type;

        $receipt_details = $this->voucherUtil->getReceiptDetails($voucher_id, $location_id, $invoice_layout, $business_details, $location_details, $receipt_printer_type);

        $currency_details = [
            'symbol' => $business_details->currency_symbol,
            'thousand_separator' => $business_details->thousand_separator,
            'decimal_separator' => $business_details->decimal_separator,
        ];
        $receipt_details->currency = $currency_details;

        //If print type browser - return the content, printer - return printer config data, and invoice format config
        if ($receipt_printer_type == 'printer') {
            $output['print_type'] = 'printer';
            $output['printer_config'] = $this->businessUtil->printerConfig($business_id, $location_details->printer_id);
            $output['data'] = $receipt_details;
        } else {
            $output['html_content'] = $receipt_details;
        }

        return $output;
    }
}
