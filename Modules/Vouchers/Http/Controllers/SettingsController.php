<?php

namespace Modules\Manufacturing\Http\Controllers;

use App\Business;
use App\System;
use App\Utils\ModuleUtil;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Manufacturing\Utils\ManufacturingUtil;

class SettingsController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $vouchersUtil;
    protected $moduleUtil;

    /**
     * Constructor
     *
     * @param ProductUtils $product
     * @return void
     */
    public function __construct(ModuleUtil $moduleUtil, ManufacturingUtil $mfgUtil, ManufacturingUtil $vouchersUtil)
    {
        $this->moduleUtil = $moduleUtil;
        $this->vouchersUtil = $vouchersUtil;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $business_id = request()->session()->get('user.business_id');
        if (!(auth()->user()->can('superadmin') || $this->moduleUtil->hasThePermissionInSubscription($business_id, 'vouchers_module'))) {
            abort(403, 'Unauthorized action.');
        }

        //start from here
        $vouchers_settings = $this->vouchersUtil->getSettings($business_id);

        $version = System::getProperty('vouchers_version');
        return view('vouchers::settings.index')->with(compact('vouchers_settings', 'version'));
    }

}
