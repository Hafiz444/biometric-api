<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Business Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during registration of a business.
    |
    */
    'voucher' => 'Voucher',
    'amount' => 'Amount',
    'print' => 'Print',
    'download' => 'Download',
    'type' => 'Type',
    'account' => 'Account',
    'save_and_print' => 'Save and Print',
    'ref_no' => 'Ref No',
    'to_account' => 'Account To',
    'from_account' => 'Account From',
    'add_cash_received' => 'Cash received',
    'cash_received' => 'Cash receive',
    'add_bank_payment' => 'Add bank payment Voucher',
    'bank_payment' => 'Bank Payment',
    'bank_received' => 'Bank Received',


];
