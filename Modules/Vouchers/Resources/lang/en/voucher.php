<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Business Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during registration of a business.
    |
    */
    'vouchers_module' => 'Vouchers Module',
    'vouchers' => 'Vouchers',
    'sidebar_label' => 'Voucher',
    'add_cash_payment_voucher' =>'Cash payments vouchers',
    'add_cash_payment' =>'Cash Payments',
    'add_voucher' =>'Add Voucher',
    'cash_payment' =>'Cash Payment',
    'cash_received' =>'Cash Received',
    'add_cash_received' => 'Cash Received',
    'add_bank_payment' => 'Add bank payment',
    'bank_payment' => 'Bank Payment',
    'bank_received' => 'Bank Received',
    'date' => 'Date',
    'voucher_from_account' => 'From account',
    'voucher_to_account' =>'To account',
    'account' =>'Account',
    'from_account' => 'Voucher from account',
    'to_account' =>'Voucher to account',
    'cash_payment_voucher_added' =>'Cash Payment Voucher Added Successfully',
    'cash_received_payment_voucher_added' =>'Cash Received Voucher Added Successfully',
    'amount' => 'Amount',
    'add_bank_received' => 'Add bank received',
    'bank_recevied' => 'Bank Recevied',
    'add_journal' => 'Add journal',
    'add_cash_received_voucher' =>'Cash received vouchers',
    'cash_received_voucher' =>'Cash received voucher',
    'add_bank_payment_voucher'=>'Bank payments vouchers Added Successfully',
    'bank_payment_voucher'=>'Bank payments voucher',
    'add_bank_received_voucher'=>'Bank received vouchers',
    'bank_received_voucher'=>'Bank received voucher added successfully',
    'add_journal_voucher'=>'Add Journal vouchers',
    'added_journal_voucher'=>'Journal voucher added successfully',
    'something_went_wrong' => 'Something Went Wrong',
    'journal_voucher'=>'Journal Voucher',
    'ref_no'=> 'Ref No.',
    'type'=> 'Type',
    'voucher_credit' => 'Voucher Credit',
    'voucher_debit' => 'Voucher Debit',
    'credit' => 'Credit',
    'debit' => 'Debit',
    'received_by' =>'Received By',
    'authorised_by' =>'Authorised By',
    'voucher_no' =>'Voucher No',
    'voucher_date' =>'Voucher Date',
    'note' => 'Note',
    'voucher_delete_success' => 'Voucher Deleted Successfully',
    'action' => 'Action'
];
