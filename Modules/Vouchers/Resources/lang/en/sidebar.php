<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Business Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during registration of a business.
    |
    */
    'main_label' => 'Voucher',    
	'cash_payment_voucher' =>'Cash Payment Voucher',
	'cash_received_voucher' =>'Cash Received Voucher',
	'bank_payment_voucher' =>'Bank Payment Voucher',
	'bank_received_voucher' =>'Bank Received Voucher',
	'journal_voucher' =>'Journal Voucher',
];
