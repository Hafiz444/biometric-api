@extends('layouts.app')
@section('title', __('vouchers::voucher.add_voucher'))

@section('content')
<style>
.voucher_account{
	width:340px;

}
/* .add-new-voucher-form-journal{
	margin-top:25px;
} */
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('vouchers::voucher.add_journal_voucher')</h1>
</section>

<!-- Main content -->
<section class="content">
	{!! Form::open(['url' => action('\Modules\Vouchers\Http\Controllers\VoucherController@storeJournalVoucher'), 'method' => 'post', 'id' => 'add_cash_payment_voucher_form', 'files' => true ]) !!}
	<div class="box box-solid">
		<div class="box-body">
			<div class="row">
				@if(count($business_locations) == 1)
					@php 
						$default_location = current(array_keys($business_locations->toArray())) 
					@endphp
				@else
					@php $default_location = null; @endphp
				@endif				
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('location_id', __('purchase.business_location').':*') !!}
						{!! Form::select('location_id', $business_locations, $default_location, ['class' => 'form-control select2', 'placeholder' => __('messages.please_select'), 'required']); !!}
					</div>
				</div>
				{{--<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('ref_no', __('purchase.ref_no').':') !!}
						{!! Form::text('visible_ref_no','JV-'.$voucher_id, ['class' => 'form-control','disabled']); !!}
						{!! Form::hidden('ref_no', $voucher_id, ['class' => 'form-control']); !!}
					</div>
				</div>--}}
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('date', __('messages.date') . ':*') !!}
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							{!! Form::text('date', $default_datetime, ['class' => 'form-control', 'required', 'id' => 'date']); !!}
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			
						<div class="col-sm-4">
							<div class="form-group">
								{!! Form::label('account_transaction[0][from_account]', __('vouchers::voucher.account').':') !!}
								{!! Form::select('account_transaction[0][from_account]', $accounts, null, ['class' => 'form-control voucher-form-select2 required', 'placeholder' => __('messages.please_select'), 'required']); !!}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								{!! Form::label('account_transaction[0][to_account]', __('vouchers::voucher.credit').':') !!} / {!! Form::label('account_transaction[0][to_account]', __('vouchers::voucher.debit').':') !!}
								<select name="account_transaction[0][type]" class="form-control v_type" id="0" required data-id="0">
								        <option value="">select option</option>
										<option value="credit">credit</option>
										<option value="debit">debit</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4 amount_div">
							{!! Form::label('amount', __('vouchers::voucher.amount') . ':*') !!}
							<div class="input-group">
								{!! Form::text('account_transaction[0][amount]', null, ['class' => 'form-control input_number amount_voucher', 'placeholder' => __('vouchers::voucher.amount'), 'required']); !!}

								<span class="input-group-btn">
			                    	<a href="#" class="btn btn-primary pull-right add-new-voucher-form-journal">@lang('messages.add')</a>
			                   </span>
								</div>
						</div>
						<!-- <div class="col-sm-1">
						    <span class="input-group-btn">
			                    	<a href="#" class="btn btn-primary pull-right add-new-voucher-form-journal">@lang('messages.add')</a>
			                </span>
						</div> -->


			</div>
			<div class="main_div_journal_voucher"></div>
			
			<div class="row">
				<div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('document', __('purchase.attach_document') . ':') !!}
                        {!! Form::file('document', ['id' => 'upload_document']); !!}
                        <p class="help-block">@lang('purchase.max_file_size', ['size' => (config('constants.document_size_limit') / 1000000)])</p>
                    </div>
                </div>
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('note', __('vouchers::voucher.note') . ':') !!}
								{!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 3]); !!}
					</div>
				</div>
				<div class="col-sm-12">
					<button type="botton" name="submit" value="save" class="btn btn-success pull-right submit-voucher">@lang('messages.save')</button>
					<button type="" name="submit" value="save_and_print" class="btn btn-primary pull-right submit-voucher">@lang('vouchers::default.save_and_print')</button>
				</div>
			
			</div>			
		</div>
	</div> <!--box end-->
	<div class="box">
        <div class="box-header">
        </div>
        <div class="box-body">
        	<div class="table-responsive">
                <table class="table table-bordered table-striped" id="journal_voucher_table">
                    <thead>
                        <tr>
                            <th>@lang('messages.date')</th>
                            <th>@lang('vouchers::voucher.ref_no')</th>
							<th>@lang('vouchers::voucher.action')</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
{!! Form::close() !!}
</section>
@endsection


@section('javascript')
<script type="text/javascript">
$(document).ready( function(){

	//Date picker
	$('#date').datetimepicker({
		format: moment_date_format + ' ' + moment_time_format,
		ignoreReadonly: true,
	});

    journal_voucher_table = $('#journal_voucher_table').DataTable({
        processing: true,
        serverSide: true,
        aaSorting: [[1, 'desc']],
        ajax: "{{ url()->current() }}",
        columns: [
            { data: 'date', name: 'date'  },
            { data: 'ref_no', name: 'ref_no'},
			{ data: 'action', name: 'action'}
        ]
    });

	// delete journal voucher and its transaction
	$(document).on('click', 'a.delete_voucher', function(e) {
		e.preventDefault();
		swal({
			title: LANG.sure,
			text: LANG.confirm_delete_voucher,
			icon: 'warning',
			buttons: true,
			dangerMode: true,
		}).then(willDelete => {
			if (willDelete) {
				var href = $(this).attr('href');
				var data = $(this).serialize();
				$.ajax({
					url: href,
					dataType: 'json',
					data: data,
					success: function(result) {
						if (result.success === true) {
							toastr.success(result.msg);
							journal_voucher_table.ajax.reload();
						} else {
							toastr.error(result.msg);
						}
					},
				});
			}
		});
	});
	var voucherId = 1;
	$(document).on('click','.add-new-voucher-form-journal',function(){
		$('.main_div_journal_voucher').
		append('<div class="row remove_div">'+
						'<div class="col-sm-4">'+
							'<div class="form-group">'+
								'<label>{{ __("vouchers::voucher.account")}}</lable>'+
								
								'<select name="account_transaction['+voucherId+'][from_account]" class="form-control voucher_account voucher-form-select2" required data-id="'+voucherId+'" id="'+voucherId+'">'+
								        '@if(count($accountsAdd) > 0)'+
										'@foreach($accountsAdd as $key)'+
										  '<option value="{{$key->id}}">{{$key->name}}</option>'+
										'@endforeach'+
										'@endif'+
								'</select>'+
							'</div>'+
						'</div>'+
						'<div class="col-sm-4">'+
							'<div class="form-group">'+
								'<label>{{ __("vouchers::voucher.credit")}}</label> / <label>{{ __("vouchers::voucher.debit")}}</label>'+ 
								'<select name="account_transaction['+voucherId+'][type]" class="form-control v_type" required data-id="'+voucherId+'" id="'+voucherId+'">'+
								        '<option value="">select option</option>'+	
										'<option value="credit">credit</option>'+
										'<option value="debit">debit</option>'+
								'</select>'+
							'</div>'+
						'</div>	'+	
						'<div class="col-sm-4 amount_div">'+
							'<lable>{{ __("vouchers::voucher.amount")}}</label>'+
							'<div class="input-group">'+						
								'<input type="text" name="account_transaction['+voucherId+'][amount]" class= "form-control input_number amount_voucher"  required ata-id="'+voucherId+'"  id="amount'+voucherId+'">'+
								'<span class="input-group-btn">'+
			                    	'<a href="#" class="btn btn-danger pull-right remove-voucher-form-journal" data-id="'+voucherId+'">remove</a>'+
			                    '</span>'+
							'</div>'+					
						'</div>'+		
		'</div>');
		voucherId ++;
	});
	////////////////////////////////////////////////////////
	$(document).on('click','.remove-voucher-form-journal',function(){
		val = $(this).val();
		data = $(this).attr('data-id');
		parent = $(this).parents('.input-group-btn').parents('.input-group').parents('.col-sm-4').parents('.remove_div').remove();
		console.log(parent);
	});
	
});
///////////////////////////
$(document).on('click','.submit-voucher',function(){
	val = $(this).val();
	var credit=0;
	var debit=0;
	$('.v_type').each(function(){
		type = $(this).val();
		if(type==='credit'){
			credit = +credit + +$(this).parents('.form-group').parents('.col-sm-4').siblings('.amount_div').children().find('.amount_voucher').val();
		}
		if(type==='debit'){
			debit = +debit + +$(this).parents('.form-group').parents('.col-sm-4').siblings('.amount_div').children().find('.amount_voucher').val();
		}
		
	});
	console.log('credit'+credit+'debit'+debit);
	if(credit ==0 || debit ==0){
      alert('please  check debit and credit and amount');
	  return false;
	}
    else if(debit != credit){
        alert('debit and credit not equal amount please check it ');
		return false;
	}
	
	else{
		return true;
	}
	
});
</script>
@endsection
