@extends('layouts.app')
@section('title', __('vouchers::voucher.add_voucher'))

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('vouchers::voucher.add_bank_received')</h1>
</section>

<!-- Main content -->
<section class="content">
	{!! Form::open(['url' => action('\Modules\Vouchers\Http\Controllers\VoucherController@storeBankReceivedVoucher'), 'method' => 'post', 'id' => 'add_cash_payment_voucher_form', 'files' => true ]) !!}
	<div class="box box-solid">
		<div class="box-body">
			<div class="row">

				@if(count($business_locations) == 1)
					@php 
						$default_location = current(array_keys($business_locations->toArray())) 
					@endphp
				@else
					@php $default_location = null; @endphp
				@endif
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('location_id', __('purchase.business_location').':*') !!}
						{!! Form::select('location_id', $business_locations, $default_location, ['class' => 'form-control select2', 'placeholder' => __('messages.please_select'), 'required']); !!}
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('date', __('messages.date') . ':*') !!}
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							{!! Form::text('date', $default_datetime, ['class' => 'form-control', 'required', 'id' => 'date']); !!}
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('from_account', __('vouchers::voucher.from_account').':') !!}
						{!! Form::select('from_account', $accounts, null, ['class' => 'form-control select2 required', 'placeholder' => __('messages.please_select'), 'required']); !!}
					</div>
				</div>
					<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('to_account', __('vouchers::voucher.to_account').':') !!}
						{!! Form::select('to_account', $cash_at_bank_accounts, null, ['class' => 'form-control select2 required', 'placeholder' => __('messages.please_select'), 'required']); !!}
					</div>
				</div>
		
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('amount', __('vouchers::voucher.amount') . ':*') !!}
						{!! Form::text('amount', null, ['class' => 'form-control input_number', 'placeholder' => __('vouchers::voucher.amount'), 'required']); !!}
					</div>
				</div>
				<div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('document', __('purchase.attach_document') . ':') !!}
                        {!! Form::file('document', ['id' => 'upload_document']); !!}
                        <p class="help-block">@lang('purchase.max_file_size', ['size' => (config('constants.document_size_limit') / 1000000)])</p>
                    </div>
                </div>
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('note', __('vouchers::voucher.note') . ':') !!}
								{!! Form::textarea('note', null, ['class' => 'form-control', 'rows' => 3]); !!}
					</div>
				</div>
				<div class="col-sm-12">
					<button type="submit" class="btn btn-success pull-right">@lang('messages.save')</button>
					<button type="" name="submit" value="save_and_print" class="btn btn-primary pull-right submit-voucher">@lang('vouchers::default.save_and_print')</button>
				</div>
			</div>
		</div>
	</div> <!--box end-->

	<div class="box">
        <div class="box-header">
        	<h3 class="box-title">Ledger</h3>
        </div>
        <div class="box-body">
        	<div class="table-responsive">
                <table class="table table-bordered table-striped" id="add_bank_received_voucher_table">
                     <thead>
                        <tr>
                            <th>@lang('messages.date')</th>
                            <th>@lang('vouchers::default.ref_no')</th>
                            <th>@lang('vouchers::default.to_account')</th>
                            <th>@lang('vouchers::default.from_account')</th>
                            <th>@lang('vouchers::default.amount')</th>
							<th>@lang('vouchers::voucher.action')</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
	{!! Form::close() !!}
</section>
@endsection


@section('javascript')
<script type="text/javascript">
	$(document).ready( function(){

		//Date picker
		$('#date').datetimepicker({
			format: moment_date_format + ' ' + moment_time_format,
			ignoreReadonly: true,
		});

		add_bank_received_voucher_table = $('#add_bank_received_voucher_table').DataTable({
			processing: true,
			serverSide: true,
			aaSorting: [[1, 'desc']],
			ajax: "{{ url()->current() }}",
			columns: [
				{ data: 'date', name: 'date'  },
				{ data: 'ref_no', name: 'ref_no'},
				{ data: 'account_to.name', name: 'account_to.name',sortable : false},
				{ data: 'account_from.name', name: 'account_from.name',sortable : false},
				{ data: 'amount', name: 'amount'},
				{ data: 'action', name: 'action'}
			]
		});

		// delete voucher
		$(document).on('click', 'a.delete_voucher', function(e) {
			e.preventDefault();
			swal({
				title: LANG.sure,
				text: LANG.confirm_delete_voucher,
				icon: 'warning',
				buttons: true,
				dangerMode: true,
			}).then(willDelete => {
				if (willDelete) {
					var href = $(this).attr('href');
					var data = $(this).serialize();
					$.ajax({
						url: href,
						dataType: 'json',
						data: data,
						success: function(result) {
							if (result.success === true) {
								toastr.success(result.msg);
								add_bank_received_voucher_table.ajax.reload();
							} else {
								toastr.error(result.msg);
							}
						},
					});
				}
			});
		});
	});
</script>
@endsection
