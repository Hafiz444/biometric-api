@extends('layouts.app')
@section('title', __("Vouchers") )

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>@lang('vouchers::default.voucher') {{ $voucher->ref_no }}</h1>
</section>

<!-- Main content -->
<section class="content">	
	<div class="box box-solid">
		<div class="box-body">
			<div class="row">
				{!! Form::open(['url' => action('\Modules\Vouchers\Http\Controllers\VoucherController@downloadVoucher',$voucher), 'method' => 'post', 'id' => 'voucher_form', 'files' => true ]) !!}
				@if(count($business_locations) == 1)
					@php 
						$default_location = current(array_keys($business_locations->toArray())) 
					@endphp
				@else
					@php $default_location = null; @endphp
				@endif
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('location_id', __('purchase.business_location').':*') !!}
						{!! Form::select('location_id', $business_locations, $default_location, ['class' => 'form-control select2', 'placeholder' => __('messages.please_select'), 'required','disabled']); !!}
					</div>
				</div>

				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('ref_no', __('purchase.ref_no').':') !!}
						{!! Form::text('ref_no', $voucher->ref_no, ['class' => 'form-control','disabled']); !!}
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('date', __('messages.date') . ':*') !!}
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							{!! Form::text('date', @format_datetime($voucher->date), ['class' => 'form-control', 'required', 'id' => 'date','disabled']); !!}
						</div>
					</div>
				</div>
				@if($voucher->type != 'JV')
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('from_account', __('vouchers::voucher.voucher_from_account').':') !!}
						{!! Form::select('from_account',  $accounts, $voucher->from_account, ['class' => 'form-control select2 required', 'placeholder' => __('messages.please_select'), 'required','disabled']); !!}
					</div>
				</div>
					<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('to_account', __('vouchers::voucher.voucher_to_account').':') !!}
						{!! Form::select('to_account', $accounts, $voucher->to_account, ['class' => 'form-control select2 required', 'placeholder' => __('messages.please_select'), 'required','disabled']); !!}
					</div>
				</div>
		
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('amount', __('vouchers::voucher.amount') . ':*') !!}
						{!! Form::text('amount', $voucher->amount, ['class' => 'form-control input_number', 'placeholder' => __('voucher.amount'), 'required','disabled']); !!}
					</div>
				</div>
				@endif				
				<div class="col-sm-4">
					<div class="form-group">
						{!! Form::label('note', __('vouchers::voucher.note') . ':') !!}
								{!! Form::textarea('note', $voucher->note, ['class' => 'form-control', 'rows' => 3,'disabled']); !!}
					</div>
				</div>
				<div class="col-sm-12">
					<a data-href="{{ action('\Modules\Vouchers\Http\Controllers\VoucherController@printVoucher',$voucher) }}" class="btn btn-primary pull-right print-voucher">@lang('messages.print')</a>
					<!-- <button type="submit" name="submit" value="download" class="btn btn-primary pull-right">@lang('vouchers::default.download')</button> -->
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div> <!--box end-->
	@if($voucher->type == 'JV')
	<div class="box">
        <div class="box-header">
        	<h3 class="box-title">Transaction</h3>
        </div>
        <div class="box-body">
        	<div class="table-responsive">
                <table class="table table-bordered table-striped" 
                id="journal_voucher_table">
                    <thead>
                        <tr>
                            <th>@lang('messages.date')</th>                            
                            <th>@lang('vouchers::default.type')</th>                            
                            <th>@lang('vouchers::default.amount')</th>                            
                            <th>@lang('vouchers::default.account')</th>                            
                        </tr>
                    </thead>
                    <tbody>
                    	@foreach($voucher->account_transactions as $account_transaction)
                    	<tr>
                            <td>{{ $voucher->date }}</td>
                            <td>{{ ucwords($account_transaction->type) }}</td>
                            <td>{{ $account_transaction->amount }}</td>
                            <td>{{ $account_transaction->account->name }}</td>
                        </tr>
                        @endforeach                    	
                    </tbody>
                </table>
            </div>
        </div>
    </div>
	@endif
</section>
@endsection
