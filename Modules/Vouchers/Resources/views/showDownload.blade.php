<!DOCTYPE html>
<html>
<head>
    <title>@lang('voucher.voucher_no') {{ $voucher->ref_no}}</title>
{{--    @if(isset($printing))--}}
{{--        @include('layouts.partials.css')--}}
{{--    @else--}}
{{--        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" >--}}
{{--    @endif--}}
    <style>
        .height {
            min-height: 50px;
        }

        .bord{
            color: black;
            border-top: 1px dotted;
            padding-top: 10px;
        }
        .icon {
            font-size: 47px;
            color: #5CB85C;
        }

        .iconbig {
            font-size: 77px;
            color: #5CB85C;
        }

        .table > tbody > tr > .emptyrow {
            border-top: none;
        }

        .table > thead > tr > .emptyrow {
            border-bottom: none;
        }

        .table > tbody > tr > .highrow {
            border-top: 3px solid;
        }
        footer {
            position: fixed;
            bottom: -20px;
            left: 0px;
            right: 0px;
            height:20px;

        }
    </style>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-xs-12" style="">
            <div class="text-center">
                <i class="fa fa-search-plus float-xs-left icon"></i>
                <h3>{{ $receipt_details->business_name }}</h3>
                <br>{{ $receipt_details->location_name }}<br>
                @switch($voucher->type)
                    @case('CP')
                    <h2 class="text-center"><strong>@lang('voucher.cash_payment')</strong></h2>
                    @break
                    @case('CR')
                    <h2 class="text-center"><strong>@lang('voucher.cash_received')</strong></h2>
                    @break
                    @case('BP')
                    <h2 class="text-center"><strong>@lang('voucher.bank_payment')</strong></h2>
                    @break
                    @case('BR')
                    <h2 class="text-center"><strong>@lang('voucher.bank_recevied')</strong></h2>
                    @break
                    @case('JV')
                    <h2 class="text-center"><strong>@lang('voucher.journal_voucher')</strong></h2>
                    @break
                    @default
                    <h2 class="text-center"><strong>@lang('voucher.journal_voucher')</strong></h2>
                    @break
                @endswitch
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="card height">
                        <div class="card-block">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <td>@lang('voucher.voucher_no')  <strong>{{ $voucher->ref_no}}</strong></td>
                                        <td>@lang('voucher.voucher_date')  <strong>{{ $voucher->date}}</strong></td>
                                    </tr>
                                    </thead>
                                    <thead>
                                    <tr>
                                        <td>Description: </td>
                                        <td>{{ $voucher->note }}</td>
                                    </tr>
                                    </thead>
                                    <thead>
                                    <tr>
                                        <td>Paid To: <strong>{{ $voucher->account_to->name }}</strong></td>
                                        <td>Of Rs.: <strong>{{ $voucher->amount }}</strong></td>
                                    </tr>
                                    </thead>

                                    <thead>
                                    <tr>
                                        <td>Description: <strong> {{ $voucher->note }}</strong></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-3" style="margin-left: 50%">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <td>Previous Balance:</td>
                                        <td>{{ $previous_total}}</td>
                                    </tr>
                                    </thead>
                                    <thead>
                                    <tr>
                                        <td>Paid Amount:</td>
                                        <td>{{ $voucher->amount }}</td>
                                    </tr>
                                    </thead>
                                    <thead>
                                    <tr>
                                        <td><strong>Balance Amount:</strong></td>
                                        <td>{{ $after_total}}</td>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <td><strong>@lang('voucher.received_by')</strong> ______________________</td>
                            <td><strong>@lang('voucher.authorised_by'):</strong> ______________________</td>
                        </tr>
                        </thead>
                    </table>
                </div>



            </div>
        </div>
    </div>
</div>
</body>
</html>