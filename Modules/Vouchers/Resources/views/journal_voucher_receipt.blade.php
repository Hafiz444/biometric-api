<!DOCTYPE html>
<html>
<head>
    <title>@lang('vouchers::voucher.voucher_no') {{ $voucher->ref_no}}</title>
	@if(isset($printing))
		@include('layouts.partials.css')
	@else
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" >	
	@endif

<style>
.height {
    min-height: 50px;
}

.bord{
    color: black;
    border-top: 1px dotted;
    padding-top: 10px;
}
.icon {
    font-size: 47px;
    color: #5CB85C;
}

.iconbig {
    font-size: 77px;
    color: #5CB85C;
}

.table > tbody > tr > .emptyrow {
    border-top: none;
}

.table > thead > tr > .emptyrow {
    border-bottom: none;
}

.table > tbody > tr > .highrow {
    border-top: 3px solid;
}
footer {
	position: fixed; 
	bottom: -20px; 
	left: 0px; 
	right: 0px;
	height:20px; 

}
</style>
</head>

<body>

<div class="container">
    <div class="row" style="border-style: solid">
        <div class="col-xs-12" style="">
            <div class="text-center">
				<h3>{{  $business->name }} </h3>
                <br>{{  $business->b_address }}<br>
                <!-- <h3>{{ $receipt_details->business_name }}</h3>
                <br>{{ $receipt_details->location_name }}<br> -->
            	<h1><strong>@lang('vouchers::voucher.journal_voucher')</strong></h1>
            </div>
            <div class="row" >
                <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="card height">
                        <div class="card-block">
			                	<div class="table-responsive">
			                        <table class="table">
			                            <thead>
			                                <tr style="border-bottom-style: solid">
			                                    <td><strong>@lang('vouchers::voucher.voucher_no')</strong>: {{ $voucher->ref_no}}</td>
			                                    <td class="text-right"><strong>@lang('vouchers::voucher.voucher_date')</strong>: {{ $voucher->date}}</td>
			                                </tr>
			                                <tr >
			                                    <td colspan="2"><strong>Description:</strong> {{ $voucher->note }}</td>
			                                </tr>
			                            </thead>
			                        </table>
			                        <table class="table table-bordered">
			                            <thead class="thead-dark">
			                                <tr>
			                                    <td><strong>Sr # </strong></td>
												<td><strong>@lang('vouchers::voucher.account')</strong></td>
												<td><strong>@lang('vouchers::voucher.debit')</strong></td>
			                                    <td><strong>@lang('vouchers::voucher.credit')</strong></td>
			                                </tr>
			                            </thead>
			                            <tbody>
										@php
											$debit_total = 0;
											$credit_total = 0;
										@endphp
			                       			@foreach($voucher->account_transactions as $account_transaction)
												<tr>
													<td>{{ $loop->index + 1}}</td>
													<td>{{ $account_transaction->account->name }}</td>
													<td>
														@if($account_transaction->type == 'debit' )
															@php
															$debit_total = $debit_total + $account_transaction->amount;
															@endphp
															@format_currency($account_transaction->amount)
														@endif
													</td>
													<td>
														@if($account_transaction->type == 'credit' )
															@php
																$credit_total = $credit_total + $account_transaction->amount;
															@endphp
															@format_currency($account_transaction->amount)
														@endif
													</td>
												</tr>
											@endforeach
			                            </tbody>
										<tfoot>
										<tr class="font-17 text-bold text-center">
											<td colspan="2">Total</td>
											<td>@format_currency($debit_total)</td>
											<td>@format_currency($credit_total)</td>
										</tr>
										</tfoot>
			                        </table>
			                    </div>
		                    </div>
		                </div>
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <td><strong>@lang('vouchers::voucher.received_by'):</strong> ______________________</td>
                                    <td><strong>@lang('vouchers::voucher.authorised_by'):</strong> ______________________</td>
                                </tr>
                            </thead>
                        </table>
            </div>



        </div>
    </div>
</div>
<footer>
	<strong>Software Developed By KodeInn Technologies. Email: info@kodeinn.com -- Ph: 0304-4612345 </strong>
</footer>
</div>
</body>
</html>