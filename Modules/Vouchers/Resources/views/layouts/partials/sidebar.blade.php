@if($__is_vouchers_enabled)
    <li class="treeview {{ $request->segment(1) == 'voucher' ? 'active active-sub' : '' }}">
        <a href="#"><i class="fa fa-money" aria-hidden="true"></i> <span>@lang('vouchers::sidebar.main_label')</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            @can('vouchers.all_vouchers')
                <li class="{{ $request->segment(1) == 'voucher' && $request->segment(2) == 'add-cash-payment' ? 'active' : '' }}">
                    <a href="{{action('\Modules\Vouchers\Http\Controllers\VoucherController@addCashPaymentVoucher')}}">
                        <i class="fa fa-list"></i>@lang('vouchers::sidebar.cash_payment_voucher')
                    </a>
                </li>

                <li class="{{ $request->segment(1) == 'voucher' && $request->segment(2) == 'add-cash-received' ? 'active' : '' }}">
                    <a href="{{action('\Modules\Vouchers\Http\Controllers\VoucherController@addCashReceivedVoucher')}}">
                        <i class="fa fa-list"></i>@lang('vouchers::sidebar.cash_received_voucher')
                    </a>
                </li>

                <li class="{{ $request->segment(1) == 'voucher' && $request->segment(2) == 'add-bank-payment' ? 'active' : '' }}">
                    <a href="{{action('\Modules\Vouchers\Http\Controllers\VoucherController@addBankPaymentVoucher')}}">
                        <i class="fa fa-list"></i>@lang('vouchers::sidebar.bank_payment_voucher')
                    </a>
                </li>

                <li class="{{ $request->segment(1) == 'voucher' && $request->segment(2) == 'add-bank-received' ? 'active' : '' }}">
                    <a href="{{action('\Modules\Vouchers\Http\Controllers\VoucherController@addBankReceivedVoucher')}}">
                        <i class="fa fa-list"></i>@lang('vouchers::sidebar.bank_received_voucher')
                    </a>
                </li>

                <li class="{{ $request->segment(1) == 'voucher' && $request->segment(2) == 'add-journal' ? 'active' : '' }}"><a
                            href="{{action('\Modules\Vouchers\Http\Controllers\VoucherController@addJournalVoucher')}}">
                        <i class="fa fa-list"></i>@lang('vouchers::sidebar.journal_voucher')
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endif