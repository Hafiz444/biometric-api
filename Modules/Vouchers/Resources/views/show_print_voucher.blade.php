@extends('layouts.guest')
@section('content')
    <style>
        .height {
            min-height: 50px;
        }

        .bord {
            color: black;
            border-top: 1px dotted;
            padding-top: 10px;
        }

        .icon {
            font-size: 47px;
            color: #5CB85C;
        }

        .iconbig {
            font-size: 77px;
            color: #5CB85C;
        }

        .table > tbody > tr > .emptyrow {
            border-top: none;
        }

        .table > thead > tr > .emptyrow {
            border-bottom: none;
        }

        .table > tbody > tr > .highrow {
            border-top: 3px solid;
        }

        footer {
            position: fixed;
            bottom: -20px;
            left: 0px;
            right: 0px;
            height: 20px;

        }

        @page {
            size: auto;   /* auto is the initial value */
            margin: 0;  /* this affects the margin in the printer settings */
        }
    </style>

    <div class="container">
        <div class="spacer"></div>
        <div id="invoice_content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <small class="pull-left">{{date('d/m/Y')}}</small>
                    </div>
                    <div class="col-md-4">
                        <small class="pull-right">@lang('vouchers::voucher.voucher_no') {{ $voucher->ref_no}}</small>
                    </div>
                    <div class="col-md-4">&nbsp;</div>
                </div>

                <div class="row" style="border-style: solid">
                    <div class="col-xs-12" style="">
                        <div class="text-center">

                            <h3>{{  $business->name }} </h3>
                            <br><br>

                            @switch($voucher->type)
                                @case('CP')
                                <h2 class="text-center"><strong>@lang('vouchers::voucher.cash_payment')</strong></h2>
                                @break
                                @case('CR')
                                <h2 class="text-center"><strong>@lang('vouchers::voucher.cash_received')</strong></h2>
                                @break
                                @case('BP')
                                <h2 class="text-center"><strong>@lang('vouchers::voucher.bank_payment')</strong></h2>
                                @break
                                @case('BR')
                                <h2 class="text-center"><strong>@lang('vouchers::voucher.bank_recevied')</strong></h2>
                                @break
                                @case('JV')
                                <h2 class="text-center"><strong>@lang('vouchers::voucher.journal_voucher')</strong></h2>
                                @break
                                @default
                                <h2 class="text-center"><strong>@lang('vouchers::voucher.journal_voucher')</strong></h2>
                                @break
                            @endswitch
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-lg-12">
                                <div class="card height">
                                    <div class="card-block">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr style="border-bottom-style: solid">
                                                    <td><strong>@lang('vouchers::voucher.voucher_no')
                                                            :</strong> {{ $voucher->ref_no}}</strong></td>

                                                    <td class="text-right">
                                                        <strong>@lang('vouchers::voucher.voucher_date')
                                                            :</strong> {{ $voucher->date}}</td>
                                                </tr>
                                                <tr style="border-bottom-style: solid">
                                                    <td><strong>Paid To:</strong> {{ $voucher->account_to->name }}</td>
                                                    <td><strong>Of</strong> @format_currency($voucher->amount)</td>
                                                </tr>
                                                <tr style="border-bottom-style: solid">
                                                    <td><strong>From:</strong> {{ $voucher->account_from->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><strong>Description:</strong> {{ $voucher->note }}
                                                    </td>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-md-3" style="margin-left: 50%">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <td>Previous Balance:</td>
                                                    <td>@format_currency($previous_total)</td>
                                                </tr>
                                                </thead>
                                                <thead>
                                                <tr>
                                                    <td>Paid Amount:</td>
                                                    <td>@format_currency($voucher->amount)</td>
                                                </tr>
                                                </thead>
                                                <thead>
                                                <tr>
                                                    <td><strong>Balance Amount:</strong></td>
                                                    <td>@format_currency($after_total)</td>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <table class="table table-sm">
                                    <thead>
                                    <tr>
                                        <td><strong>@lang('vouchers::voucher.received_by')</strong>
                                            ______________________
                                        </td>
                                        <td><strong>@lang('vouchers::voucher.authorised_by'):</strong>
                                            ______________________
                                        </td>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <strong>Software Developed By KodeInn Technologies. Email: info@kodeinn.com -- Ph:
                    0304-4612345 </strong>
            </footer>
        </div>
        <div class="spacer"></div>
    </div>
@stop
@section('javascript')
    <script type="text/javascript">
        $(window).on('load', function () {
            window.print();
        });
    </script>
@endsection