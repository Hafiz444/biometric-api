<?php

namespace Modules\Vouchers\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use App\BusinessLocation;
use App\AccountTransaction;
use App\Account;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    /* Mutators and Accessors */
    /* Scopes */
    /* Eloquent Relationsips */

    use SoftDeletes;
    protected $guarded = ['id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function location()
    {

        return $this->belongsTo(BusinessLocation::class);
    }

    public function account_transactions()
    {

        return $this->hasMany(AccountTransaction::class,'voucher_id','id');
    }

    public function account_to()
    {

        return $this->belongsTo(Account::class,'to_account','id');
    }

    public function account_from()
    {

        return $this->belongsTo(Account::class,'from_account','id');
    }

}
