<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('business_id');
            $table->integer('location_id');
            $table->dateTime('date');
            $table->integer('from_account')->nullable();
            $table->integer('to_account')->nullable();
            $table->string('ref_no')->nullable();
            $table->integer('voucher_no')->default(0);
            $table->decimal('amount', 22, 4)->default(0);
            $table->enum('payment_method',['Cash','Bank'])->default('Cash');
            $table->enum('type',['CP','CR','BP','BR','JV'])->default('CP');
            $table->integer('user_id');
            $table->text('note')->nullable();
            $table->string('document')->nullable();            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
