<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //temperature
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->string('customer_business')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('email')->nullable();
            $table->string('designation')->nullable();
            $table->string('plant_capacity')->nullable();
            $table->dateTime('date_time');
            $table->string('address')->nullable();
            $table->string('hours_meter_RO1')->nullable();
            $table->string('hours_meter_RO2')->nullable();
            $table->string('water_temperature_RO1')->nullable();
            $table->string('water_temperature_RO2')->nullable();
            $table->string('multimedia_filter_inlet_RO1')->nullable();
            $table->string('multimedia_filter_inlet_RO2')->nullable();
            $table->string('multimedia_filter_outlet_RO1')->nullable();
            $table->string('multimedia_filter_outlet_RO2')->nullable();
            $table->string('differential_pressure_RO1')->nullable();
            $table->string('differential_pressure_RO2')->nullable();
            $table->string('ph_feed_RO1')->nullable();
            $table->string('ph_feed_RO2')->nullable();
            $table->string('tds_conductivity_RO1')->nullable();
            $table->string('tds_conductivity_RO2')->nullable();
            $table->string('free_chlorine_RO1')->nullable();
            $table->string('free_chlorine_RO2')->nullable();
            $table->string('iron_RO1')->nullable();
            $table->string('iron_RO2')->nullable();
            $table->string('sdi_before_cartridge_RO1')->nullable();
            $table->string('sdi_before_cartridge_RO2')->nullable();
            $table->string('sdi_after_cartridge_RO1')->nullable();
            $table->string('sdi_after_cartridge_RO2')->nullable();
            $table->string('ph_product_water_RO1')->nullable();
            $table->string('ph_product_water_RO2')->nullable();
            $table->string('tds_conductivity_product_water_RO1')->nullable();
            $table->string('tds_conductivity_product_water_RO2')->nullable();
            $table->string('ph_reject_water_RO1')->nullable();
            $table->string('ph_reject_water_RO2')->nullable();
            $table->string('sodium_hypochlorite_dosing_RO1')->nullable();
            $table->string('sodium_hypochlorite_dosing_RO2')->nullable();
            $table->string('sodium_hypochlorite_tank_level_RO1')->nullable();
            $table->string('sodium_hypochlorite_tank_level_RO2')->nullable();
            $table->string('polygard_600_dosing_RO1')->nullable();
            $table->string('polygard_600_dosing_RO2')->nullable();
            $table->string('polygard_600_tank_level_RO1')->nullable();
            $table->string('polygard_600_tank_level_RO2')->nullable();
            $table->string('sodium_metabisulphite_dosing_RO1')->nullable();
            $table->string('sodium_metabisulphite_dosing_RO2')->nullable();
            $table->string('sodium_metabisulphite_tank_level_RO1')->nullable();
            $table->string('sodium_metabisulphite_tank_level_RO2')->nullable();
            $table->string('acid_dosing_RO1')->nullable();
            $table->string('acid_dosing_RO2')->nullable();
            $table->string('acid_tank_level_RO1')->nullable();
            $table->string('acid_tank_level_RO2')->nullable();
            $table->string('biocide_dosing_RO1')->nullable();
            $table->string('biocide_dosing_RO2')->nullable();
            $table->string('biocide_tank_level_RO1')->nullable();
            $table->string('biocide_tank_level_RO2')->nullable();
            $table->string('Caustic_dosing_RO1')->nullable();
            $table->string('Caustic_dosing_RO2')->nullable();
            $table->string('Caustic_tank_level_RO1')->nullable();
            $table->string('Caustic_tank_level_RO2')->nullable();
            $table->string('filter_cartridge_in_RO1')->nullable();
            $table->string('filter_cartridge_in_RO2')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
