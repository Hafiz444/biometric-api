<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemainingColumnsToVisits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('visits', function (Blueprint $table) {
            $table->string('filter_cartridge_out_RO1')->nullable();
            $table->string('filter_cartridge_out_RO2')->nullable();
            $table->string('differential_pressure_pressure_RO1')->nullable();
            $table->string('differential_pressure_pressure_RO2')->nullable();
            $table->string('pump_discharge_pressure_RO1')->nullable();
            $table->string('pump_discharge_pressure_RO2')->nullable();
            $table->string('membrane_feed_pressure_a_RO1')->nullable();
            $table->string('membrane_feed_pressure_a_RO2')->nullable();
            $table->string('inter_stage_pressure_b_RO1')->nullable();
            $table->string('inter_stage_pressure_b_RO2')->nullable();
            $table->string('reject_pressure_RO1')->nullable();
            $table->string('reject_pressure_RO2')->nullable();
            $table->string('differential_pressure_a_b_RO1')->nullable();
            $table->string('differential_pressure_a_b_RO2')->nullable();
            $table->string('differential_pressure_b_c_RO1')->nullable();
            $table->string('differential_pressure_b_c_RO2')->nullable();
            $table->string('differential_pressure_a_c_RO1')->nullable();
            $table->string('differential_pressure_a_c_RO2')->nullable();
            $table->string('product_flow_RO1')->nullable();
            $table->string('product_flow_RO2')->nullable();
            $table->string('reject_flow_RO1')->nullable();
            $table->string('reject_flow_RO2')->nullable();
            $table->string('feed_flow_RO1')->nullable();
            $table->string('feed_flow_RO2')->nullable();
            $table->string('1st_stage_product_flow_RO1')->nullable();
            $table->string('1st_stage_product_flow_RO2')->nullable();
            $table->string('recovery_RO1')->nullable();
            $table->string('recovery_RO2')->nullable();
            $table->string('salt_passage_RO1')->nullable();
            $table->string('salt_passage_RO2')->nullable();
            $table->string('rejection_RO1')->nullable();
            $table->string('rejection_RO2')->nullable();
            $table->text('comments')->nullable();
            $table->text('recommendations')->nullable();
            $table->text('stage_product_flow_RO1')->nullable();
            $table->text('stage_product_flow_RO2')->nullable();
            $table->text('recovery_RO1')->nullable();
            $table->text('recovery_RO2')->nullable();
            $table->text('salt_passage_RO1')->nullable();
            $table->text('salt_passage_RO2')->nullable();
            $table->text('rejection_RO1')->nullable();
            $table->text('rejection_RO2')->nullable();
            $table->text('comments')->nullable();
            $table->text('recommendations')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visits', function (Blueprint $table) {
            //
        });
    }
}
