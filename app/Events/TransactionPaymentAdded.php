<?php

namespace App\Events;

use App\TransactionPayment;
use Illuminate\Queue\SerializesModels;

class TransactionPaymentAdded
{
    use SerializesModels;

    public $transactionPayment;
    public $formInput;

    /**
     * Create a new event instance.
     *
     * @param TransactionPayment $transactionPayment
     * @param array $formInput = []
     */
    public function __construct(TransactionPayment $transactionPayment, $formInput = [])
    {
        $this->transactionPayment = $transactionPayment;
        $this->formInput = $formInput;
    }
}
