<?php

namespace App\Listeners;

use App\Account;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\AccountTransaction;

use App\Utils\ModuleUtil;

class UpdateAccountTransaction
{
    protected $moduleUtil;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ModuleUtil $moduleUtil)
    {
        $this->moduleUtil = $moduleUtil;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if(!$this->moduleUtil->isModuleEnabled('account')){
            return true;
        }

        //Create new account transaction
        if ($event->transactionType == 'sell'){

            $credit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'credit' => $event->transactionPayment['amount'],
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'note' => $event->transactionPayment['note']
            ];
            AccountTransaction::where('type', 'credit')->where('sub_type', 'sell_payment')
                ->where('transaction_payment_id', $event->transactionPayment->id)
                ->update($credit_transaction_data);

            $debit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $event->transactionPayment['account_id'],
                'debit' => $event->transactionPayment['amount'],
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'note' => $event->transactionPayment['note']
            ];
            AccountTransaction::where('type', 'debit')->where('sub_type', 'sell_payment')
                ->where('transaction_payment_id', $event->transactionPayment->id)
                ->update($debit_transaction_data);
        }

        if ($event->transactionType == 'purchase'){

            $debit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'debit' => $event->transactionPayment['amount'],
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'note' => $event->transactionPayment['note']
            ];
            AccountTransaction::where('type', 'debit')->where('sub_type', 'purchase_payment')
                ->where('transaction_payment_id', $event->transactionPayment->id)
                ->update($debit_transaction_data);

            $credit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $event->transactionPayment['account_id'],
                'credit' => $event->transactionPayment['amount'],
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'note' => $event->transactionPayment['note']
            ];
            AccountTransaction::where('type', 'credit')->where('sub_type', 'purchase_payment')
                ->where('transaction_payment_id', $event->transactionPayment->id)
                ->update($credit_transaction_data);
        }

        if ($event->transactionType == 'expense'){

            $debit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'debit' => $event->transactionPayment['amount'],
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'note' => $event->transactionPayment['note']
            ];
            AccountTransaction::where('type', 'debit')->where('sub_type', 'expense_payment')
                ->where('transaction_payment_id', $event->transactionPayment->id)
                ->update($debit_transaction_data);

            $credit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $event->transactionPayment['account_id'],
                'credit' => $event->transactionPayment['amount'],
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'note' => $event->transactionPayment['note']
            ];
            AccountTransaction::where('type', 'credit')->where('sub_type', 'expense_payment')
                ->where('transaction_payment_id', $event->transactionPayment->id)
                ->update($credit_transaction_data);
        }
    }
}
