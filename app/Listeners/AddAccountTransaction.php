<?php

namespace App\Listeners;

use App\Account;
use App\AccountTransaction;

use App\Events\TransactionPaymentAdded;

use App\Transaction;
use App\Utils\ModuleUtil;

class AddAccountTransaction
{
    protected $moduleUtil;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ModuleUtil $moduleUtil)
    {
        $this->moduleUtil = $moduleUtil;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(TransactionPaymentAdded $event)
    {
        //dd($event);
        if (!$this->moduleUtil->isModuleEnabled('account')) {
            return true;
        }

        // get account id against contact_id(payment_for)
        $account_id = Account::where('contact_id', $event->transactionPayment['payment_for'])->pluck('id')->first();
        $transaction_type = $event->formInput['transaction_type'] ? $event->formInput['transaction_type'] : $event->formInput['type'];
        if (empty($event->transactionPayment['account_id'])){
            $business_id = request()->session()->get('user.business_id');
            $payment_account_id = Account::where('name', 'Cash in Hand')->where('business_id', $business_id)->pluck('id')->first();
        }else{
            $payment_account_id = $event->transactionPayment['account_id'];
        }

        //Create new account transaction
        if ($transaction_type == 'sell'){

            $credit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $account_id ? $account_id : 0,
                'debit' => 0,
                'credit' => $event->transactionPayment['amount'],
                'type' => 'credit',
                'sub_type' => 'sell_payment',
                'reff_no' => 'sp'.$event->transactionPayment->paid_on,
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'transaction_id' => $event->transactionPayment->transaction_id,
                'transaction_payment_id' =>  $event->transactionPayment->id,
                'note' => $event->transactionPayment['note']
            ];
            $credit_transaction = AccountTransaction::create($credit_transaction_data);

            $debit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $payment_account_id,
                'debit' => $event->transactionPayment['amount'],
                'credit' => 0,
                'type' => 'debit',
                'sub_type' => 'sell_payment',
                'reff_no' => 'sp'.$event->transactionPayment->paid_on,
                'transfer_transaction_id' => $credit_transaction->id,
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'transaction_id' => $event->transactionPayment->transaction_id,
                'transaction_payment_id' =>  $event->transactionPayment->id,
                'note' => $event->transactionPayment['note']
            ];
            $debit_transaction = AccountTransaction::create($debit_transaction_data);
            $credit_transaction->transfer_transaction_id = $debit_transaction->id;
            $credit_transaction->save();
        }

        if ($transaction_type == 'purchase'){

            $debit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $account_id,
                'debit' => $event->transactionPayment['amount'],
                'credit' => 0,
                'type' => 'debit',
                'sub_type' => 'purchase_payment',
                'reff_no' => 'pp'.$event->transactionPayment->paid_on,
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'transaction_id' => $event->transactionPayment->transaction_id,
                'transaction_payment_id' =>  $event->transactionPayment->id,
                'note' => $event->transactionPayment['note']
            ];
            $debit_transaction = AccountTransaction::create($debit_transaction_data);

            $credit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $payment_account_id,
                'debit' => 0,
                'credit' => $event->transactionPayment['amount'],
                'type' => 'credit',
                'sub_type' => 'purchase_payment',
                'reff_no' => 'pp'.$event->transactionPayment->paid_on,
                'transfer_transaction_id' => $debit_transaction->id,
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'transaction_id' => $event->transactionPayment->transaction_id,
                'transaction_payment_id' =>  $event->transactionPayment->id,
                'note' => $event->transactionPayment['note']
            ];
            $credit_transaction = AccountTransaction::create($credit_transaction_data);
            $debit_transaction->transfer_transaction_id = $credit_transaction->id;
            $debit_transaction->save();
        }

        if ($transaction_type == 'expense'){

            if (array_key_exists( 'expense_category_id', $event->formInput)){
                $expense_account_id = Account::where('expense_category_id', $event->formInput['expense_category_id'])->pluck('id')->first();
            }else{
                $expense_category_id = Transaction::where('id', $event->transactionPayment['transaction_id'])->pluck('expense_category_id')->first();
                $expense_account_id = Account::where('expense_category_id', $expense_category_id)->pluck('id')->first();
            }

            $debit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $expense_account_id,
                'debit' => $event->transactionPayment['amount'],
                'credit' => 0,
                'type' => 'debit',
                'sub_type' => 'expense_payment',
                'reff_no' => 'ep'.$event->transactionPayment->paid_on,
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'transaction_id' => $event->transactionPayment->transaction_id,
                'transaction_payment_id' =>  $event->transactionPayment->id,
                'note' => $event->transactionPayment['note']
            ];
            $debit_transaction = AccountTransaction::create($debit_transaction_data);

            $credit_transaction_data = [
                'amount' => $event->transactionPayment['amount'],
                'account_id' => $payment_account_id,
                'debit' => 0,
                'credit' => $event->transactionPayment['amount'],
                'type' => 'credit',
                'sub_type' => 'expense_payment',
                'reff_no' => 'ep'.$event->transactionPayment->paid_on,
                'transfer_transaction_id' => $debit_transaction->id,
                'operation_date' => $event->transactionPayment->paid_on,
                'created_by' => $event->transactionPayment->created_by,
                'transaction_id' => $event->transactionPayment->transaction_id,
                'transaction_payment_id' =>  $event->transactionPayment->id,
                'note' => $event->transactionPayment['note']
            ];
            $credit_transaction = AccountTransaction::create($credit_transaction_data);
            $debit_transaction->transfer_transaction_id = $credit_transaction->id;
            $debit_transaction->save();
        }
    }
}
