<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $guarded = ['id'];

    /**
     * Return list of locations for a business
     *
     * @param int $business_id
     * @param boolean $show_all = false
     * @param array $receipt_printer_type_attribute =
     *
     * @return array
     */
    public static function forDropdown($business_id, $show_all = false)
    {
        $devices = Device::where('business_id', $business_id)->pluck('device_name', 'id');
        if ($show_all) {
            $devices->prepend(__('messages.please_select'), '');
        }
        return $devices;
    }
}
