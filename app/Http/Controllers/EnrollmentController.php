<?php

namespace App\Http\Controllers;

use App\Device;
use App\Enrollment;
use App\User;
use App\Utils\BusinessUtil;
use App\Utils\ProductUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class EnrollmentController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $businessUtil;
    protected $productUtil;

    /**
     * Constructor
     *
     * @param BusinessUtil $businessUtil
     * @param ProductUtil $productUtil
     */
    public function __construct(BusinessUtil $businessUtil, ProductUtil $productUtil)
    {
        $this->businessUtil = $businessUtil;
        $this->productUtil = $productUtil;
    }

    /**
     * Display a listing of the resource.
     * @param  \App\Visit  $visit
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        if (!auth()->user()->can('enrollment.view') && !auth()->user()->can('enrollment.create')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            $business_id = request()->session()->get('user.business_id');

            $enrollments = Enrollment::leftjoin('devices AS d', 'd.id', '=', 'enrollments.device_id')
                ->leftjoin('users AS u', 'u.id', '=', 'enrollments.user_id')
                ->where('enrollments.business_id', $business_id)
                ->select(['enrollments.id','user_id', 'd.device_name', 'finger_id', 'u.enrollment_status',  'u.email',
                    DB::raw("CONCAT(COALESCE(u.surname, ''), ' ', COALESCE(u.first_name, ''), ' ', COALESCE(u.last_name, '')) as full_name")]);

            return Datatables::of($enrollments)
                ->editColumn('finger_id', function ($row) {
                    return '<span>'. __("device.finger_id").' '.$row->finger_id.'</span>';
                })
                ->addColumn(
                    'action',
                    '<a href="{{action(\'EnrollmentController@edit\', [$id])}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> @lang("messages.edit")</a>
                        &nbsp;<button data-href="{{action(\'EnrollmentController@destroy\', [$id])}}" class="btn btn-xs btn-danger delete_user_button"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</button>'
                )
                ->editColumn('enrollment_status', function ($row) {
                    if ($row->enrollment_status == 1){
                        return '<span class="text-green">'. __("device.enrolled") .'</span>';
                    }else{
                        return '<span class="text-danger">'. __("device.not_enrolled") .'</span>';
                    }
                })
                ->removeColumn('id')
                ->rawColumns(['action', 'finger_id', 'enrollment_status'])
                ->make(true);
        }

        return view('enrollment.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        if (!auth()->user()->can('enrollment.update')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');
        $users =User::forDropdown($business_id);
        $devices = Device::forDropdown($business_id, true);
        return view('enrollment.create')->with(compact('users', 'devices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('enrollment.store')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            //Enroll user for biometric
            $business_id = request()->session()->get('user.business_id');
            $input = array_merge(['business_id' => $business_id], $request->except('_token'));
            Enrollment::create($input);

            $output = ['success' => 1,
                'msg' => __("user.user_enrolled_success")
            ];
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = ['success' => 0,
                'msg' => __("messages.something_went_wrong")
            ];
        }
        return redirect('enrollments')->with('status', $output);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visit  $visitReport
     * @return \Illuminate\Http\Response
     */
    public function show(Visit $visitReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visit  $visitReport
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->can('enrollment.edit')) {
            abort(403, 'Unauthorized action.');
        }
        $business_id = request()->session()->get('user.business_id');
        $enrollment = Enrollment::where('id', $id)->first();
        $users =User::forDropdown($business_id);
        $devices = Device::forDropdown($business_id, true);
        return view('enrollment.edit')->with(compact('users', 'devices', 'enrollment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visit  $visitReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('enrollment.update')) {
            abort(403, 'Unauthorized action.');
        }
        try {
            $input = $request->except('_token', '_method', 'user_id_show');
            Enrollment::where('id', $id)->update($input);

            $output = [
                'success' => 1,
                'msg' => __('user.user_update_success')
            ];

        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = [
                'success' => 0,
                'msg' => __('messages.something_went_wrong')
            ];
        }
        return redirect('enrollments')->with('status', $output);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visit  $visitReport
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('visits.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            try {

                $enrollment = Enrollment::where('id', $id)->first();
                $enrollment->delete();

                $output = ['success' => true,
                    'msg' => __("user.enrollment_delete_success")
                ];
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

                $output = [
                    'success' => false,
                    'msg' => __("messages.something_went_wrong")
                ];
            }
            return $output;
        }
    }
}
