<?php

namespace App\Http\Controllers;

use App\BusinessLocation;
use App\Contact;
use App\Utils\BusinessUtil;
use App\Utils\ProductUtil;
use App\Visit;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class VisitController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $businessUtil;
    protected $productUtil;

    /**
     * Constructor
     *
     * @param BusinessUtil $businessUtil
     * @param ProductUtil $productUtil
     */
    public function __construct(BusinessUtil $businessUtil, ProductUtil $productUtil)
    {
        $this->businessUtil = $businessUtil;
        $this->productUtil = $productUtil;
    }

    /**
     * Display a listing of the resource.
     * @param  \App\Visit  $visit
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('visits.view') && !auth()->user()->can('visits.create')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');
        if (request()->ajax()) {

            $visits = Visit::leftJoin('contacts AS c', 'visits.customer_id', '=', 'c.id')
                ->join('business_locations AS bl', 'visits.location_id', '=', 'bl.id')
                ->where('visits.business_id', $business_id)->select(
                    'visits.*', 'bl.name as location_name', 'c.name AS customer_name'
                );

            $permitted_locations = auth()->user()->permitted_locations();
            if ($permitted_locations != 'all') {
                $visits->whereIn('visits.location_id', $permitted_locations);
            }

            //Add condition for location,used in sales representative expense report
            if (request()->has('location_id')) {
                $location_id = request()->get('location_id');
                if (!empty($location_id)) {
                    $visits->where('visits.location_id', $location_id);
                }
            }

            if (!empty(request()->customer_id)) {
                $customer_id = request()->customer_id;
                $visits->where('visits.customer_id', $customer_id);
            }

            if (!empty(request()->start_date) && !empty(request()->end_date)) {
                $start = request()->start_date;
                $end =  request()->end_date;
                $visits->whereDate('visits.date_time', '>=', $start)
                    ->whereDate('visits.date_time', '<=', $end);
            }

            $datatable = Datatables::of($visits)
                ->addColumn(
                    'action',
                    function ($row) {
                        $html = '<div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle btn-xs" data-toggle="dropdown" 
                                    aria-expanded="false">' . __("messages.actions") . '<span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-left" role="menu">';
                        /*if (auth()->user()->can("visits.view") || auth()->user()->can("visits.access")) {
                            $html .= '<li><a href="#" data-href="' . action("SellController@show", [$row->id]) . '" class="btn-modal" data-container=".view_modal"><i class="fas fa-eye" aria-hidden="true"></i> ' . __("messages.view") . '</a></li>';
                        }*/

                        if (auth()->user()->can("visits.access")) {
                            $html .= '<li><a href="' . action('VisitController@edit', [$row->id]) . '"><i class="fas fa-edit"></i> ' . __("messages.edit") . '</a></li>';
                        }
                        if (auth()->user()->can("visits.delete") || auth()->user()->can("visits.delete")) {
                            $html .= '<li><a href="#" data-href="' . action('VisitController@destroy', [$row->id]) . '" class="delete_visit"><i class="fas fa-trash"></i> ' . __("messages.delete") . '</a></li>';
                        }
                        if (auth()->user()->can("visits.view") || auth()->user()->can("visits.access")) {
                            $html .= '<li><a href="#" class="print-invoice" data-href="' . route('visit.printVisitReport', [$row->id]) . '"><i class="fas fa-print" aria-hidden="true"></i> ' . __("messages.print") . '</a></li>';
                        }
                        $html .= '</ul></div>';
                        return $html;
                    }
                )
                ->removeColumn('id')
                ->editColumn('date_time', '{{@format_datetime($date_time)}}')
                ->addIndexColumn();
            $rawColumns = ['action'];
            return $datatable->rawColumns($rawColumns)->make(true);
        }
        $customers = Contact::customersDropdown($business_id, false);
        $business_locations = BusinessLocation::forDropdown($business_id);

        return view('visits.index')->with(compact('customers', 'business_locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        if (!auth()->user()->can('visits.create')) {
            abort(403, 'Unauthorized action.');
        }
        $business_id = request()->session()->get('user.business_id');
        $customers = Contact::customersDropdown($business_id, false);
        $default_datetime = $this->businessUtil->format_date('now', true);
        $business_locations = BusinessLocation::forDropdown($business_id);
        return view('visits.create')->with(compact('customers', 'default_datetime', 'business_locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('visits.create')) {
            abort(403, 'Unauthorized action.');
        }
        try {
            $business_id = $request->session()->get('user.business_id');
            $date_time = $this->productUtil->uf_date($request->input('date_time'), true);
            $input = array_merge(['business_id' => $business_id, 'date_time' => $date_time], $request->except('_token', 'date_time'));
            $visit = Visit::create($input);

            if (!empty($visit)){
                Visit::where('id', $visit->id)->update(['business_id' => $business_id]);
            }

            $output = [
                'success' => 1,
                'msg' => __('visit.added_success')
            ];
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = ['success' => 0,
                'msg' => __("messages.something_went_wrong")
            ];
        }

        return redirect('visits')->with('status', $output);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visit  $visitReport
     * @return \Illuminate\Http\Response
     */
    public function show(Visit $visitReport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visit  $visitReport
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->can('visits.create')) {
            abort(403, 'Unauthorized action.');
        }
        $business_id = request()->session()->get('user.business_id');
        $visit = Visit::where('business_id', $business_id)->where('id', $id)->first();
        $customers = Contact::customersDropdown($business_id, false);
        $default_datetime = $this->businessUtil->format_date('now', true);
        $business_locations = BusinessLocation::forDropdown($business_id);
        return view('visits.edit')->with(compact('customers', 'default_datetime', 'business_locations', 'visit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visit  $visitReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request, $id);
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }
        try {
            $business_id = $request->session()->get('user.business_id');
            $date_time = $this->productUtil->uf_date($request->input('date_time'), true);
            $input = array_merge(['business_id' => $business_id, 'date_time' => $date_time], $request->except('_token', 'date_time', '_method'));

            Visit::where('business_id', $business_id)->where('id', $id)->update($input);

            $output = [
                'success' => 1,
                'msg' => __('visit.updated_success')
            ];

        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = [
                'success' => 0,
                'msg' => __('messages.something_went_wrong')
            ];
        }
        return redirect('visits')->with('status', $output);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visit  $visitReport
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('visits.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            try {

                $visit = Visit::where('id', $id)->first();
                $visit->delete();

                $output = ['success' => true,
                    'msg' => __("visit.visit_delete_success")
                ];
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

                $output = [
                    'success' => false,
                    'msg' => __("messages.something_went_wrong")
                ];
            }
            return $output;
        }
    }

    public function getCustomerDetails(Request $request){
        $customer = Contact::where('id', $request->customer_id)->first();
        return json_encode($customer);
    }

    /**
     * Prints Ledger for account
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function printVisitReport(Request $request, $id)
    {
        if (request()->ajax()) {
            try {

                $output = ['is_enabled' => false,
                    'print_type' => 'browser',
                    'html_content' => null,
                    'printer_config' => [],
                    'data' => []
                ];

                $business_name = $request->session()->get('business.name');
                $business_id = $request->session()->get('business.id');
                $visit = Visit::where('business_id', $business_id)->where('id', $id)->first();
                $customer_name = Contact::where('id', $visit->customer_id)->value('name');
                $location_name = BusinessLocation::where('id', $visit->location_id)->value('name');
                $output['html_content'] = view('visits.print_visit', compact('business_name', 'visit', 'customer_name', 'location_name'))->render();
                $receipt = $output;

                if (!empty($receipt)) {
                    $output = ['success' => 1, 'receipt' => $receipt];
                }

            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

                $output = ['success' => 0,
                    'msg' => trans("messages.something_went_wrong")
                ];
            }
            return $output;
        }
    }
}
