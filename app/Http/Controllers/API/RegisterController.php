<?php

namespace App\Http\Controllers\API;

use App\Business;
use App\Device;
use App\EmployeeAttendance;
use App\Enrollment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'business_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input = $request->all();
        $business_name = trim($input['business_id']);
        $business_id = Business::where('name', $business_name)->value('id');
        $input['password'] = bcrypt($input['password']);
        $input['business_id'] = $business_id;
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['first_name'] =  $user->first_name;

        return $this->sendResponse($success, 'User register successfully.');
    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('MyApp')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }

    public function AuthenticateDevice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mac' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $device = Device::where('mac_address', $request->mac)->first();

        if (!empty($device)) {
            $device->update(['status' => 1]);
            return response()->json(['device_key' => $device->device_key,'message' => 'device active successfully'], 200);
        } else {
            return response()->json(['error' => 'device is not present of this mac'], 401);
        }
    }

    public function EmployeeFingerID(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $enrollment = Enrollment::where('finger_id', $request->employee_id)->first();

        if (!empty($enrollment)) {
            return response()->json(['finger_id' => $enrollment->finger_id], 200);
        } else {
            return response()->json(['error' => 'Employee not found with this Employee ID'], 401);
        }
    }

    public function enrollmentSuccess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
            'enrollment_status' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::where('id', $request->employee_id)->update(['enrollment_status' => $request->enrollment_status]);

        if (!empty($user)) {
            return response()->json(['message' => 'Employee enrolled successfully'], 200);
        } else {
            return response()->json(['error' => 'Employee not enrolled'], 401);
        }
    }

    public function activeEmployees()
    {
        $user_id = auth()->guard('api')->user()->id;
        $business_id = User::where('id', $user_id)->value('business_id');

        $users = User::where('business_id', $business_id)->where('enrollment_status', '=', 1)->pluck('id')->toArray();

        if (!empty($users)) {
            return response()->json(['active_users' => $users], 200);
        } else {
            return response()->json(['error' => 'Employee not enrolled'], 401);
        }
    }

    public function employeeAttendance(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'finger_id' => 'required',
            'device_key' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $device = Device::where('device_key', $request->device_key)->first();
        $enrollment = Enrollment::where('device_id', $device->id)->where('finger_id', $request->finger_id)->first();
        $current_date_time = Carbon::now()->toDateTimeString();

        if ($device->device_location == 'outside'){

            $employee_attendance = EmployeeAttendance::where('business_id', $device->business_id)
                ->where('user_id', $enrollment->user_id)->orderBy('id','DESC')->first();
            if (!empty($employee_attendance->clock_in_time)){
                return response()->json(['error' => 'User already Checked-in'], 401);
            }else{
                EmployeeAttendance::create(['business_id' => $device->business_id, 'user_id' => $enrollment->user_id, 'clock_in_time' => $current_date_time]);
                return response()->json(['message' => 'User Checked-in successfully on '. $current_date_time], 200);
            }

        }elseif ($device->device_location == 'inside'){

            $employee_attendance = EmployeeAttendance::where('business_id', $device->business_id)
                ->where('user_id', $enrollment->user_id)->orderBy('id','DESC')->first();
            if (!empty($employee_attendance->clock_out_time)){
                return response()->json(['error' => 'User already Checked-out'], 401);
            }else{
                EmployeeAttendance::create(['business_id' => $device->business_id, 'user_id' => $enrollment->user_id, 'clock_out_time' => $current_date_time]);
                return response()->json(['message' => 'User Checked-out successfully on '. $current_date_time], 200);
            }

        }else{
            return response()->json(['error' => 'Error while saving attendance'], 401);
        }
    }
}