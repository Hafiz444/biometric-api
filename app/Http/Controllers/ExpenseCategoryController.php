<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountType;
use App\ExpenseCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ExpenseCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            $business_id = request()->session()->get('user.business_id');

            $expense_category = ExpenseCategory::join('accounts as a', 'expense_categories.account_id', '=', 'a.id')
                ->leftjoin('account_types as ats', 'a.account_type_id', '=', 'ats.id')
                ->where('expense_categories.business_id', $business_id)

                ->select(['ats.name as account_type_name', 'expense_categories.name as name', 'code', 'expense_categories.id as id'])
                ->orderby('expense_categories.name', 'ASC');

            return Datatables::of($expense_category)
                ->addColumn(
                    'action',
                    '<button data-href="{{action(\'ExpenseCategoryController@edit\', [$id])}}" class="btn btn-xs btn-primary btn-modal" data-container=".expense_category_modal"><i class="glyphicon glyphicon-edit"></i>  @lang("messages.edit")</button>
                        &nbsp;
                        <button data-href="{{action(\'ExpenseCategoryController@destroy\', [$id])}}" class="btn btn-xs btn-danger delete_expense_category"><i class="glyphicon glyphicon-trash"></i> @lang("messages.delete")</button>'
                )
                ->removeColumn('id')
                ->rawColumns([3])
                ->make(false);
        }

        return view('expense_category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }
        $business_id = request()->session()->get('user.business_id');
        $account_parent_type = AccountType::where('business_id', $business_id)
            ->where('name', '05-EXPENSES')
            ->pluck('id')
            ->first();

        $sub_accounts = AccountType::where('business_id', $business_id)
            ->where('parent_account_type_id', $account_parent_type)
            ->pluck('name', 'id');

        return view('expense_category.create',compact('sub_accounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }

        try {
            DB::beginTransaction();

            $business_id = $request->session()->get('user.business_id');
            $user_id = $request->session()->get('user.id');
            $input = $request->only(['name', 'code']);
            $input['business_id'] = $request->session()->get('user.business_id');

            $expense_category = ExpenseCategory::create($input);

            if (!empty($expense_category->id)) {
                $counter = Account::where('business_id', $business_id)->count();
                $account_next_no = str_pad($counter + 1, 2, 0, STR_PAD_LEFT);
                $account_name = AccountType::where('id', $request->account_id)->pluck('name')->first();
                $account_prefix = substr($account_name, 3, 4);

                $account_details = array(
                    'business_id' => $business_id,
                    'name' => $request->name,
                    'account_number' => $business_id . '05' . $account_prefix . $account_next_no,
                    'account_type_id' => $request->account_id,
                    'note' => 'Created from expense category',
                    'expense_category_id' => $expense_category->id,
                    'created_by' => $user_id,
                );
                $account = Account::create($account_details);

                $expense_category->account_id = $account->id;
                $expense_category->save();
            }

            DB::commit();
            $output = [
                'success' => true,
                'msg' => __("expense.added_success")
            ];

        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            DB::rollBack();
            $output = [
                'success' => false,
                'msg' => __("messages.something_went_wrong")
            ];
        }
        return $output;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpenseCategory  $expenseCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ExpenseCategory $expenseCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            $business_id = request()->session()->get('user.business_id');
            $expense_category = ExpenseCategory::where('business_id', $business_id)->find($id);
            $linked_account_id = Account::where('business_id', $business_id)
                ->where('expense_category_id', $expense_category->id)->pluck('account_type_id')->first();

            $account_parent_type = AccountType::where('business_id', $business_id)
                ->where('name', '05-EXPENSES')
                ->pluck('id')
                ->first();

            $sub_accounts = AccountType::where('business_id', $business_id)
                ->where('parent_account_type_id', $account_parent_type)
                ->get();

            return view('expense_category.edit')
                    ->with(compact('expense_category', 'linked_account_id', 'sub_accounts'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            try {
                DB::beginTransaction();
                $input = $request->only(['name', 'code']);
                $business_id = $request->session()->get('user.business_id');
                $user_id = $request->session()->get('user.id');

                $expense_category = ExpenseCategory::where('business_id', $business_id)->findOrFail($id);
                $expense_category->name = $input['name'];
                $expense_category->code = $input['code'];
                $expense_category->save();

                if (!empty($expense_category->id)) {

                    $account_details = array(
                        'name' => $request->name,
                        'account_type_id' => $request->account_id,
                        'note' => 'Updated from expense category',
                        'created_by' => $user_id,
                    );
                    //dd($account_details);
                    Account::where('expense_category_id', $id)->update($account_details);
                }

                DB::commit();
                $output = ['success' => true,
                            'msg' => __("expense.updated_success")
                            ];
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

                DB::rollBack();
                $output = ['success' => false,
                            'msg' => __("messages.something_went_wrong")
                        ];
            }

            return $output;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('expense.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            try {
                $business_id = request()->session()->get('user.business_id');

                $expense_category = ExpenseCategory::where('business_id', $business_id)->findOrFail($id);
                $expense_category->delete();

                if (!empty($expense_category)){
                    $expense_account = Account::where('business_id', $business_id)->where('expense_category_id', $id);
                    $expense_account->delete();
                }

                $output = ['success' => true,
                            'msg' => __("expense.deleted_success")
                            ];
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());
            
                $output = ['success' => false,
                            'msg' => __("messages.something_went_wrong")
                        ];
            }

            return $output;
        }
    }
}
