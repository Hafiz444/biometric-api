<?php

namespace App\Http\Controllers;

use App\BusinessLocation;
use App\DeviceFinger;
use App\Enrollment;
use App\User;
use App\Utils\BusinessUtil;
use App\Utils\ProductUtil;
use App\Contact;
use App\Device;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Validator;
use Yajra\DataTables\Facades\DataTables;

class DeviceController extends Controller
{
    /**
     * All Utils instance.
     *
     */
    protected $businessUtil;
    protected $productUtil;

    /**
     * Constructor
     *
     * @param BusinessUtil $businessUtil
     * @param ProductUtil $productUtil
     */
    public function __construct(BusinessUtil $businessUtil, ProductUtil $productUtil)
    {
        $this->businessUtil = $businessUtil;
        $this->productUtil = $productUtil;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!auth()->user()->can('devices.view') && !auth()->user()->can('devices.create')) {
            abort(403, 'Unauthorized action.');
        }

        $business_id = request()->session()->get('user.business_id');
        if (request()->ajax()) {

            $devices = Device::join('business_locations AS bl', 'devices.location_id', '=', 'bl.id')
                ->where('devices.business_id', $business_id)
                ->select('devices.*', 'bl.name as location_name');

            $permitted_locations = auth()->user()->permitted_locations();
            if ($permitted_locations != 'all') {
                $devices->whereIn('devices.location_id', $permitted_locations);
            }

            //Add condition for location,used in sales representative expense report
            if (request()->has('location_id')) {
                $location_id = request()->get('location_id');
                if (!empty($location_id)) {
                    $devices->where('devices.location_id', $location_id);
                }
            }

            if (!empty(request()->device_location)) {
                $device_location = request()->device_location;
                $devices->where('devices.device_location', $device_location);
            }

            $datatable = Datatables::of($devices)
                ->addColumn(
                    'action',
                    function ($row) {
                        $html = '<div class="btn-group">
                                    <button type="button" class="btn btn-info dropdown-toggle btn-xs" data-toggle="dropdown" 
                                    aria-expanded="false">' . __("messages.actions") . '<span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-menu-left" role="menu">';

                        if (auth()->user()->can("devices.access")) {
                            $html .= '<li><a href="' . action('DeviceController@edit', [$row->id]) . '"><i class="fas fa-edit"></i> ' . __("messages.edit") . '</a></li>';
                        }
                        if (auth()->user()->can("devices.delete") || auth()->user()->can("devices.delete")) {
                            $html .= '<li><a href="#" data-href="' . action('DeviceController@destroy', [$row->id]) . '" class="delete_device"><i class="fas fa-trash"></i> ' . __("messages.delete") . '</a></li>';
                        }
                        $html .= '</ul></div>';
                        return $html;
                    }
                )
                ->removeColumn('id')

                ->editColumn('device_location', function ($row) {
                    if ($row->device_location == 'inside') {
                        return '<b>Inside</b>';
                    }else{
                        return '<b>Outside</b>';
                    }
                })

                ->editColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return '<span class="text-green">Active</span>';
                    }else{
                        return '<span class="text-danger">In-Active</span>';
                    }
                })
                ->addIndexColumn();
            $rawColumns = ['action', 'device_location', 'status'];
            return $datatable->rawColumns($rawColumns)->make(true);
        }
        $business_locations = BusinessLocation::forDropdown($business_id);

        return view('device.index')->with(compact('business_locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        if (!auth()->user()->can('devices.create')) {
            abort(403, 'Unauthorized action.');
        }
        $business_id = request()->session()->get('user.business_id');
        $customers = Contact::customersDropdown($business_id, false);
        $default_datetime = $this->businessUtil->format_date('now', true);
        $business_locations = BusinessLocation::forDropdown($business_id);
        return view('device.create')->with(compact('customers', 'default_datetime', 'business_locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        if (!auth()->user()->can('devices.create')) {
            abort(403, 'Unauthorized action.');
        }
        try {
            $business_id = $request->session()->get('user.business_id');
            $encrypted_mac = Crypt::encryptString($request->mac_address);
            $input = array_merge(['business_id' => $business_id, 'device_key' => $encrypted_mac], $request->except('_token'));

            Device::create($input);

            $output = [
                'success' => 1,
                'msg' => __('device.added_success')
            ];
        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = ['success' => 0,
                'msg' => __("messages.something_went_wrong")
            ];
        }

        return redirect('devices')->with('status', $output);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function show(Device $device)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!auth()->user()->can('visits.create')) {
            abort(403, 'Unauthorized action.');
        }
        $business_id = request()->session()->get('user.business_id');
        $device = Device::where('business_id', $business_id)->where('id', $id)->first();
        $business_locations = BusinessLocation::forDropdown($business_id);
        return view('device.edit')->with(compact('business_locations', 'device'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        if (!auth()->user()->can('devices.access')) {
            abort(403, 'Unauthorized action.');
        }
        try {
            $business_id = $request->session()->get('user.business_id');
            $encrypted_mac = Crypt::encryptString($request->mac_address);
            $input = array_merge(['business_id' => $business_id, 'device_key' => $encrypted_mac], $request->except('_token', '_method'));

            Device::where('business_id', $business_id)->where('id', $id)->update($input);

            $output = [
                'success' => 1,
                'msg' => __('device.updated_success')
            ];

        } catch (\Exception $e) {
            \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

            $output = [
                'success' => 0,
                'msg' => __('messages.something_went_wrong')
            ];
        }
        return redirect('devices')->with('status', $output);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Device  $device
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!auth()->user()->can('visits.access')) {
            abort(403, 'Unauthorized action.');
        }

        if (request()->ajax()) {
            try {

                $device = Device::where('id', $id)->first();
                $device->delete();

                $output = ['success' => true,
                    'msg' => __("device.delete_success")
                ];
            } catch (\Exception $e) {
                \Log::emergency("File:" . $e->getFile(). "Line:" . $e->getLine(). "Message:" . $e->getMessage());

                $output = [
                    'success' => false,
                    'msg' => __("messages.something_went_wrong")
                ];
            }
            return $output;
        }
    }

    public function getFingers(Request $request)
    {
        $device_id = $request->device_id;

        $device_limit = Device::where('id', $device_id)->pluck('device_limit')->first();
        if ($device_limit > 0){
            $free_finger_ids = array();
            $enrolled_fingers = Enrollment::where('device_id', $device_id)->pluck('finger_id')->toArray();
            for($i=1; $i<=$device_limit; $i++){
                if (!in_array($i, $enrolled_fingers)){
                    array_push($free_finger_ids, $i);
                }
            }
            return $free_finger_ids;
        }
    }
}
