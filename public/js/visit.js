$(document).ready(function() {

    // get customer detail on change
    $("#customer_id").change(function(){
        var customer_id = $("#customer_id").val();
        $.ajax({
            url: "/visits/customer-details",
            type: "POST",
            data: {
                customer_id: customer_id,
            },
            cache: false,
            success: function(result){
                if(result != null){
                    var customer_details = JSON.parse(result);
                    var address = customer_details.address_line_1+" "+customer_details.city+" "+customer_details.state+" "+customer_details.country;
                    $("#customer_business").val(customer_details.supplier_business_name);
                    $("#customer_phone").val(customer_details.mobile);
                    $("#email").val(customer_details.email);
                    $("#address").val(address);
                    toastr.success(result.msg);
                }else{
                    toastr.error(LANG.something_went_wrong);
                }
            }
        });
    });

    // datepicker
    $('#date_time').datetimepicker({
        format: moment_date_format + ' ' + moment_time_format,
        ignoreReadonly: true,
    });

    // check required fields
    $(document).on('click', 'button#visit_submit', function(e) {
        e.preventDefault();

        if ($('form#visit_add_form').valid()) {
            $('form#visit_add_form').submit();
        }
    });

    //Date range as a button
    $('#visit_list_filter_date_range').daterangepicker(
        dateRangeSettings,
        function (start, end) {
            $('#visit_list_filter_date_range').val(start.format(moment_date_format) + ' ~ ' + end.format(moment_date_format));
            visit_table.ajax.reload();
        }
    );
    $('#visit_list_filter_date_range').on('cancel.daterangepicker', function (ev, picker) {
        $('#visit_list_filter_date_range').val('');
        visit_table.ajax.reload();
    });

    // list visit

    //Date range as a button
    visit_table = $('#visit_table').DataTable({
        processing: true,
        serverSide: true,
        aaSorting: [[1, 'desc']],
        "ajax": {
            "url": "/visits",
            "data": function (d) {
                if ($('#visit_list_filter_date_range').val()) {
                    var start = $('#visit_list_filter_date_range').data('daterangepicker').startDate.format('YYYY-MM-DD');
                    var end = $('#visit_list_filter_date_range').data('daterangepicker').endDate.format('YYYY-MM-DD');
                    d.start_date = start;
                    d.end_date = end;
                }

                d.location_id = $('#visit_list_filter_location_id').val();
                d.customer_id = $('#visit_list_filter_customer_id').val();
                d = __datatable_ajax_callback(d);
            }
        },
        scrollCollapse: true,
        columns: [
            { data: 'DT_Row_Index', name: 'DT_Row_Index' },
            {data: 'customer_name', name: 'customer_name'},
            {data: 'customer_business', name: 'customer_business'},
            {data: 'customer_phone', name: 'customer_phone'},
            {data: 'date_time', name: 'date_time'},
            {data: 'location_name', name: 'location_name'},
            {data: 'action', name: 'action', orderable: false, "searchable": false},
        ]
    });

    $(document).on('change', '#visit_list_filter_location_id, #visit_list_filter_customer_id', function () {
        visit_table.ajax.reload();
    });

    // delete visit
    $(document).on('click', 'a.delete_visit', function(e) {
        e.preventDefault();
        swal({
            title: LANG.sure,
            text: LANG.confirm_delete_visit,
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then(willDelete => {
            if (willDelete) {
                var href = $(this).data('href');
                var data = $(this).serialize();

                $.ajax({
                    method: 'DELETE',
                    url: href,
                    dataType: 'json',
                    data: data,
                    success: function(result) {
                        if (result.success === true) {
                            toastr.success(result.msg);
                            visit_table.ajax.reload();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                });
            }
        });
    });
});