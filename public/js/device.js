$(document).ready(function() {

    // check required fields
    $(document).on('click', 'button#device_submit', function(e) {
        e.preventDefault();

        if ($('form#add_device_form').valid()) {
            $('form#add_device_form').submit();
        }
    });

    // list Devices

    //Date range as a button
    device_table = $('#device_table').DataTable({
        processing: true,
        serverSide: true,
        aaSorting: [[1, 'desc']],
        "ajax": {
            "url": "/devices",
            "data": function (d) {

                d.location_id = $('#device_list_filter_location_id').val();
                d.device_location = $('#device_list_filter_device_location').val();
                d = __datatable_ajax_callback(d);
            }
        },
        scrollCollapse: true,
        columns: [
            { data: 'DT_Row_Index', name: 'DT_Row_Index' },
            {data: 'device_name', name: 'device_name'},
            {data: 'mac_address', name: 'mac_address'},
            {data: 'device_location', name: 'device_location'},
            {data: 'device_limit', name: 'device_limit'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, "searchable": false},
        ]
    });

    $(document).on('change', '#device_list_filter_location_id, #device_list_filter_device_location', function () {
        device_table.ajax.reload();
    });

    // delete device
    $(document).on('click', 'a.delete_device', function(e) {
        e.preventDefault();
        swal({
            title: LANG.sure,
            text: LANG.confirm_delete_device,
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then(willDelete => {
            if (willDelete) {
                var href = $(this).data('href');
                var data = $(this).serialize();

                $.ajax({
                    method: 'DELETE',
                    url: href,
                    dataType: 'json',
                    data: data,
                    success: function(result) {
                        if (result.success === true) {
                            toastr.success(result.msg);
                            device_table.ajax.reload();
                        } else {
                            toastr.error(result.msg);
                        }
                    },
                });
            }
        });
    });
});